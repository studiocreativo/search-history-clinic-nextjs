import { useState } from 'react'

import {
  TableCell as MuiTableCell
} from '@mui/material'
import Template from '@components/shared/Template'
import Table from '@components/shared/TableStick'
import TableRow from '@components/shared/TableStick/TableRow'
import ToolBarMain from '@components/shared/ToolBarMain'

const Billings = () => {
  const ID_KEY = 'name'

  const [sortBy, setSortBy] = useState('none')
  const [selectedList, setSelectedList] = useState([])

  const createHeaderCell = (id, label, isNumeric) => ({
    id,
    label,
    isNumeric
  })

  const header = [
    createHeaderCell('name', 'Dessert (100g serving)', false),
    createHeaderCell('calories', 'Calories', true),
    createHeaderCell('fat', 'Fat (g)', true),
    createHeaderCell('carbs', 'Carbs (g)', true),
    createHeaderCell('protein', 'Protein (g)', true)
  ]

  const normalizeHeaderToSort = header.map(({ label, id }) => ({
    label,
    value: id
  }))

  const handleClickRow = (_, { [ID_KEY]: key }) => {
    const exist = selectedList.indexOf(key) !== -1
    if (exist) {
      const newList = selectedList.filter(item => item !== key)
      setSelectedList(newList)
      return
    }

    setSelectedList([...selectedList, key])
  }

  const handleSelectAllClick = ({ target: { checked } }) => {
    if (checked) {
      const newSelectList = data.map(({ [ID_KEY]: key }) => key)
      setSelectedList(newSelectList)
      return
    }
    setSelectedList([])
  }

  const rows = ({
    dataRow,
    selectedProps
  }) => (
    <TableRow
      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
      selectedProps={selectedProps}
      onClick={(event) => handleClickRow(event, dataRow)}
      hover
    >
      <MuiTableCell component="th" scope="row">
        {dataRow.name}
      </MuiTableCell>
      <MuiTableCell align="right">{dataRow.calories}</MuiTableCell>
      <MuiTableCell align="right">{dataRow.fat}</MuiTableCell>
      <MuiTableCell align="right">{dataRow.carbs}</MuiTableCell>
      <MuiTableCell align="right">{dataRow.protein}</MuiTableCell>
    </TableRow>
  )

  function createData (name, calories, fat, carbs, protein) {
    return { name, calories, fat, carbs, protein }
  }

  const data = [
    createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
    createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
    createData('Eclair', 262, 16.0, 24, 6.0),
    createData('Cupcake', 305, 3.7, 67, 4.3),
    createData('Frozen yoghurt 1', 159, 6.0, 24, 4.0),
    createData('Ice cream sandwich 1', 237, 9.0, 37, 4.3),
    createData('Eclair 1', 262, 16.0, 24, 6.0),
    createData('Cupcake 1', 305, 3.7, 67, 4.3),
    createData('Frozen yoghurt 2', 159, 6.0, 24, 4.0),
    createData('Ice cream sandwich 2', 237, 9.0, 37, 4.3),
    createData('Eclair 2', 262, 16.0, 24, 6.0),
    createData('Cupcake 2', 305, 3.7, 67, 4.3),
    createData('Frozen yoghurt 3', 159, 6.0, 24, 4.0),
    createData('Ice cream sandwich 3', 237, 9.0, 37, 4.3),
    createData('Eclair 3', 262, 16.0, 24, 6.0),
    createData('Cupcake 3', 305, 3.7, 67, 4.3),
    createData('Frozen yoghurt 4', 159, 6.0, 24, 4.0),
    createData('Ice cream sandwich 4', 237, 9.0, 37, 4.3),
    createData('Eclair 4', 262, 16.0, 24, 6.0),
    createData('Cupcake 4', 305, 3.7, 67, 4.3)
  ]

  const handleFilterBy = (filterBy) => setSortBy(filterBy)

  const handleSelectedView = (view) => {
    // eslint-disable-next-line no-console
    console.log(view)
  }

  const handleButtonToolBarMain = () => {
    // eslint-disable-next-line no-console
    console.log('Click en ButtonToolBarMin')
  }

  const handleSearchData = (value) => {
    // eslint-disable-next-line no-console
    console.log(value)
  }

  return (
    <Template>
      <ToolBarMain
        title="Facturación"
        optionsFilter={normalizeHeaderToSort}
        onFilterBy={handleFilterBy}
        onSelectedView={handleSelectedView}
        buttonText="Nuevo Comprobante"
        buttonOnClick={handleButtonToolBarMain}
        onSearchData={handleSearchData}
      />
      <Table
        idKey={ID_KEY}
        data={data}
        headers={header}
        RowBuilder={rows}
        stickyHeader
        sorteable
        sorteableExternal={sortBy}
        selectable
        selectedList={selectedList}
        onSelectAllClick={handleSelectAllClick}
      />
    </Template>
  )
}

export default Billings
