
import { useRouter } from 'next/router'
import { FirebaseAuth } from '@services/firebase'
import { signOut } from 'firebase/auth'
import { useAuth } from '../context'
import UserPatient from '../models/UserPatient'
import FeaturesAuth from '@features/auth'

import Head from 'next/head'
import Template from '@components/shared/Template'
import CardUser from '@components/shared/CardUser'

function Home () {
  const router = useRouter()
  const { firebase: userFirebase } = useAuth()

  const handleClick = async () => {
    await signOut(FirebaseAuth)
    router.push('/login')
  }

  const user = new UserPatient({
    numberHistory: '12'
  })

  return (
    <>
      <Head>
        <title>Tu Doctor Net</title>
        <meta name="description" content="Generated by create next app" />
      </Head>
      <Template>
        <p>{userFirebase?.uid ?? ''}</p>
        <button type="button" onClick={handleClick}>Sign Out</button>
        <br />
        <CardUser
          user={user}
        >
          <h1>Oso</h1>
          <h2>OSO PEQUE</h2>
        </CardUser>
      </Template>
    </>
  )
}

export const getServerSideProps = FeaturesAuth.withAuthProps

export default Home
