import { useState, useContext } from 'react'
import { createHeaderCell, normalizeHeaderToSortBy } from '@utils/table'
import GlobalContext from '@context/GlobalContext'
import FeaturePatient from '@features/patients'
import UserBase from '@models/UserBase'

import {
  DialogContent,
  DialogContentText,
  DialogActions,
  Typography
} from '@mui/material'
import Head from 'next/head'
import Template from '@components/shared/Template'
import ToolBarMain from '@components/shared/ToolBarMain'
import Table from '@components/shared/TableStick'
import Button from '@components/shared/Button'
import Toast from '@components/shared/Toast'
import Swal from '@components/shared/Swal'
import ModalMeetings from '@components/meetings/ModalMeetings'
import ModalPatient from '@components/patients/ModalPatient'
import PatientGridView from '@components/patients/PatientGridView'
import PatientRowBuilder from '@components/patients/PatientRowBuilder'
import PatientModal from '@components/patients/PatientModal'

import {
  faTrashAlt,
  faExclamationTriangle
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import _ from 'underscore'

const tableHeaders = [
  createHeaderCell('names', 'Nombres'),
  createHeaderCell('history', 'N° Historia'),
  createHeaderCell('dni', 'DNI'),
  createHeaderCell('gender', 'Género'),
  createHeaderCell('age', 'Edad'),
  createHeaderCell('dateOfBirth', 'F. Nacimiento'),
  createHeaderCell('actions', 'Acciones')
]
const optionsSortByToolBarMain = normalizeHeaderToSortBy(tableHeaders, ['actions'])
const LABEL_ADD_PATIENT = 'Añadir Paciente'
const LABEL_EDIT_PATIENT = 'Editar Paciente'

const Patients = () => {
  const { patients } = useContext(GlobalContext)

  const [isGrid, setIsGrid] = useState(false)
  const [filter, setFilter] = useState({ value: '' })
  const [sortBy, setSortBy] = useState('')
  /** ********* Modal state *********** */
  const [open, setOpen] = useState(false)
  const [swalOpen, setSwalOpen] = useState(false)

  const [dataModal, setDataModal] = useState({
    edit: false,
    inputDisabled: true,
    title: LABEL_ADD_PATIENT
  })

  const [toastData, setToastData] = useState({
    open: false,
    type: null,
    textContent: null
  })

  /** ********* Patient State *********** */
  const initialPatientState = {
    dni: '',
    names: '',
    firtsSurname: '',
    secondSurname: '',
    gender: '',
    dateOfBirth: null,
    phones: [],
    department: null,
    province: null,
    district: null,
    address: '',
    civilStatus: '',
    numberHistory: '',
    occupation: '',
    photo: null
  }

  const [patient, setPatient] = useState(initialPatientState)

  /** ********* Select View *********** */
  const handleSelectedView = (view) => {
    if (view === 'select-grid') {
      setIsGrid(true)
    }
    setIsGrid(false)
  }

  const handleFilterBy = (filterBy) => setSortBy(filterBy)

  const filterData = () => {
    const dataFiltered = filter.value.length > 2
      ? patients.filter((el) => el.dni.toString().includes(filter.value) || el.fullname.toLowerCase().includes(filter.value))
      : patients
    return sortBy !== 'none' ? _.sortBy(dataFiltered, sortBy) : dataFiltered
  }

  /** ********* Close Modal *********** */
  const handleCloseModal = () => {
    setOpen(false)
    setPatient(initialPatientState)
  }

  /** ********* Open Modal *********** */
  const handleButtonToolBarMain = () => {
    setDataModal({
      ...dataModal,
      inputDisabled: true,
      title: LABEL_EDIT_PATIENT,
      edit: false
    })
    setPatient(initialPatientState)
    setOpen(true)
  }

  /** ******** Search Patient *********** */
  const handleSearchData = (value) => setFilter({ value })

  const handleSearchPatient = () => {
    fetch(`http://localhost:3000/api/dni/${patient.dni}`)
      .then((res) => res.json())
      .then((data) => {
        if (data.success) {
          const objUserBase = new UserBase({ ...data.user, numberHistory: patients.length + 1 })
          return setPatient({ ...patient, ...objUserBase.toJSON() })
        }
        console.log(data)
        setToastData({ open: true, textContent: data.message, type: 'danger' })
      })
  }

  /** ********* Close Modal *********** */
  const handleEditPatient = (patient) => {
    setDataModal({
      ...dataModal,
      inputDisabled: false,
      title: LABEL_EDIT_PATIENT,
      edit: true
    })
    setPatient(patient)
    setOpen(true)
  }

  const handleDeletePatient = (id) => {
    console.log(id)
    setSwalOpen(true)
  }

  /** ********* View User Modal *********** */
  const handleViewPatient = (patient) => {
    setDataModal({
      ...dataModal,
      inputDisabled: true,
      title: 'Ver Paciente',
      edit: false
    })
    setPatient(patient)
    setOpen(true)
  }

  /** ********* View Swal *********** */
  const handleCloseSwal = () => setSwalOpen(false)

  /** ********* Toast *********** */
  const handleCofirmDelete = () => {
    setSwalOpen(false)
    setToastData({ ...toastData, open: true })
  }

  const handleCloseToast = () => setToastData({ ...toastData, open: false })

  /** ********* Send patient data to backend *********** */
  const addPatient = () => {
    const patientExist = patients.find((el) => el.dni === patient.dni)
    if (patientExist) {
      setToastData({ open: true, type: 'warning', textContent: 'El paciente ya está registrado.' })
    } else {
      FeaturePatient.createPatient({ ...patient, numberHistory: patients.length })
    }
  }

  const editPatient = async () => {
    const response = await FeaturePatient.editPatient(patient)
    if (response) {
      setToastData({ open: true, type: 'success', textContent: 'Paciente actualizado satisfactoriamente.' })
    } else {
      setToastData({ open: true, type: 'danger', textContent: 'Ha ocurrido un error!.' })
    }
  }

  const handleSubmitModal = () => {
    dataModal.edit ? editPatient() : addPatient()
  }

  const handleAddMeeting = () => {
    console.log('HANDLE ADD MEETING')
  }

  const rowBuilder = ({
    dataRow,
    selectedProps
  }) => (
    <PatientRowBuilder
      dataRow={dataRow}
      selectedProps={selectedProps}
      onAddMeeting={handleAddMeeting}
      onEditPatient={handleEditPatient}
      onDeletePatient={handleDeletePatient}
    />
  )

  return (
    <>
      <Head>
        <title>Pacientes | doctorNet</title>
        <meta name="description" content="Generated by create next app" />
      </Head>
      <Template title="Pacientes">
        <ToolBarMain
          optionsFilter={optionsSortByToolBarMain}
          onFilterBy={handleFilterBy}
          onSelectedView={handleSelectedView}
          buttonText="Añadir Paciente"
          buttonOnClick={handleButtonToolBarMain}
          onSearchData={handleSearchData}
        />
        {
          !isGrid
            ? (
              <Table
                data={filterData()}
                headers={tableHeaders}
                RowBuilder={rowBuilder}
                tablePagination
              />
              )
            : (
              <PatientGridView
                data={filterData()}
                handleEditPatient={handleEditPatient}
                handleViewModal={handleViewPatient}
                handleDeletePatient={handleDeletePatient}
              />
              )
        }
      </Template>
      <PatientModal
        open={open}
        onClose={handleCloseModal}
        patient={patient}
        onChangePatient={setPatient}
      />
{/*       <ModalPatient
        open={open}
        dataModal={dataModal}
        handleCloseModal={handleCloseModal}
        handleSearchPatient={handleSearchPatient}
        handleSubmitModal={handleSubmitModal}
        patient={patient}
        setPatient={setPatient}
      /> */}
      <Swal
        open={swalOpen}
        onClose={handleCloseSwal}
      >
        <DialogContent>
          <Typography align="center" color="text.warning">
            <FontAwesomeIcon icon={faExclamationTriangle} size="5x" />
          </Typography>
          <DialogContentText variant="h6" align="center">
            ¿Estás seguro de eliminar este registro?
          </DialogContentText>
          <DialogContentText align="center">
            Si elimina este registro, se perderán todos los datos, ya no se podrán recuperar a futuro.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button variant="outlined" onClick={handleCloseSwal}>Cancelar</Button>
          <Button
            startIcon={<FontAwesomeIcon icon={faTrashAlt} size="xs" />}
            color="danger"
            onClick={handleCofirmDelete}
          >
            Eliminar
          </Button>
        </DialogActions>
      </Swal>
      <Toast
        open={toastData.open}
        type={toastData.type}
        textContent={toastData.textContent}
        onClose={handleCloseToast}
      />
      <ModalMeetings />
    </>
  )
}

export default Patients
