import { useState, useMemo } from 'react'
import { usePatients } from '@context/index'
import { createHeaderCell, normalizeHeaderToFilterBy } from '@utils/table'
import FeaturesPatient from '@features/patients'

import Head from 'next/head'
import Template from '@components/shared/Template'
import ToolBarMain from '@components/shared/ToolBarMain'
import Table from '@components/shared/TableStick'
import PatientRowBuilder from '@components/patients/PatientRowBuilder'
import PatientGridView from '@components/patients/PatientGridView'
import PatientModal from '@components/patients/PatientModal'

const tableHeaders = [
  createHeaderCell('names', 'Nombres'),
  createHeaderCell('numberHistory', 'N° Historia'),
  createHeaderCell('dni', 'DNI'),
  createHeaderCell('gender', 'Género'),
  createHeaderCell('age', 'Edad'),
  createHeaderCell('dateOfBirth', 'F. Nacimiento'),
  createHeaderCell('actions', 'Acciones')
]
const optionsFilterByToolBarMain = normalizeHeaderToFilterBy(tableHeaders, ['actions'])

const Patients = () => {
  const patients = usePatients()
  const [patient, setPatient] = useState(FeaturesPatient.initialPatientState)
  const [isList, setIsList] = useState(true)
  const [filterBy, setFilterBy] = useState('dni')
  const [searchData, setSearchData] = useState('')
  const [isOpenPatientModal, setIsOpenPatientModal] = useState(false)
  const [editPatient, setEditPatient] = useState(false)

  const patientsList = useMemo(() => {
    if (searchData.length > 2) {
      const searchWord = searchData.toString().toLowerCase()
      const listFiltered = patients.filter((patient) =>
        patient[filterBy]?.toString().toLowerCase().includes(searchWord))
      return listFiltered
    }
    return patients
  }, [filterBy, patients, searchData])

  const handleFilterByToolBarMain = (filterBy) => setFilterBy(filterBy)
  const handleSelectedView = (view) => setIsList(view === 'select-list')
  const handleClickButtonToolBarMain = () => setIsOpenPatientModal(true)
  const handleSearchDataToolBarMain = (search) => setSearchData(search)

  const handleAddMeeting = () => console.log('add meeting')
  const handleEditPatient = (patient) => {
    setPatient(patient)
    setEditPatient(true)
    setIsOpenPatientModal(true)
  }
  const handleDeletePatient = () => console.log('delete patient')
  const handleViewPatient = () => console.log('view patient')

  // ! OPTIMIZAR ESTO
  const handleClosePatientModal = () => {
    setPatient(FeaturesPatient.initialPatientState)
    setIsOpenPatientModal(false)
    setEditPatient(false)
  }

  const rowBuilder = ({
    dataRow
  }) => (
    <PatientRowBuilder
      dataRow={dataRow}
      onAddMeeting={handleAddMeeting}
      onEditPatient={handleEditPatient}
      onDeletePatient={handleDeletePatient}
    />
  )

  return (
    <>
      <Head>
        <title>Pacientes | TuDoctorNet</title>
        <meta name="description" content="Listado de Pacientes" />
      </Head>
      <Template title="Pacientes" >
        <ToolBarMain
          optionsFilter={optionsFilterByToolBarMain}
          onFilterBy={handleFilterByToolBarMain}
          optionDefaultFilterBy="dni"
          onSelectedView={handleSelectedView}
          buttonText="Nuevo Paciente"
          buttonOnClick={handleClickButtonToolBarMain}
          onSearchData={handleSearchDataToolBarMain}
        />
        {
          isList
            ? (
              <Table
                data={patientsList}
                headers={tableHeaders}
                RowBuilder={rowBuilder}
                tablePagination
                sorteable
              />
              )
            : (
              <PatientGridView
                data={patientsList}
                onViewModal={handleViewPatient}
                onEditPatient={handleEditPatient}
                onDeletePatient={handleDeletePatient}
              />
              )
        }
      </Template>
      <PatientModal
        edit={editPatient}
        open={isOpenPatientModal}
        onClose={handleClosePatientModal}
        patient={patient}
        onChangePatient={setPatient}
      />
    </>
  )
}

export default Patients
