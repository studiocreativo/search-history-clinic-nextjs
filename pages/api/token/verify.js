import axios from 'axios'

const handler = async (req, res) => {
  const { token } = req.body

  const response = await axios.post('http://localhost:8000/api/v1/token/verify', {
    token
  })
  const data = await res.json(response.data)

  return data
}

export default handler
