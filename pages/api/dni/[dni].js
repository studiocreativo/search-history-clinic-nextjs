import axios from 'axios'

export default async function handler (req, res) {
  const { dni } = req.query
  /* try {
    response = await axios.get(`https://apiperu.net.pe/api/dni_plus/${dni}`, {
      headers: {
        Authorization: 'Bearer vd5i5LBnH6enA17AEEJKPZTzbn97DriHE19EMKJ9Hj6p2lQaGP',
      },
    });
    return res.json(response.data);
  } catch (error) {
    return res.json(response.data);
  } */
  axios.get(`http://localhost:8000/api/v1/dni/${dni}`, {
    headers: {
      'Content-Type': 'application/json'
    }
  })
    .then((re) => {
      if (re.data.success) {
        return res.json(re.data)
      }
      return res.json(re.data)
    })
    .catch((err) => {
      throw err
    })
}
