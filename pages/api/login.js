import axios from 'axios'

const handler = async (req, res) => {
  const { dni, password, type } = req.body

  const response = await axios.post('http://localhost:8000/api/v1/login', {
    dni,
    password,
    type
  }, {
    headers: {
      'Content-Type': 'application/json'
    }
  })
  const data = await res.json(response.data)

  return data
}

export default handler
