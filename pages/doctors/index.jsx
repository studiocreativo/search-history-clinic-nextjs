import { useState } from 'react'
import DoctorGridView from '@components/doctor/DoctorGridView'
import ToolbarMain from '@components/shared/ToolBarMain'
import Template from '@components/shared/Template'
import AddDoctor from '@components/doctor/ModalDoctor'
import Table from '@components/shared/Table'
import DoctorTableRowCell from '@components/doctor/DoctorTableRowCell'
import PropTypes from 'prop-types'

import request from '../../utils/request'

function createData (name, calories, fat, carbs, protein) {
  return {
    name,
    calories,
    fat,
    carbs,
    protein
  }
}

const rows = [
  createData('Cupcake', 305, 3.7, 67, 4.3),
  createData('Donut', 452, 25.0, 51, 4.9),
  createData('Eclair', 262, 16.0, 24, 6.0),
  createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
  createData('Honeycomb', 408, 3.2, 87, 6.5),
  createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
  createData('Jelly Bean', 375, 0.0, 94, 0.0),
  createData('KitKat', 518, 26.0, 65, 7.0),
  createData('Lollipop', 392, 0.2, 98, 0.0),
  createData('Marshmallow', 318, 0, 81, 2.0),
  createData('Nougat', 360, 19.0, 9, 37.0),
  createData('Oreo', 437, 18.0, 63, 4.0)
]

const tableHeadCell = [
  {
    id: 'name',
    numeric: false,
    disablePadding: true,
    label: 'Nombres'
  },
  {
    id: 'calories',
    numeric: true,
    disablePadding: false,
    label: 'Género'
  },
  {
    id: 'fat',
    numeric: true,
    disablePadding: false,
    label: 'Celular'
  },
  {
    id: 'carbs',
    numeric: true,
    disablePadding: false,
    label: 'Especialidad'
  },
  {
    id: 'protein',
    numeric: true,
    disablePadding: false,
    label: 'Acciones'
  }
]

const Doctors = ({ ver }) => {
  const [open, setOpen] = useState(false)
  const [isGrid, setIsGrid] = useState(false)
  const [isDisabled, setIsDisabled] = useState(false)
  const [doctor, setDoctor] = useState({
    names: undefined,
    especiality: undefined,
    phone: undefined,
    email: undefined,
    identity: undefined
  })

  console.log(ver)

  const doctors = [
    {
      id: 1,
      names: 'Wende yanen',
      especiality: 'none',
      phone: '0000000',
      email: 'foo@bar.com',
      identity: '7555555',
      gender: 0
    },
    {
      id: 2,
      names: 'Wende yanen',
      especiality: 'none',
      phone: '0000000',
      email: 'foo@bar.com',
      identity: '7555555',
      gender: 0
    },
    {
      id: 3,
      names: 'Wende yanen',
      especiality: 'none',
      phone: '0000000',
      email: 'foo@bar.com',
      identity: '7555555',
      gender: 0
    },
    {
      id: 4,
      names: 'Wende yanen',
      especiality: 'none',
      phone: '0000000',
      email: 'foo@bar.com',
      identity: '7555555',
      gender: 0
    },
    {
      id: 5,
      names: 'Wende yanen',
      especiality: 'none',
      phone: '0000000',
      email: 'foo@bar.com',
      identity: '7555555',
      gender: 0
    },
    {
      id: 6,
      names: 'Wende yanen',
      especiality: 'none',
      phone: '0000000',
      email: 'foo@bar.com',
      identity: '7555555',
      gender: 0
    }
  ]

  const options = [
    {
      label: 'Doctors',
      value: 'Doctors'
    }
  ]

  const handleSelectedView = (view) => {
    // eslint-disable-next-line no-console
    console.log(view)
    if (view === 'select-grid') {
      setIsGrid(true)
    } else {
      setIsGrid(false)
    }
  }

  const handleFilterBy = (filter) => {
    // eslint-disable-next-line no-console
    console.log(filter)
  }

  // Open modal
  const handleButtonToolBarMain = () => {
    setOpen(true)
  }

  // Close modal
  const handleCloseModal = () => {
    setOpen(false)
  }

  // Edit Modal
  const handleEditModal = (id) => {
    setOpen(true)
    setIsDisabled(false)
    const currentDoctor = doctors.find((el) => el.id === id)
    setDoctor(currentDoctor)
  }

  // View Modal
  const handleViewModal = (id) => {
    setOpen(true)
    setIsDisabled(true)
    const currentDoctor = doctors.find((el) => el.id === id)
    setDoctor(currentDoctor)
  }

  return (
    <Template>
      <ToolbarMain
        title="Doctors"
        optionsFilter={options}
        onFilterBy={handleFilterBy}
        onSelectedView={handleSelectedView}
        buttonText="Añadir Doctor"
        buttonOnClick={handleButtonToolBarMain}
      />
      {
        isGrid
          ? (
            <DoctorGridView
              data={doctors}
              handleEditModal={handleEditModal}
              handleViewModal={handleViewModal}
            />
            )
          : (
            <Table
              data={rows}
              tableRowCell={DoctorTableRowCell}
              tableHeadCell={tableHeadCell}
            />
            )
      }
      <AddDoctor
        open={open}
        handleCloseModal={handleCloseModal}
        doctor={doctor}
        disabled={isDisabled}
      />
    </Template>
  )
}

export const getServerSideProps = async () => {
  let response = null
  // eslint-disable-next-line no-useless-catch
  try {
    response = await request('GET', {
      collection: 'doctors'
    })
  } catch (error) {
    throw error
  }

  return {
    props: {
      ver: response
    }
  }
}

Doctors.propTypes = {
  ver: PropTypes.arrayOf(PropTypes.object).isRequired
}

export default Doctors
