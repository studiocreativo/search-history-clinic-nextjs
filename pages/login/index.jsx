import { useState } from 'react'
import {
  Box,
  Grid,
  FormControl,
  InputLabel,
  OutlinedInput,
  InputAdornment,
  IconButton,
  TextField,
  Button
} from '@material-ui/core'
import { VisibilityOff, Visibility } from '@material-ui/icons'
import { signInWithCustomToken } from 'firebase/auth'
import { FirebaseAuth } from '@services/firebase'
import { useRouter } from 'next/router'

import styles from '../../styles/shared/Login.module.scss'

const Login = () => {
  const initialValues = {
    dni: '',
    password: '',
    showPassword: false
  }

  const [values, setValues] = useState(initialValues)
  const router = useRouter()

  const handleOnSubmit = async (e) => {
    e.preventDefault()
    // petition post fetch
    const req = await fetch('http://localhost:3000/api/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        dni: values.dni,
        password: values.password,
        type: 'patient'
      })
    })

    const { token } = await req.json()

    // eslint-disable-next-line no-console
    console.log(token)

    signInWithCustomToken(FirebaseAuth, token).then(() => {
      router.replace('/')
    })
  }

  return (
    <Box width="100vw" height="100vh">
      <Grid container spacing={0} className={styles.gridBox}>
        <Grid item md={8}>
          <h1>images</h1>
        </Grid>
        <Grid item md={4} className={styles.gridContent}>
          <Grid
            container
            spacing={0}
            width="100%"
            className={styles.gridContentLogin}
          >
            <form onSubmit={handleOnSubmit}>
              <Grid item md={12} className={styles.gridItem}>
                <TextField
                  fullWidth
                  size="small"
                  width="100%"
                  variant="outlined"
                  label="Correo"
                  type="number"
                  value={values.dni}
                  onChange={(e) => setValues({ ...values, dni: e.target.value })}
                />
              </Grid>
              <Grid item md={12} className={styles.gridItem}>
                <FormControl variant="outlined" size="small" fullWidth>
                  <InputLabel htmlFor="outlined-adornment-password">
                    Password
                  </InputLabel>
                  <OutlinedInput
                    fullWidth
                    id="outlined-adornment-password"
                    type={values.showPassword ? 'text' : 'password'}
                    value={values.password}
                    onChange={(e) => setValues({ ...values, password: e.target.value })}
                    endAdornment={(
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          edge="end"
                          onClick={() => setValues({
                            ...values,
                            showPassword: !values.showPassword
                          })}
                        >
                          {values.showPassword
                            ? (
                            <VisibilityOff />
                              )
                            : (
                            <Visibility />
                              )}
                        </IconButton>
                      </InputAdornment>
                    )}
                    label="Password"
                  />
                </FormControl>
              </Grid>
              <Grid item md={12} className={styles.gridItem}>
                <Button fullWidth variant="outlined" type="submit">
                  Iniciar Sesión
                </Button>
              </Grid>
            </form>
          </Grid>
        </Grid>
      </Grid>
    </Box>
  )
}

export default Login
