import { useEffect } from 'react'
import PropTypes from 'prop-types'
import Head from 'next/head'
import { ThemeProvider, StyledEngineProvider } from '@material-ui/core/styles'
import { CssBaseline } from '@material-ui/core'
import theme from '@styles/theme'
import GlobalProvider from '../context'
import MeetingProvider from '../context/meeting/meetingProvider'

const MyApp = ({
  Component,
  pageProps
}) => {
  useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side')
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles)
    }
  }, [])

  return (
    <>
      <Head>
        <title>Clinca San Carlos</title>
        <meta
          name="viewport"
          content="initial-scale=1, width=device-width"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <GlobalProvider>
            <MeetingProvider>
              <Component {...pageProps} />
            </MeetingProvider>
          </GlobalProvider>
        </ThemeProvider>
      </StyledEngineProvider>
    </>
  )
}

MyApp.propTypes = {
  Component: PropTypes.elementType.isRequired,
  pageProps: PropTypes.object.isRequired
}

export default MyApp
