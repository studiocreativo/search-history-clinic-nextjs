import { initializeApp } from 'firebase/app'
import { getFirestore } from 'firebase/firestore'
import { getAuth } from 'firebase/auth'

const firebaseConfig = {
  apiKey: 'AIzaSyA9MwNg0HtI7zxd8yfXE4yBsqwfP6S9h3o',
  authDomain: 'virtual-clinic-740ca.firebaseapp.com',
  databaseURL: 'https://virtual-clinic-740ca.firebaseio.com',
  projectId: 'virtual-clinic-740ca',
  storageBucket: 'virtual-clinic-740ca.appspot.com',
  messagingSenderId: '1074600012793',
  appId: '1:1074600012793:web:3e6f00cd5800252029ab28',
  measurementId: 'G-088RXXK3JH'
}

const app = initializeApp(firebaseConfig)
export const FirebaseFirestore = getFirestore(app)
export const FirebaseAuth = getAuth(app)

export default app
