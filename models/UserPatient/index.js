import UserBase from '../UserBase'

const generatorNumberHistory = (index) => {
  const numberHistory = index.toString().padStart(6, '0')
  const today = new Date()
  return `CSC-${today.getFullYear()}-${numberHistory}`
}

class UserPatient extends UserBase {
  constructor (data) {
    const {
      numberHistory, historyClinic, meetings, bloodType, occupation
    } = data
    super(data)

    this.access = 'patient'
    this.type = 'patient'

    this.numberHistory = numberHistory ?? null
    this.historyClinic = historyClinic ?? []
    this.refHistoryClinic = generatorNumberHistory(numberHistory)
    this.meetings = meetings ?? []

    this.bloodType = bloodType ?? ''
    this.occupation = occupation ?? ''
  }

  toJSON () {
    return {
      access: this.access,
      type: this.type,
      numberHistory: this.numberHistory,
      refHistoryClinic: this.refHistoryClinic,
      bloodType: this.bloodType,
      occupation: this.occupation,
      ...super.toJSON()
    }
  }
}

export default UserPatient
