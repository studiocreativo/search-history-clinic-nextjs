import UserBase from '@models/UserBase'

export default interface UserPatient extends UserBase {
  access: string;
  type: string;
  numberHistory: number;
  historyClinic: Array;
  refHistoryClinic: string;
  meetings: Array;
  bloodType: string;
  occupation: string;
}
