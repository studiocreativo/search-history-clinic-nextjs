export default interface UserReniec {
  dni: string;
  fullname: string;
  shortname: string;
  names: string;
  firstSurname: string;
  secondSurname: string;
  dateOfBirth: Date;
  civilStatus: string;
  gender: string;
  age: number;
  address: string;
  addressComplete: string;
  department: string;
  province: string;
  district: string;
  ubigeo: Array<string>;
  photo: string;
}
