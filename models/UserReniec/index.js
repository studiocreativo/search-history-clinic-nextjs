import dayjs from '../../utils/dayjs'

class UserReniec {
  constructor ({
    dni,
    names,
    firtsSurname,
    secondSurname,
    dateOfBirth,
    civilStatus,
    gender,
    address,
    department,
    province,
    district,
    ubigeo,
    photo
  }) {
    this.dni = dni
    this.names = names
    this.firtsSurname = firtsSurname
    this.secondSurname = secondSurname
    this.dateOfBirth = dayjs(dateOfBirth)
    this.fecha = this.dateOfBirth.format('DD-MM-YYYY')
    this.civilStatus = civilStatus
    this.gender = gender
    this.address = address
    this.department = department
    this.province = province
    this.district = district
    this.ubigeo = ubigeo
    this.photo = photo
  }

  get shortname () {
    return `${this.names.split(' ')[0]} ${this.firtsSurname}`
  }

  get fullname () {
    return `${this.names} ${this.firtsSurname} ${this.secondSurname}`
  }

  get addressComplete () {
    return `${this.department}, ${this.province}, ${this.district}, ${this.address}`
  }

  get age () {
    const today = dayjs()
    return today.diff(this.dateOfBirth, 'years', true)
  }

  toJSON () {
    return {
      dni: this.dni,
      names: this.names,
      firtsSurname: this.firtsSurname,
      secondSurname: this.secondSurname,
      dateOfBirth: this.dateOfBirth.format('YYYY-MM-DDTHH:mm:ssZ'),
      civilStatus: this.civilStatus,
      gender: this.gender,
      address: this.address,
      department: this.department,
      province: this.province,
      district: this.district,
      ubigeo: this.ubigeo,
      photo: this.photo
    }
  }
}

export default UserReniec
