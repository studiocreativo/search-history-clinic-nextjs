import dayjs from '../../utils/dayjs'

class Meeting {
  constructor (data) {
    const {
      patient,
      doctor,
      attentionDate,
      ailments,
      status
    } = data

    this.patient = patient
    this.doctor = doctor
    this.attentionDate = attentionDate
    this.ailments = ailments
    this.status = status
    this.createAt = new Date()
    this.updateAt = new Date()
  }

  toJSON () {
    return {
      patient: this.patient,
      doctor: this.doctor,
      attentionDate: dayjs(this.attentionDate).format(),
      ailments: this.ailments,
      status: parseInt(this.status, 10),
      createAt: this.createAt,
      updateAt: this.updateAt
    }
  }
}

export default Meeting
