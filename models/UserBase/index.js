import UserReniec from '../UserReniec'

const CLIENT_DOMAIN = process.env.CLIENT_DOMAIN || 'example.com'

class UserBase extends UserReniec {
  constructor (data) {
    const { dni, phones, createdAt } = data
    super(data)

    this.id = dni

    this.email = `${dni}@${CLIENT_DOMAIN}`
    this.password = dni

    this.phones = phones ?? []

    this.createdAt = createdAt ? new Date(createdAt) : new Date()
    this.updatedAt = new Date()

    this.inSession = false
    this.lastSession = null
  };

  toJSON () {
    return {
      id: this.id,
      email: this.email,
      password: this.password,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      inSession: this.inSession,
      lastSession: this.lastSession,
      phones: this.phones,
      ...super.toJSON()
    }
  };
}

export default UserBase
