import UserReniec from '@models/UserReniec'

export default interface UserBase extends UserReniec {
  id: string;
  email: string;
  password: string;
  phones: Array<string>;
  createdAt: Date;
  updatedAt: Date;
  inSession: boolean;
  lastSession: Date;
}
