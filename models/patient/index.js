import dayjs from '../../utils/dayjs'

export default function Patient (arg) {
  this.dni = arg.dni
  this.names = arg.names
  this.firtsSurname = arg.firtsSurname
  this.secondSurname = arg.secondSurname
  this.gender = arg.gender
  this.dateOfBirth = dayjs(arg.dateOfBirth)
  this.department = arg.department
  this.province = arg.province
  this.district = arg.district
  this.address = arg.address
  this.civilStatus = arg.civilStatus
  this.numberHistory = arg.numberHistory
  this.occupation = arg.occupation
  this.age = arg.age
  this.photo = arg.photo
  this.type = arg.type | 'patient'
  this.ubigeo = arg.ubigeo
  this.updatedAt = arg.updatedAt | dayjs()

  this.getPatient = function () {
    return {
      access: 'patient',
      bloodType: '',
      createdAt: dayjs(),
      id: this.dni,
      dni: this.dni,
      names: this.names,
      firtsSurname: this.firtsSurname,
      secondSurname: this.secondSurname,
      shortname: this.getShortname(),
      fullname: `${this.names} ${this.firtsSurname} ${this.secondSurname}`,
      email: this.getEmail(),
      password: this.dni,
      gender: this.gender,
      dateOfBirth: dayjs(this.dateOfBirth),
      department: this.department,
      province: this.province,
      district: this.district,
      address: this.address,
      civilStatus: this.civilStatus,
      addressComplete: this.getAddressComplete(),
      inSession: false,
      lastSession: null,
      numberHistory: this.numberHistory,
      occupation: '',
      age: this.age,
      photo: this.photo ? `data:image/jpg;base64, ${this.photo}` : null,
      refHistoryClinic: this.getRefHistoryClinic(),
      type: this.type,
      ubigeo: this.ubigeo,
      updatedAt: this.updatedAt
    }
  }

  this.getfullname = () => `${this.names} ${this.firtsSurname} ${this.secondSurname}`

  this.getEmail = () => `${this.dni}@clinicasancarlos.com.pe`

  this.getAddressComplete = () => `${this.address} ${this.department} ${this.province} ${this.district}`

  this.getShortname = () => {
    const a = this.names.split(' ')
    return `${a[0]} ${this.firtsSurname}`
  }

  this.getRefHistoryClinic = () => `CSC-${dayjs().year()}-${this.numberHistory.toString().padStart(6, 0)}`
}
