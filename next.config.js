module.exports = {
  reactStrictMode: true,
  images: {
    domains: ['thumbs.dreamstime.com']
  },
  eslint: {
    dirs: [
      'pages',
      'components',
      'models',
      'context',
      'features',
      'helpers',
      'services',
      'utils'
    ]
  }
}
