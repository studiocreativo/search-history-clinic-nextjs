const colors = [
  {
    color: 'primary',
    value: '#02A7E1'
  },
  {
    color: 'secondary',
    value: '#084B83'
  },
  {
    color: 'success',
    value: '#5DFDCB'
  },
  {
    color: 'warning',
    value: '#FFC857'
  },
  {
    color: 'danger',
    value: '#FF5E5B'
  },
  {
    color: 'black',
    value: '#08090A'
  }
]

function hexToRGB (hex) {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
  if (result) {
    const r = parseInt(result[1], 16)
    const g = parseInt(result[2], 16)
    const b = parseInt(result[3], 16)
    return `${r},${g},${b}`
  }
  return null
}
// eslint-disable-next-line import/prefer-default-export
export function getColor (color) {
  return hexToRGB(colors.find((el) => el.color === color).value)
}
