// eslint-disable-next-line import/prefer-default-export
export function validateDNI (dni) {
  if (dni === undefined) {
    return {
      error: true,
      msg: 'DNI no válido.'
    }
  }
  if (dni.length > 8) {
    return {
      error: true,
      msg: 'El dni no debe tener más de 8 digitos.'
    }
  }
  return {
    error: false,
    msg: null
  }
}
