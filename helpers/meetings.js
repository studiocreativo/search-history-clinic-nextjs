// eslint-disable-next-line import/prefer-default-export
export function statusCode (status) {
  switch (status) {
    case 1:
      return {
        color: 'primary',
        text: 'En espera'
      }
    case 2:
      return {
        color: 'warning',
        text: 'En proceso'
      }
    case 3:
      return {
        color: 'success',
        text: 'Completado'
      }
    case 4:
      return {
        color: 'danger',
        text: 'Cancelado'
      }
    default:
      return {
        color: 'primary',
        text: 'En espera'
      }
  }
}
