import {
  collection, getDocs, doc, setDoc, onSnapshot, query
} from 'firebase/firestore'
import db from './auth'

const getData = async (options) => {
  const queryCol = collection(db, options.collection)
  const querySnapshot = await getDocs(queryCol)
  const queryList = querySnapshot.docs.map((el) => el.data())
  return queryList
}

export const onChangeData = (options) => {
  const q = query(collection(db, options.collection))
  onSnapshot(q, (querySnapshot) => {
    const response = []
    // eslint-disable-next-line no-shadow
    querySnapshot.forEach((doc) => {
      response.push(doc.data())
    })
    options.callback(response)
  })
}

// eslint-disable-next-line no-return-await
const setData = async (options) => (await setDoc(doc(db, options.collection), options.body))

// eslint-disable-next-line consistent-return
async function request (method, options) {
  switch (method) {
    case 'GET':
      // eslint-disable-next-line no-return-await
      return await getData(options)
    case 'POST':
      // eslint-disable-next-line no-return-await
      return await setData(options)
    case 'PUT':
      return 'put'
    default:
      break
  }
}

export default request
