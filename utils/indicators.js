const colorizedAge = (age) => {
  if (age > 18 && age <= 25) {
    return 'success'
  }

  if (age > 25 && age <= 45) {
    return 'warning'
  }

  if (age > 45) {
    return 'danger'
  }

  return 'primary'
}

export {
  colorizedAge
}
