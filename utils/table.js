export const createHeaderCell = (id, label, aling = 'left') => ({
  id,
  label,
  aling
})

export const normalizeHeaderToFilterBy = (header, skipFields) => {
  let output = header.map(({ label, id }) => ({
    label,
    value: id
  }))

  if (skipFields && skipFields.length !== 0) {
    output = output.filter(({ value }) => skipFields.indexOf(value) === -1)
  }

  return output
}
