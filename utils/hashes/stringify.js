import validate from './validate'

const byteToHex = []

for (let i = 0; i < 256; i++) {
  byteToHex.push((i + 0x100).toString(16).substr(1))
}

const stringify = (arr, offset = 0) => {
  const hash = (
    byteToHex[arr[offset + 0]] +
    byteToHex[arr[offset + 1]] +
    byteToHex[arr[offset + 2]] +
    byteToHex[arr[offset + 3]]
  ).toLowerCase()

  if (!validate(hash)) {
    throw TypeError('Stringified invalid hash')
  }

  return hash
}

export default stringify
