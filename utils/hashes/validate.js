const validate = (hash) => typeof hash === 'string'

export default validate
