import crypto from 'crypto'

const rnd8Pool = new Uint8Array(256)
let poolPtr = rnd8Pool.length

export default function rng () {
  if (poolPtr > rnd8Pool.length - 16) {
    crypto.randomFillSync(rnd8Pool)
    poolPtr = 0
  }
  return rnd8Pool.slice(poolPtr, (poolPtr += 16))
}
