/* eslint-disable no-unused-vars */
import { initializeApp } from 'firebase/app'
import { getFirestore } from 'firebase/firestore'
import {
  getAuth,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  onAuthStateChanged,
  signOut
} from 'firebase/auth'

const firebaseConfig = {
  apiKey: 'AIzaSyA9MwNg0HtI7zxd8yfXE4yBsqwfP6S9h3o',
  authDomain: 'virtual-clinic-740ca.firebaseapp.com',
  databaseURL: 'https://virtual-clinic-740ca.firebaseio.com',
  projectId: 'virtual-clinic-740ca',
  storageBucket: 'virtual-clinic-740ca.appspot.com',
  messagingSenderId: '1074600012793',
  appId: '1:1074600012793:web:3e6f00cd5800252029ab28',
  measurementId: 'G-088RXXK3JH'
}

const app = initializeApp(firebaseConfig)

const db = getFirestore(app)

export default db

// USER AUTH

/* export function login(email, password) {
  const auth = getAuth();
  createUserWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      // Signed in
      const { user } = userCredential;
      console.log(user);
      // ...
    })
    .catch((error) => {
      const errorCode = error.code;
      console.log(errorCode);
      const errorMessage = error.message;
      // ..
    });
} */

const auth = getAuth()

export function login (email, password) {
  return signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      // Signed in
      const { user } = userCredential
      return user
      // ...
    })
    .catch((error) => {
      const errorCode = error.code
      const errorMessage = error.message
      console.log(error)
    })
}

export function getUser (callback) {
  return onAuthStateChanged(auth, callback)
}

export function closeSession () {
  signOut(auth)
}
