import departments from './departments'
import provinces from './provinces'
import districts from './districts'

const getProvinces = (department) => {
  const existsDepartment = Boolean(department)
  let output = []
  if (existsDepartment) {
    output = [...provinces[department.id]]
  }
  return output
}

const getDistricts = (province) => {
  const existsProvince = Boolean(province)
  let output = []
  if (existsProvince) {
    output = [...districts[province.id]]
  }
  return output
}

const Ubigeo = {
  departments,
  getProvinces,
  getDistricts
}

export default Ubigeo
