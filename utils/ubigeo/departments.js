const data = [
  {
    id: '2534',
    name: 'AMAZONAS',
    code: '01'
  },
  {
    id: '2625',
    name: 'ANCASH',
    code: '02'
  },
  {
    id: '2812',
    name: 'APURIMAC',
    code: '03'
  },
  {
    id: '2900',
    name: 'AREQUIPA',
    code: '04'
  },
  {
    id: '3020',
    name: 'AYACUCHO',
    code: '05'
  },
  {
    id: '3143',
    name: 'CAJAMARCA',
    code: '06'
  },
  {
    id: '3292',
    name: 'CUSCO',
    code: '08'
  },
  {
    id: '3414',
    name: 'HUANCAVELICA',
    code: '09'
  },
  {
    id: '3518',
    name: 'HUANUCO',
    code: '10'
  },
  {
    id: '3606',
    name: 'ICA',
    code: '11'
  },
  {
    id: '3655',
    name: 'JUNIN',
    code: '12'
  },
  {
    id: '3788',
    name: 'LA LIBERTAD',
    code: '13'
  },
  {
    id: '3884',
    name: 'LAMBAYEQUE',
    code: '14'
  },
  {
    id: '3926',
    name: 'LIMA',
    code: '15'
  },
  {
    id: '4108',
    name: 'LORETO',
    code: '16'
  },
  {
    id: '4165',
    name: 'MADRE DE DIOS',
    code: '17'
  },
  {
    id: '4180',
    name: 'MOQUEGUA',
    code: '18'
  },
  {
    id: '4204',
    name: 'PASCO',
    code: '19'
  },
  {
    id: '4236',
    name: 'PIURA',
    code: '20'
  },
  {
    id: '4309',
    name: 'PUNO',
    code: '21'
  },
  {
    id: '4431',
    name: 'SAN MARTIN',
    code: '22'
  },
  {
    id: '4519',
    name: 'TACNA',
    code: '23'
  },
  {
    id: '4551',
    name: 'TUMBES',
    code: '24'
  },
  {
    id: '4567',
    name: 'UCAYALI',
    code: '25'
  }
]

export default data
