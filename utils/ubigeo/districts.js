const data = {
  2535: [
    {
      id: '2537',
      name: 'ASUNCION',
      code: '02',
      parentId: '2535'
    },
    {
      id: '2538',
      name: 'BALSAS',
      code: '03',
      parentId: '2535'
    },
    {
      id: '2536',
      name: 'CHACHAPOYAS',
      code: '01',
      parentId: '2535'
    },
    {
      id: '2539',
      name: 'CHETO',
      code: '04',
      parentId: '2535'
    },
    {
      id: '2540',
      name: 'CHILIQUIN',
      code: '05',
      parentId: '2535'
    },
    {
      id: '2541',
      name: 'CHUQUIBAMBA',
      code: '06',
      parentId: '2535'
    },
    {
      id: '2542',
      name: 'GRANADA',
      code: '07',
      parentId: '2535'
    },
    {
      id: '2543',
      name: 'HUANCAS',
      code: '08',
      parentId: '2535'
    },
    {
      id: '2544',
      name: 'LA JALCA',
      code: '09',
      parentId: '2535'
    },
    {
      id: '2545',
      name: 'LEIMEBAMBA',
      code: '10',
      parentId: '2535'
    },
    {
      id: '2546',
      name: 'LEVANTO',
      code: '11',
      parentId: '2535'
    },
    {
      id: '2547',
      name: 'MAGDALENA',
      code: '12',
      parentId: '2535'
    },
    {
      id: '2548',
      name: 'MARISCAL CASTILLA',
      code: '13',
      parentId: '2535'
    },
    {
      id: '2549',
      name: 'MOLINOPAMPA',
      code: '14',
      parentId: '2535'
    },
    {
      id: '2550',
      name: 'MONTEVIDEO',
      code: '15',
      parentId: '2535'
    },
    {
      id: '2551',
      name: 'OLLEROS',
      code: '16',
      parentId: '2535'
    },
    {
      id: '2552',
      name: 'QUINJALCA',
      code: '17',
      parentId: '2535'
    },
    {
      id: '2553',
      name: 'SAN FRANCISCO DE DAGUAS',
      code: '18',
      parentId: '2535'
    },
    {
      id: '2554',
      name: 'SAN ISIDRO DE MAINO',
      code: '19',
      parentId: '2535'
    },
    {
      id: '2555',
      name: 'SOLOCO',
      code: '20',
      parentId: '2535'
    },
    {
      id: '2556',
      name: 'SONCHE',
      code: '21',
      parentId: '2535'
    }
  ],
  2557: [
    {
      id: '2559',
      name: 'ARAMANGO',
      code: '02',
      parentId: '2557'
    },
    {
      id: '2560',
      name: 'COPALLIN',
      code: '03',
      parentId: '2557'
    },
    {
      id: '2561',
      name: 'EL PARCO',
      code: '04',
      parentId: '2557'
    },
    {
      id: '2562',
      name: 'IMAZA',
      code: '05',
      parentId: '2557'
    },
    {
      id: '2558',
      name: 'LA PECA',
      code: '01',
      parentId: '2557'
    }
  ],
  2563: [
    {
      id: '2567',
      name: 'CHISQUILLA',
      code: '04',
      parentId: '2563'
    },
    {
      id: '2568',
      name: 'CHURUJA',
      code: '05',
      parentId: '2563'
    },
    {
      id: '2565',
      name: 'COROSHA',
      code: '02',
      parentId: '2563'
    },
    {
      id: '2566',
      name: 'CUISPES',
      code: '03',
      parentId: '2563'
    },
    {
      id: '2569',
      name: 'FLORIDA',
      code: '06',
      parentId: '2563'
    },
    {
      id: '2575',
      name: 'JAZAN',
      code: '12',
      parentId: '2563'
    },
    {
      id: '2564',
      name: 'JUMBILLA',
      code: '01',
      parentId: '2563'
    },
    {
      id: '2570',
      name: 'RECTA',
      code: '07',
      parentId: '2563'
    },
    {
      id: '2571',
      name: 'SAN CARLOS',
      code: '08',
      parentId: '2563'
    },
    {
      id: '2572',
      name: 'SHIPASBAMBA',
      code: '09',
      parentId: '2563'
    },
    {
      id: '2573',
      name: 'VALERA',
      code: '10',
      parentId: '2563'
    },
    {
      id: '2574',
      name: 'YAMBRASBAMBA',
      code: '11',
      parentId: '2563'
    }
  ],
  2576: [
    {
      id: '2578',
      name: 'EL CENEPA',
      code: '02',
      parentId: '2576'
    },
    {
      id: '2577',
      name: 'NIEVA',
      code: '01',
      parentId: '2576'
    },
    {
      id: '2579',
      name: 'RIO SANTIAGO',
      code: '03',
      parentId: '2576'
    }
  ],
  2580: [
    {
      id: '2582',
      name: 'CAMPORREDONDO',
      code: '02',
      parentId: '2580'
    },
    {
      id: '2583',
      name: 'COCABAMBA',
      code: '03',
      parentId: '2580'
    },
    {
      id: '2584',
      name: 'COLCAMAR',
      code: '04',
      parentId: '2580'
    },
    {
      id: '2585',
      name: 'CONILA',
      code: '05',
      parentId: '2580'
    },
    {
      id: '2586',
      name: 'INGUILPATA',
      code: '06',
      parentId: '2580'
    },
    {
      id: '2581',
      name: 'LAMUD',
      code: '01',
      parentId: '2580'
    },
    {
      id: '2587',
      name: 'LONGUITA',
      code: '07',
      parentId: '2580'
    },
    {
      id: '2588',
      name: 'LONYA CHICO',
      code: '08',
      parentId: '2580'
    },
    {
      id: '2589',
      name: 'LUYA',
      code: '09',
      parentId: '2580'
    },
    {
      id: '2590',
      name: 'LUYA VIEJO',
      code: '10',
      parentId: '2580'
    },
    {
      id: '2591',
      name: 'MARIA',
      code: '11',
      parentId: '2580'
    },
    {
      id: '2592',
      name: 'OCALLI',
      code: '12',
      parentId: '2580'
    },
    {
      id: '2593',
      name: 'OCUMAL',
      code: '13',
      parentId: '2580'
    },
    {
      id: '2594',
      name: 'PISUQUIA',
      code: '14',
      parentId: '2580'
    },
    {
      id: '2595',
      name: 'PROVIDENCIA',
      code: '15',
      parentId: '2580'
    },
    {
      id: '2596',
      name: 'SAN CRISTOBAL',
      code: '16',
      parentId: '2580'
    },
    {
      id: '2597',
      name: 'SAN FRANCISCO DEL YESO',
      code: '17',
      parentId: '2580'
    },
    {
      id: '2598',
      name: 'SAN JERONIMO',
      code: '18',
      parentId: '2580'
    },
    {
      id: '2599',
      name: 'SAN JUAN DE LOPECANCHA',
      code: '19',
      parentId: '2580'
    },
    {
      id: '2600',
      name: 'SANTA CATALINA',
      code: '20',
      parentId: '2580'
    },
    {
      id: '2601',
      name: 'SANTO TOMAS',
      code: '21',
      parentId: '2580'
    },
    {
      id: '2602',
      name: 'TINGO',
      code: '22',
      parentId: '2580'
    },
    {
      id: '2603',
      name: 'TRITA',
      code: '23',
      parentId: '2580'
    }
  ],
  2604: [
    {
      id: '2606',
      name: 'CHIRIMOTO',
      code: '02',
      parentId: '2604'
    },
    {
      id: '2607',
      name: 'COCHAMAL',
      code: '03',
      parentId: '2604'
    },
    {
      id: '2608',
      name: 'HUAMBO',
      code: '04',
      parentId: '2604'
    },
    {
      id: '2609',
      name: 'LIMABAMBA',
      code: '05',
      parentId: '2604'
    },
    {
      id: '2610',
      name: 'LONGAR',
      code: '06',
      parentId: '2604'
    },
    {
      id: '2611',
      name: 'MARISCAL BENAVIDES',
      code: '07',
      parentId: '2604'
    },
    {
      id: '2612',
      name: 'MILPUC',
      code: '08',
      parentId: '2604'
    },
    {
      id: '2613',
      name: 'OMIA',
      code: '09',
      parentId: '2604'
    },
    {
      id: '2605',
      name: 'SAN NICOLAS',
      code: '01',
      parentId: '2604'
    },
    {
      id: '2614',
      name: 'SANTA ROSA',
      code: '10',
      parentId: '2604'
    },
    {
      id: '2615',
      name: 'TOTORA',
      code: '11',
      parentId: '2604'
    },
    {
      id: '2616',
      name: 'VISTA ALEGRE',
      code: '12',
      parentId: '2604'
    }
  ],
  2617: [
    {
      id: '2618',
      name: 'BAGUA GRANDE',
      code: '01',
      parentId: '2617'
    },
    {
      id: '2619',
      name: 'CAJARURO',
      code: '02',
      parentId: '2617'
    },
    {
      id: '2620',
      name: 'CUMBA',
      code: '03',
      parentId: '2617'
    },
    {
      id: '2621',
      name: 'EL MILAGRO',
      code: '04',
      parentId: '2617'
    },
    {
      id: '2622',
      name: 'JAMALCA',
      code: '05',
      parentId: '2617'
    },
    {
      id: '2623',
      name: 'LONYA GRANDE',
      code: '06',
      parentId: '2617'
    },
    {
      id: '2624',
      name: 'YAMON',
      code: '07',
      parentId: '2617'
    }
  ],
  2626: [
    {
      id: '2628',
      name: 'COCHABAMBA',
      code: '02',
      parentId: '2626'
    },
    {
      id: '2629',
      name: 'COLCABAMBA',
      code: '03',
      parentId: '2626'
    },
    {
      id: '2630',
      name: 'HUANCHAY',
      code: '04',
      parentId: '2626'
    },
    {
      id: '2627',
      name: 'HUARAZ',
      code: '01',
      parentId: '2626'
    },
    {
      id: '2631',
      name: 'INDEPENDENCIA',
      code: '05',
      parentId: '2626'
    },
    {
      id: '2632',
      name: 'JANGAS',
      code: '06',
      parentId: '2626'
    },
    {
      id: '2633',
      name: 'LA LIBERTAD',
      code: '07',
      parentId: '2626'
    },
    {
      id: '2634',
      name: 'OLLEROS',
      code: '08',
      parentId: '2626'
    },
    {
      id: '2635',
      name: 'PAMPAS',
      code: '09',
      parentId: '2626'
    },
    {
      id: '2636',
      name: 'PARIACOTO',
      code: '10',
      parentId: '2626'
    },
    {
      id: '2637',
      name: 'PIRA',
      code: '11',
      parentId: '2626'
    },
    {
      id: '2638',
      name: 'TARICA',
      code: '12',
      parentId: '2626'
    }
  ],
  2639: [
    {
      id: '2640',
      name: 'AIJA',
      code: '01',
      parentId: '2639'
    },
    {
      id: '2641',
      name: 'CORIS',
      code: '02',
      parentId: '2639'
    },
    {
      id: '2642',
      name: 'HUACLLAN',
      code: '03',
      parentId: '2639'
    },
    {
      id: '2643',
      name: 'LA MERCED',
      code: '04',
      parentId: '2639'
    },
    {
      id: '2644',
      name: 'SUCCHA',
      code: '05',
      parentId: '2639'
    }
  ],
  2645: [
    {
      id: '2647',
      name: 'ACZO',
      code: '02',
      parentId: '2645'
    },
    {
      id: '2648',
      name: 'CHACCHO',
      code: '03',
      parentId: '2645'
    },
    {
      id: '2649',
      name: 'CHINGAS',
      code: '04',
      parentId: '2645'
    },
    {
      id: '2646',
      name: 'LLAMELLIN',
      code: '01',
      parentId: '2645'
    },
    {
      id: '2650',
      name: 'MIRGAS',
      code: '05',
      parentId: '2645'
    },
    {
      id: '2651',
      name: 'SAN JUAN DE RONTOY',
      code: '06',
      parentId: '2645'
    }
  ],
  2652: [
    {
      id: '2654',
      name: 'ACOCHACA',
      code: '02',
      parentId: '2652'
    },
    {
      id: '2653',
      name: 'CHACAS',
      code: '01',
      parentId: '2652'
    }
  ],
  2655: [
    {
      id: '2657',
      name: 'ABELARDO PARDO LEZAMETA',
      code: '02',
      parentId: '2655'
    },
    {
      id: '2658',
      name: 'ANTONIO RAYMONDI',
      code: '03',
      parentId: '2655'
    },
    {
      id: '2659',
      name: 'AQUIA',
      code: '04',
      parentId: '2655'
    },
    {
      id: '2660',
      name: 'CAJACAY',
      code: '05',
      parentId: '2655'
    },
    {
      id: '2661',
      name: 'CANIS',
      code: '06',
      parentId: '2655'
    },
    {
      id: '2656',
      name: 'CHIQUIAN',
      code: '01',
      parentId: '2655'
    },
    {
      id: '2662',
      name: 'COLQUIOC',
      code: '07',
      parentId: '2655'
    },
    {
      id: '2663',
      name: 'HUALLANCA',
      code: '08',
      parentId: '2655'
    },
    {
      id: '2664',
      name: 'HUASTA',
      code: '09',
      parentId: '2655'
    },
    {
      id: '2665',
      name: 'HUAYLLACAYAN',
      code: '10',
      parentId: '2655'
    },
    {
      id: '2666',
      name: 'LA PRIMAVERA',
      code: '11',
      parentId: '2655'
    },
    {
      id: '2667',
      name: 'MANGAS',
      code: '12',
      parentId: '2655'
    },
    {
      id: '2668',
      name: 'PACLLON',
      code: '13',
      parentId: '2655'
    },
    {
      id: '2669',
      name: 'SAN MIGUEL DE CORPANQUI',
      code: '14',
      parentId: '2655'
    },
    {
      id: '2670',
      name: 'TICLLOS',
      code: '15',
      parentId: '2655'
    }
  ],
  2671: [
    {
      id: '2673',
      name: 'ACOPAMPA',
      code: '02',
      parentId: '2671'
    },
    {
      id: '2674',
      name: 'AMASHCA',
      code: '03',
      parentId: '2671'
    },
    {
      id: '2675',
      name: 'ANTA',
      code: '04',
      parentId: '2671'
    },
    {
      id: '2676',
      name: 'ATAQUERO',
      code: '05',
      parentId: '2671'
    },
    {
      id: '2672',
      name: 'CARHUAZ',
      code: '01',
      parentId: '2671'
    },
    {
      id: '2677',
      name: 'MARCARA',
      code: '06',
      parentId: '2671'
    },
    {
      id: '2678',
      name: 'PARIAHUANCA',
      code: '07',
      parentId: '2671'
    },
    {
      id: '2679',
      name: 'SAN MIGUEL DE ACO',
      code: '08',
      parentId: '2671'
    },
    {
      id: '2680',
      name: 'SHILLA',
      code: '09',
      parentId: '2671'
    },
    {
      id: '2681',
      name: 'TINCO',
      code: '10',
      parentId: '2671'
    },
    {
      id: '2682',
      name: 'YUNGAR',
      code: '11',
      parentId: '2671'
    }
  ],
  2683: [
    {
      id: '2684',
      name: 'SAN LUIS',
      code: '01',
      parentId: '2683'
    },
    {
      id: '2685',
      name: 'SAN NICOLAS',
      code: '02',
      parentId: '2683'
    },
    {
      id: '2686',
      name: 'YAUYA',
      code: '03',
      parentId: '2683'
    }
  ],
  2687: [
    {
      id: '2689',
      name: 'BUENA VISTA ALTA',
      code: '02',
      parentId: '2687'
    },
    {
      id: '2688',
      name: 'CASMA',
      code: '01',
      parentId: '2687'
    },
    {
      id: '2690',
      name: 'COMANDANTE NOEL',
      code: '03',
      parentId: '2687'
    },
    {
      id: '2691',
      name: 'YAUTAN',
      code: '04',
      parentId: '2687'
    }
  ],
  2692: [
    {
      id: '2694',
      name: 'ACO',
      code: '02',
      parentId: '2692'
    },
    {
      id: '2695',
      name: 'BAMBAS',
      code: '03',
      parentId: '2692'
    },
    {
      id: '2693',
      name: 'CORONGO',
      code: '01',
      parentId: '2692'
    },
    {
      id: '2696',
      name: 'CUSCA',
      code: '04',
      parentId: '2692'
    },
    {
      id: '2697',
      name: 'LA PAMPA',
      code: '05',
      parentId: '2692'
    },
    {
      id: '2698',
      name: 'YANAC',
      code: '06',
      parentId: '2692'
    },
    {
      id: '2699',
      name: 'YUPAN',
      code: '07',
      parentId: '2692'
    }
  ],
  2700: [
    {
      id: '2702',
      name: 'ANRA',
      code: '02',
      parentId: '2700'
    },
    {
      id: '2703',
      name: 'CAJAY',
      code: '03',
      parentId: '2700'
    },
    {
      id: '2704',
      name: 'CHAVIN DE HUANTAR',
      code: '04',
      parentId: '2700'
    },
    {
      id: '2705',
      name: 'HUACACHI',
      code: '05',
      parentId: '2700'
    },
    {
      id: '2706',
      name: 'HUACCHIS',
      code: '06',
      parentId: '2700'
    },
    {
      id: '2707',
      name: 'HUACHIS',
      code: '07',
      parentId: '2700'
    },
    {
      id: '2708',
      name: 'HUANTAR',
      code: '08',
      parentId: '2700'
    },
    {
      id: '2701',
      name: 'HUARI',
      code: '01',
      parentId: '2700'
    },
    {
      id: '2709',
      name: 'MASIN',
      code: '09',
      parentId: '2700'
    },
    {
      id: '2710',
      name: 'PAUCAS',
      code: '10',
      parentId: '2700'
    },
    {
      id: '2711',
      name: 'PONTO',
      code: '11',
      parentId: '2700'
    },
    {
      id: '2712',
      name: 'RAHUAPAMPA',
      code: '12',
      parentId: '2700'
    },
    {
      id: '2713',
      name: 'RAPAYAN',
      code: '13',
      parentId: '2700'
    },
    {
      id: '2714',
      name: 'SAN MARCOS',
      code: '14',
      parentId: '2700'
    },
    {
      id: '2715',
      name: 'SAN PEDRO DE CHANA',
      code: '15',
      parentId: '2700'
    },
    {
      id: '2716',
      name: 'UCO',
      code: '16',
      parentId: '2700'
    }
  ],
  2717: [
    {
      id: '2719',
      name: 'COCHAPETI',
      code: '02',
      parentId: '2717'
    },
    {
      id: '2720',
      name: 'CULEBRAS',
      code: '03',
      parentId: '2717'
    },
    {
      id: '2718',
      name: 'HUARMEY',
      code: '01',
      parentId: '2717'
    },
    {
      id: '2721',
      name: 'HUAYAN',
      code: '04',
      parentId: '2717'
    },
    {
      id: '2722',
      name: 'MALVAS',
      code: '05',
      parentId: '2717'
    }
  ],
  2723: [
    {
      id: '2724',
      name: 'CARAZ',
      code: '01',
      parentId: '2723'
    },
    {
      id: '2725',
      name: 'HUALLANCA',
      code: '02',
      parentId: '2723'
    },
    {
      id: '2726',
      name: 'HUATA',
      code: '03',
      parentId: '2723'
    },
    {
      id: '2727',
      name: 'HUAYLAS',
      code: '04',
      parentId: '2723'
    },
    {
      id: '2728',
      name: 'MATO',
      code: '05',
      parentId: '2723'
    },
    {
      id: '2729',
      name: 'PAMPAROMAS',
      code: '06',
      parentId: '2723'
    },
    {
      id: '2730',
      name: 'PUEBLO LIBRE',
      code: '07',
      parentId: '2723'
    },
    {
      id: '2731',
      name: 'SANTA CRUZ',
      code: '08',
      parentId: '2723'
    },
    {
      id: '2732',
      name: 'SANTO TORIBIO',
      code: '09',
      parentId: '2723'
    },
    {
      id: '2733',
      name: 'YURACMARCA',
      code: '10',
      parentId: '2723'
    }
  ],
  2734: [
    {
      id: '2736',
      name: 'CASCA',
      code: '02',
      parentId: '2734'
    },
    {
      id: '2737',
      name: 'ELEAZAR GUZMAN BARRON',
      code: '03',
      parentId: '2734'
    },
    {
      id: '2738',
      name: 'FIDEL OLIVAS ESCUDERO',
      code: '04',
      parentId: '2734'
    },
    {
      id: '2739',
      name: 'LLAMA',
      code: '05',
      parentId: '2734'
    },
    {
      id: '2740',
      name: 'LLUMPA',
      code: '06',
      parentId: '2734'
    },
    {
      id: '2741',
      name: 'LUCMA',
      code: '07',
      parentId: '2734'
    },
    {
      id: '2742',
      name: 'MUSGA',
      code: '08',
      parentId: '2734'
    },
    {
      id: '2735',
      name: 'PISCOBAMBA',
      code: '01',
      parentId: '2734'
    }
  ],
  2743: [
    {
      id: '2745',
      name: 'ACAS',
      code: '02',
      parentId: '2743'
    },
    {
      id: '2746',
      name: 'CAJAMARQUILLA',
      code: '03',
      parentId: '2743'
    },
    {
      id: '2747',
      name: 'CARHUAPAMPA',
      code: '04',
      parentId: '2743'
    },
    {
      id: '2748',
      name: 'COCHAS',
      code: '05',
      parentId: '2743'
    },
    {
      id: '2749',
      name: 'CONGAS',
      code: '06',
      parentId: '2743'
    },
    {
      id: '2750',
      name: 'LLIPA',
      code: '07',
      parentId: '2743'
    },
    {
      id: '2744',
      name: 'OCROS',
      code: '01',
      parentId: '2743'
    },
    {
      id: '2751',
      name: 'SAN CRISTOBAL DE RAJAN',
      code: '08',
      parentId: '2743'
    },
    {
      id: '2752',
      name: 'SAN PEDRO',
      code: '09',
      parentId: '2743'
    },
    {
      id: '2753',
      name: 'SANTIAGO DE CHILCAS',
      code: '10',
      parentId: '2743'
    }
  ],
  2754: [
    {
      id: '2756',
      name: 'BOLOGNESI',
      code: '02',
      parentId: '2754'
    },
    {
      id: '2755',
      name: 'CABANA',
      code: '01',
      parentId: '2754'
    },
    {
      id: '2757',
      name: 'CONCHUCOS',
      code: '03',
      parentId: '2754'
    },
    {
      id: '2758',
      name: 'HUACASCHUQUE',
      code: '04',
      parentId: '2754'
    },
    {
      id: '2759',
      name: 'HUANDOVAL',
      code: '05',
      parentId: '2754'
    },
    {
      id: '2760',
      name: 'LACABAMBA',
      code: '06',
      parentId: '2754'
    },
    {
      id: '2761',
      name: 'LLAPO',
      code: '07',
      parentId: '2754'
    },
    {
      id: '2762',
      name: 'PALLASCA',
      code: '08',
      parentId: '2754'
    },
    {
      id: '2763',
      name: 'PAMPAS',
      code: '09',
      parentId: '2754'
    },
    {
      id: '2764',
      name: 'SANTA ROSA',
      code: '10',
      parentId: '2754'
    },
    {
      id: '2765',
      name: 'TAUCA',
      code: '11',
      parentId: '2754'
    }
  ],
  2766: [
    {
      id: '2768',
      name: 'HUAYLLAN',
      code: '02',
      parentId: '2766'
    },
    {
      id: '2769',
      name: 'PAROBAMBA',
      code: '03',
      parentId: '2766'
    },
    {
      id: '2767',
      name: 'POMABAMBA',
      code: '01',
      parentId: '2766'
    },
    {
      id: '2770',
      name: 'QUINUABAMBA',
      code: '04',
      parentId: '2766'
    }
  ],
  2771: [
    {
      id: '2773',
      name: 'CATAC',
      code: '02',
      parentId: '2771'
    },
    {
      id: '2774',
      name: 'COTAPARACO',
      code: '03',
      parentId: '2771'
    },
    {
      id: '2775',
      name: 'HUAYLLAPAMPA',
      code: '04',
      parentId: '2771'
    },
    {
      id: '2776',
      name: 'LLACLLIN',
      code: '05',
      parentId: '2771'
    },
    {
      id: '2777',
      name: 'MARCA',
      code: '06',
      parentId: '2771'
    },
    {
      id: '2778',
      name: 'PAMPAS CHICO',
      code: '07',
      parentId: '2771'
    },
    {
      id: '2779',
      name: 'PARARIN',
      code: '08',
      parentId: '2771'
    },
    {
      id: '2772',
      name: 'RECUAY',
      code: '01',
      parentId: '2771'
    },
    {
      id: '2780',
      name: 'TAPACOCHA',
      code: '09',
      parentId: '2771'
    },
    {
      id: '2781',
      name: 'TICAPAMPA',
      code: '10',
      parentId: '2771'
    }
  ],
  2782: [
    {
      id: '2784',
      name: 'CACERES DEL PERU',
      code: '02',
      parentId: '2782'
    },
    {
      id: '2783',
      name: 'CHIMBOTE',
      code: '01',
      parentId: '2782'
    },
    {
      id: '2785',
      name: 'COISHCO',
      code: '03',
      parentId: '2782'
    },
    {
      id: '2786',
      name: 'MACATE',
      code: '04',
      parentId: '2782'
    },
    {
      id: '2787',
      name: 'MORO',
      code: '05',
      parentId: '2782'
    },
    {
      id: '2788',
      name: 'NEPEQA',
      code: '06',
      parentId: '2782'
    },
    {
      id: '2791',
      name: 'NUEVO CHIMBOTE',
      code: '09',
      parentId: '2782'
    },
    {
      id: '2789',
      name: 'SAMANCO',
      code: '07',
      parentId: '2782'
    },
    {
      id: '2790',
      name: 'SANTA',
      code: '08',
      parentId: '2782'
    }
  ],
  2792: [
    {
      id: '2794',
      name: 'ACOBAMBA',
      code: '02',
      parentId: '2792'
    },
    {
      id: '2795',
      name: 'ALFONSO UGARTE',
      code: '03',
      parentId: '2792'
    },
    {
      id: '2796',
      name: 'CASHAPAMPA',
      code: '04',
      parentId: '2792'
    },
    {
      id: '2797',
      name: 'CHINGALPO',
      code: '05',
      parentId: '2792'
    },
    {
      id: '2798',
      name: 'HUAYLLABAMBA',
      code: '06',
      parentId: '2792'
    },
    {
      id: '2799',
      name: 'QUICHES',
      code: '07',
      parentId: '2792'
    },
    {
      id: '2800',
      name: 'RAGASH',
      code: '08',
      parentId: '2792'
    },
    {
      id: '2801',
      name: 'SAN JUAN',
      code: '09',
      parentId: '2792'
    },
    {
      id: '2802',
      name: 'SICSIBAMBA',
      code: '10',
      parentId: '2792'
    },
    {
      id: '2793',
      name: 'SIHUAS',
      code: '01',
      parentId: '2792'
    }
  ],
  2803: [
    {
      id: '2805',
      name: 'CASCAPARA',
      code: '02',
      parentId: '2803'
    },
    {
      id: '2806',
      name: 'MANCOS',
      code: '03',
      parentId: '2803'
    },
    {
      id: '2807',
      name: 'MATACOTO',
      code: '04',
      parentId: '2803'
    },
    {
      id: '2808',
      name: 'QUILLO',
      code: '05',
      parentId: '2803'
    },
    {
      id: '2809',
      name: 'RANRAHIRCA',
      code: '06',
      parentId: '2803'
    },
    {
      id: '2810',
      name: 'SHUPLUY',
      code: '07',
      parentId: '2803'
    },
    {
      id: '2811',
      name: 'YANAMA',
      code: '08',
      parentId: '2803'
    },
    {
      id: '2804',
      name: 'YUNGAY',
      code: '01',
      parentId: '2803'
    }
  ],
  2813: [
    {
      id: '2814',
      name: 'ABANCAY',
      code: '01',
      parentId: '2813'
    },
    {
      id: '2815',
      name: 'CHACOCHE',
      code: '02',
      parentId: '2813'
    },
    {
      id: '2816',
      name: 'CIRCA',
      code: '03',
      parentId: '2813'
    },
    {
      id: '2817',
      name: 'CURAHUASI',
      code: '04',
      parentId: '2813'
    },
    {
      id: '2818',
      name: 'HUANIPACA',
      code: '05',
      parentId: '2813'
    },
    {
      id: '2819',
      name: 'LAMBRAMA',
      code: '06',
      parentId: '2813'
    },
    {
      id: '2820',
      name: 'PICHIRHUA',
      code: '07',
      parentId: '2813'
    },
    {
      id: '2821',
      name: 'SAN PEDRO DE CACHORA',
      code: '08',
      parentId: '2813'
    },
    {
      id: '2822',
      name: 'TAMBURCO',
      code: '09',
      parentId: '2813'
    }
  ],
  2823: [
    {
      id: '2824',
      name: 'ANDAHUAYLAS',
      code: '01',
      parentId: '2823'
    },
    {
      id: '2825',
      name: 'ANDARAPA',
      code: '02',
      parentId: '2823'
    },
    {
      id: '2826',
      name: 'CHIARA',
      code: '03',
      parentId: '2823'
    },
    {
      id: '2827',
      name: 'HUANCARAMA',
      code: '04',
      parentId: '2823'
    },
    {
      id: '2828',
      name: 'HUANCARAY',
      code: '05',
      parentId: '2823'
    },
    {
      id: '2829',
      name: 'HUAYANA',
      code: '06',
      parentId: '2823'
    },
    {
      id: '2842',
      name: 'KAQUIABAMBA',
      code: '19',
      parentId: '2823'
    },
    {
      id: '2830',
      name: 'KISHUARA',
      code: '07',
      parentId: '2823'
    },
    {
      id: '2831',
      name: 'PACOBAMBA',
      code: '08',
      parentId: '2823'
    },
    {
      id: '2832',
      name: 'PACUCHA',
      code: '09',
      parentId: '2823'
    },
    {
      id: '2833',
      name: 'PAMPACHIRI',
      code: '10',
      parentId: '2823'
    },
    {
      id: '2834',
      name: 'POMACOCHA',
      code: '11',
      parentId: '2823'
    },
    {
      id: '2835',
      name: 'SAN ANTONIO DE CACHI',
      code: '12',
      parentId: '2823'
    },
    {
      id: '2836',
      name: 'SAN JERONIMO',
      code: '13',
      parentId: '2823'
    },
    {
      id: '2837',
      name: 'SAN MIGUEL DE CHACCRAMPA',
      code: '14',
      parentId: '2823'
    },
    {
      id: '2838',
      name: 'SANTA MARIA DE CHICMO',
      code: '15',
      parentId: '2823'
    },
    {
      id: '2839',
      name: 'TALAVERA',
      code: '16',
      parentId: '2823'
    },
    {
      id: '2840',
      name: 'TUMAY HUARACA',
      code: '17',
      parentId: '2823'
    },
    {
      id: '2841',
      name: 'TURPO',
      code: '18',
      parentId: '2823'
    }
  ],
  2843: [
    {
      id: '2844',
      name: 'ANTABAMBA',
      code: '01',
      parentId: '2843'
    },
    {
      id: '2845',
      name: 'EL ORO',
      code: '02',
      parentId: '2843'
    },
    {
      id: '2846',
      name: 'HUAQUIRCA',
      code: '03',
      parentId: '2843'
    },
    {
      id: '2847',
      name: 'JUAN ESPINOZA MEDRANO',
      code: '04',
      parentId: '2843'
    },
    {
      id: '2848',
      name: 'OROPESA',
      code: '05',
      parentId: '2843'
    },
    {
      id: '2849',
      name: 'PACHACONAS',
      code: '06',
      parentId: '2843'
    },
    {
      id: '2850',
      name: 'SABAINO',
      code: '07',
      parentId: '2843'
    }
  ],
  2851: [
    {
      id: '2853',
      name: 'CAPAYA',
      code: '02',
      parentId: '2851'
    },
    {
      id: '2854',
      name: 'CARAYBAMBA',
      code: '03',
      parentId: '2851'
    },
    {
      id: '2852',
      name: 'CHALHUANCA',
      code: '01',
      parentId: '2851'
    },
    {
      id: '2855',
      name: 'CHAPIMARCA',
      code: '04',
      parentId: '2851'
    },
    {
      id: '2856',
      name: 'COLCABAMBA',
      code: '05',
      parentId: '2851'
    },
    {
      id: '2857',
      name: 'COTARUSE',
      code: '06',
      parentId: '2851'
    },
    {
      id: '2858',
      name: 'HUAYLLO',
      code: '07',
      parentId: '2851'
    },
    {
      id: '2859',
      name: 'JUSTO APU SAHUARAURA',
      code: '08',
      parentId: '2851'
    },
    {
      id: '2860',
      name: 'LUCRE',
      code: '09',
      parentId: '2851'
    },
    {
      id: '2861',
      name: 'POCOHUANCA',
      code: '10',
      parentId: '2851'
    },
    {
      id: '2862',
      name: 'SAN JUAN DE CHACQA',
      code: '11',
      parentId: '2851'
    },
    {
      id: '2863',
      name: 'SAQAYCA',
      code: '12',
      parentId: '2851'
    },
    {
      id: '2864',
      name: 'SORAYA',
      code: '13',
      parentId: '2851'
    },
    {
      id: '2865',
      name: 'TAPAIRIHUA',
      code: '14',
      parentId: '2851'
    },
    {
      id: '2866',
      name: 'TINTAY',
      code: '15',
      parentId: '2851'
    },
    {
      id: '2867',
      name: 'TORAYA',
      code: '16',
      parentId: '2851'
    },
    {
      id: '2868',
      name: 'YANACA',
      code: '17',
      parentId: '2851'
    }
  ],
  2869: [
    {
      id: '2875',
      name: 'CHALLHUAHUACHO',
      code: '06',
      parentId: '2869'
    },
    {
      id: '2871',
      name: 'COTABAMBAS',
      code: '02',
      parentId: '2869'
    },
    {
      id: '2872',
      name: 'COYLLURQUI',
      code: '03',
      parentId: '2869'
    },
    {
      id: '2873',
      name: 'HAQUIRA',
      code: '04',
      parentId: '2869'
    },
    {
      id: '2874',
      name: 'MARA',
      code: '05',
      parentId: '2869'
    },
    {
      id: '2870',
      name: 'TAMBOBAMBA',
      code: '01',
      parentId: '2869'
    }
  ],
  2876: [
    {
      id: '2878',
      name: 'ANCO-HUALLO',
      code: '02',
      parentId: '2876'
    },
    {
      id: '2877',
      name: 'CHINCHEROS',
      code: '01',
      parentId: '2876'
    },
    {
      id: '2879',
      name: 'COCHARCAS',
      code: '03',
      parentId: '2876'
    },
    {
      id: '2880',
      name: 'HUACCANA',
      code: '04',
      parentId: '2876'
    },
    {
      id: '2881',
      name: 'OCOBAMBA',
      code: '05',
      parentId: '2876'
    },
    {
      id: '2882',
      name: 'ONGOY',
      code: '06',
      parentId: '2876'
    },
    {
      id: '2884',
      name: 'RANRACANCHA',
      code: '08',
      parentId: '2876'
    },
    {
      id: '2883',
      name: 'URANMARCA',
      code: '07',
      parentId: '2876'
    }
  ],
  2885: [
    {
      id: '2886',
      name: 'CHUQUIBAMBILLA',
      code: '01',
      parentId: '2885'
    },
    {
      id: '2899',
      name: 'CURASCO',
      code: '14',
      parentId: '2885'
    },
    {
      id: '2887',
      name: 'CURPAHUASI',
      code: '02',
      parentId: '2885'
    },
    {
      id: '2888',
      name: 'GAMARRA',
      code: '03',
      parentId: '2885'
    },
    {
      id: '2889',
      name: 'HUAYLLATI',
      code: '04',
      parentId: '2885'
    },
    {
      id: '2890',
      name: 'MAMARA',
      code: '05',
      parentId: '2885'
    },
    {
      id: '2891',
      name: 'MICAELA BASTIDAS',
      code: '06',
      parentId: '2885'
    },
    {
      id: '2892',
      name: 'PATAYPAMPA',
      code: '07',
      parentId: '2885'
    },
    {
      id: '2893',
      name: 'PROGRESO',
      code: '08',
      parentId: '2885'
    },
    {
      id: '2894',
      name: 'SAN ANTONIO',
      code: '09',
      parentId: '2885'
    },
    {
      id: '2895',
      name: 'SANTA ROSA',
      code: '10',
      parentId: '2885'
    },
    {
      id: '2896',
      name: 'TURPAY',
      code: '11',
      parentId: '2885'
    },
    {
      id: '2897',
      name: 'VILCABAMBA',
      code: '12',
      parentId: '2885'
    },
    {
      id: '2898',
      name: 'VIRUNDO',
      code: '13',
      parentId: '2885'
    }
  ],
  2901: [
    {
      id: '2903',
      name: 'ALTO SELVA ALEGRE',
      code: '02',
      parentId: '2901'
    },
    {
      id: '2902',
      name: 'AREQUIPA',
      code: '01',
      parentId: '2901'
    },
    {
      id: '2904',
      name: 'CAYMA',
      code: '03',
      parentId: '2901'
    },
    {
      id: '2905',
      name: 'CERRO COLORADO',
      code: '04',
      parentId: '2901'
    },
    {
      id: '2906',
      name: 'CHARACATO',
      code: '05',
      parentId: '2901'
    },
    {
      id: '2907',
      name: 'CHIGUATA',
      code: '06',
      parentId: '2901'
    },
    {
      id: '2908',
      name: 'JACOBO HUNTER',
      code: '07',
      parentId: '2901'
    },
    {
      id: '2930',
      name: 'JOSE LUIS BUSTAMANTE Y RIVERO',
      code: '29',
      parentId: '2901'
    },
    {
      id: '2909',
      name: 'LA JOYA',
      code: '08',
      parentId: '2901'
    },
    {
      id: '2910',
      name: 'MARIANO MELGAR',
      code: '09',
      parentId: '2901'
    },
    {
      id: '2911',
      name: 'MIRAFLORES',
      code: '10',
      parentId: '2901'
    },
    {
      id: '2912',
      name: 'MOLLEBAYA',
      code: '11',
      parentId: '2901'
    },
    {
      id: '2913',
      name: 'PAUCARPATA',
      code: '12',
      parentId: '2901'
    },
    {
      id: '2914',
      name: 'POCSI',
      code: '13',
      parentId: '2901'
    },
    {
      id: '2915',
      name: 'POLOBAYA',
      code: '14',
      parentId: '2901'
    },
    {
      id: '2916',
      name: 'QUEQUEQA',
      code: '15',
      parentId: '2901'
    },
    {
      id: '2917',
      name: 'SABANDIA',
      code: '16',
      parentId: '2901'
    },
    {
      id: '2918',
      name: 'SACHACA',
      code: '17',
      parentId: '2901'
    },
    {
      id: '2919',
      name: 'SAN JUAN DE SIGUAS',
      code: '18',
      parentId: '2901'
    },
    {
      id: '2920',
      name: 'SAN JUAN DE TARUCANI',
      code: '19',
      parentId: '2901'
    },
    {
      id: '2921',
      name: 'SANTA ISABEL DE SIGUAS',
      code: '20',
      parentId: '2901'
    },
    {
      id: '2922',
      name: 'SANTA RITA DE SIGUAS',
      code: '21',
      parentId: '2901'
    },
    {
      id: '2923',
      name: 'SOCABAYA',
      code: '22',
      parentId: '2901'
    },
    {
      id: '2924',
      name: 'TIABAYA',
      code: '23',
      parentId: '2901'
    },
    {
      id: '2925',
      name: 'UCHUMAYO',
      code: '24',
      parentId: '2901'
    },
    {
      id: '2926',
      name: 'VITOR',
      code: '25',
      parentId: '2901'
    },
    {
      id: '2927',
      name: 'YANAHUARA',
      code: '26',
      parentId: '2901'
    },
    {
      id: '2928',
      name: 'YARABAMBA',
      code: '27',
      parentId: '2901'
    },
    {
      id: '2929',
      name: 'YURA',
      code: '28',
      parentId: '2901'
    }
  ],
  2931: [
    {
      id: '2932',
      name: 'CAMANA',
      code: '01',
      parentId: '2931'
    },
    {
      id: '2933',
      name: 'JOSE MARIA QUIMPER',
      code: '02',
      parentId: '2931'
    },
    {
      id: '2934',
      name: 'MARIANO NICOLAS VALCARCEL',
      code: '03',
      parentId: '2931'
    },
    {
      id: '2935',
      name: 'MARISCAL CACERES',
      code: '04',
      parentId: '2931'
    },
    {
      id: '2936',
      name: 'NICOLAS DE PIEROLA',
      code: '05',
      parentId: '2931'
    },
    {
      id: '2937',
      name: 'OCOQA',
      code: '06',
      parentId: '2931'
    },
    {
      id: '2938',
      name: 'QUILCA',
      code: '07',
      parentId: '2931'
    },
    {
      id: '2939',
      name: 'SAMUEL PASTOR',
      code: '08',
      parentId: '2931'
    }
  ],
  2940: [
    {
      id: '2942',
      name: 'ACARI',
      code: '02',
      parentId: '2940'
    },
    {
      id: '2943',
      name: 'ATICO',
      code: '03',
      parentId: '2940'
    },
    {
      id: '2944',
      name: 'ATIQUIPA',
      code: '04',
      parentId: '2940'
    },
    {
      id: '2945',
      name: 'BELLA UNION',
      code: '05',
      parentId: '2940'
    },
    {
      id: '2946',
      name: 'CAHUACHO',
      code: '06',
      parentId: '2940'
    },
    {
      id: '2941',
      name: 'CARAVELI',
      code: '01',
      parentId: '2940'
    },
    {
      id: '2947',
      name: 'CHALA',
      code: '07',
      parentId: '2940'
    },
    {
      id: '2948',
      name: 'CHAPARRA',
      code: '08',
      parentId: '2940'
    },
    {
      id: '2949',
      name: 'HUANUHUANU',
      code: '09',
      parentId: '2940'
    },
    {
      id: '2950',
      name: 'JAQUI',
      code: '10',
      parentId: '2940'
    },
    {
      id: '2951',
      name: 'LOMAS',
      code: '11',
      parentId: '2940'
    },
    {
      id: '2952',
      name: 'QUICACHA',
      code: '12',
      parentId: '2940'
    },
    {
      id: '2953',
      name: 'YAUCA',
      code: '13',
      parentId: '2940'
    }
  ],
  2954: [
    {
      id: '2956',
      name: 'ANDAGUA',
      code: '02',
      parentId: '2954'
    },
    {
      id: '2955',
      name: 'APLAO',
      code: '01',
      parentId: '2954'
    },
    {
      id: '2957',
      name: 'AYO',
      code: '03',
      parentId: '2954'
    },
    {
      id: '2958',
      name: 'CHACHAS',
      code: '04',
      parentId: '2954'
    },
    {
      id: '2959',
      name: 'CHILCAYMARCA',
      code: '05',
      parentId: '2954'
    },
    {
      id: '2960',
      name: 'CHOCO',
      code: '06',
      parentId: '2954'
    },
    {
      id: '2961',
      name: 'HUANCARQUI',
      code: '07',
      parentId: '2954'
    },
    {
      id: '2962',
      name: 'MACHAGUAY',
      code: '08',
      parentId: '2954'
    },
    {
      id: '2970',
      name: 'MAJES',
      code: '20',
      parentId: '2954'
    },
    {
      id: '2963',
      name: 'ORCOPAMPA',
      code: '09',
      parentId: '2954'
    },
    {
      id: '2964',
      name: 'PAMPACOLCA',
      code: '10',
      parentId: '2954'
    },
    {
      id: '2965',
      name: 'TIPAN',
      code: '11',
      parentId: '2954'
    },
    {
      id: '2966',
      name: 'UQON',
      code: '12',
      parentId: '2954'
    },
    {
      id: '2967',
      name: 'URACA',
      code: '13',
      parentId: '2954'
    },
    {
      id: '2968',
      name: 'VIRACO',
      code: '14',
      parentId: '2954'
    },
    {
      id: '2969',
      name: 'YANQUE',
      code: '19',
      parentId: '2954'
    }
  ],
  2971: [
    {
      id: '2973',
      name: 'ACHOMA',
      code: '02',
      parentId: '2971'
    },
    {
      id: '2974',
      name: 'CABANACONDE',
      code: '03',
      parentId: '2971'
    },
    {
      id: '2975',
      name: 'CALLALLI',
      code: '04',
      parentId: '2971'
    },
    {
      id: '2976',
      name: 'CAYLLOMA',
      code: '05',
      parentId: '2971'
    },
    {
      id: '2972',
      name: 'CHIVAY',
      code: '01',
      parentId: '2971'
    },
    {
      id: '2977',
      name: 'COPORAQUE',
      code: '06',
      parentId: '2971'
    },
    {
      id: '2978',
      name: 'HUAMBO',
      code: '07',
      parentId: '2971'
    },
    {
      id: '2979',
      name: 'HUANCA',
      code: '08',
      parentId: '2971'
    },
    {
      id: '2980',
      name: 'ICHUPAMPA',
      code: '09',
      parentId: '2971'
    },
    {
      id: '2981',
      name: 'LARI',
      code: '10',
      parentId: '2971'
    },
    {
      id: '2982',
      name: 'LLUTA',
      code: '11',
      parentId: '2971'
    },
    {
      id: '2983',
      name: 'MACA',
      code: '12',
      parentId: '2971'
    },
    {
      id: '2984',
      name: 'MADRIGAL',
      code: '13',
      parentId: '2971'
    },
    {
      id: '2991',
      name: 'MAJES',
      code: '20',
      parentId: '2971'
    },
    {
      id: '2985',
      name: 'SAN ANTONIO DE CHUCA',
      code: '14',
      parentId: '2971'
    },
    {
      id: '2986',
      name: 'SIBAYO',
      code: '15',
      parentId: '2971'
    },
    {
      id: '2987',
      name: 'TAPAY',
      code: '16',
      parentId: '2971'
    },
    {
      id: '2988',
      name: 'TISCO',
      code: '17',
      parentId: '2971'
    },
    {
      id: '2989',
      name: 'TUTI',
      code: '18',
      parentId: '2971'
    },
    {
      id: '2990',
      name: 'YANQUE',
      code: '19',
      parentId: '2971'
    }
  ],
  2992: [
    {
      id: '2994',
      name: 'ANDARAY',
      code: '02',
      parentId: '2992'
    },
    {
      id: '2995',
      name: 'CAYARANI',
      code: '03',
      parentId: '2992'
    },
    {
      id: '2996',
      name: 'CHICHAS',
      code: '04',
      parentId: '2992'
    },
    {
      id: '2993',
      name: 'CHUQUIBAMBA',
      code: '01',
      parentId: '2992'
    },
    {
      id: '2997',
      name: 'IRAY',
      code: '05',
      parentId: '2992'
    },
    {
      id: '2998',
      name: 'RIO GRANDE',
      code: '06',
      parentId: '2992'
    },
    {
      id: '2999',
      name: 'SALAMANCA',
      code: '07',
      parentId: '2992'
    },
    {
      id: '3000',
      name: 'YANAQUIHUA',
      code: '08',
      parentId: '2992'
    }
  ],
  3001: [
    {
      id: '3003',
      name: 'COCACHACRA',
      code: '02',
      parentId: '3001'
    },
    {
      id: '3004',
      name: 'DEAN VALDIVIA',
      code: '03',
      parentId: '3001'
    },
    {
      id: '3005',
      name: 'ISLAY',
      code: '04',
      parentId: '3001'
    },
    {
      id: '3006',
      name: 'MEJIA',
      code: '05',
      parentId: '3001'
    },
    {
      id: '3002',
      name: 'MOLLENDO',
      code: '01',
      parentId: '3001'
    },
    {
      id: '3007',
      name: 'PUNTA DE BOMBON',
      code: '06',
      parentId: '3001'
    }
  ],
  3008: [
    {
      id: '3010',
      name: 'ALCA',
      code: '02',
      parentId: '3008'
    },
    {
      id: '3011',
      name: 'CHARCANA',
      code: '03',
      parentId: '3008'
    },
    {
      id: '3009',
      name: 'COTAHUASI',
      code: '01',
      parentId: '3008'
    },
    {
      id: '3012',
      name: 'HUAYNACOTAS',
      code: '04',
      parentId: '3008'
    },
    {
      id: '3013',
      name: 'PAMPAMARCA',
      code: '05',
      parentId: '3008'
    },
    {
      id: '3014',
      name: 'PUYCA',
      code: '06',
      parentId: '3008'
    },
    {
      id: '3015',
      name: 'QUECHUALLA',
      code: '07',
      parentId: '3008'
    },
    {
      id: '3016',
      name: 'SAYLA',
      code: '08',
      parentId: '3008'
    },
    {
      id: '3017',
      name: 'TAURIA',
      code: '09',
      parentId: '3008'
    },
    {
      id: '3018',
      name: 'TOMEPAMPA',
      code: '10',
      parentId: '3008'
    },
    {
      id: '3019',
      name: 'TORO',
      code: '11',
      parentId: '3008'
    }
  ],
  3021: [
    {
      id: '3023',
      name: 'ACOCRO',
      code: '02',
      parentId: '3021'
    },
    {
      id: '3024',
      name: 'ACOS VINCHOS',
      code: '03',
      parentId: '3021'
    },
    {
      id: '3022',
      name: 'AYACUCHO',
      code: '01',
      parentId: '3021'
    },
    {
      id: '3025',
      name: 'CARMEN ALTO',
      code: '04',
      parentId: '3021'
    },
    {
      id: '3026',
      name: 'CHIARA',
      code: '05',
      parentId: '3021'
    },
    {
      id: '3036',
      name: 'JESÚS NAZARENO',
      code: '15',
      parentId: '3021'
    },
    {
      id: '3027',
      name: 'OCROS',
      code: '06',
      parentId: '3021'
    },
    {
      id: '3028',
      name: 'PACAYCASA',
      code: '07',
      parentId: '3021'
    },
    {
      id: '3029',
      name: 'QUINUA',
      code: '08',
      parentId: '3021'
    },
    {
      id: '3030',
      name: 'SAN JOSE DE TICLLAS',
      code: '09',
      parentId: '3021'
    },
    {
      id: '3031',
      name: 'SAN JUAN BAUTISTA',
      code: '10',
      parentId: '3021'
    },
    {
      id: '3032',
      name: 'SANTIAGO DE PISCHA',
      code: '11',
      parentId: '3021'
    },
    {
      id: '3033',
      name: 'SOCOS',
      code: '12',
      parentId: '3021'
    },
    {
      id: '3034',
      name: 'TAMBILLO',
      code: '13',
      parentId: '3021'
    },
    {
      id: '3035',
      name: 'VINCHOS',
      code: '14',
      parentId: '3021'
    }
  ],
  3037: [
    {
      id: '3038',
      name: 'CANGALLO',
      code: '01',
      parentId: '3037'
    },
    {
      id: '3039',
      name: 'CHUSCHI',
      code: '02',
      parentId: '3037'
    },
    {
      id: '3040',
      name: 'LOS MOROCHUCOS',
      code: '03',
      parentId: '3037'
    },
    {
      id: '3041',
      name: 'MARIA PARADO DE BELLIDO',
      code: '04',
      parentId: '3037'
    },
    {
      id: '3042',
      name: 'PARAS',
      code: '05',
      parentId: '3037'
    },
    {
      id: '3043',
      name: 'TOTOS',
      code: '06',
      parentId: '3037'
    }
  ],
  3044: [
    {
      id: '3046',
      name: 'CARAPO',
      code: '02',
      parentId: '3044'
    },
    {
      id: '3047',
      name: 'SACSAMARCA',
      code: '03',
      parentId: '3044'
    },
    {
      id: '3045',
      name: 'SANCOS',
      code: '01',
      parentId: '3044'
    },
    {
      id: '3048',
      name: 'SANTIAGO DE LUCANAMARCA',
      code: '04',
      parentId: '3044'
    }
  ],
  3049: [
    {
      id: '3051',
      name: 'AYAHUANCO',
      code: '02',
      parentId: '3049'
    },
    {
      id: '3052',
      name: 'HUAMANGUILLA',
      code: '03',
      parentId: '3049'
    },
    {
      id: '3050',
      name: 'HUANTA',
      code: '01',
      parentId: '3049'
    },
    {
      id: '3053',
      name: 'IGUAIN',
      code: '04',
      parentId: '3049'
    },
    {
      id: '3057',
      name: 'LLOCHEGUA',
      code: '08',
      parentId: '3049'
    },
    {
      id: '3054',
      name: 'LURICOCHA',
      code: '05',
      parentId: '3049'
    },
    {
      id: '3055',
      name: 'SANTILLANA',
      code: '06',
      parentId: '3049'
    },
    {
      id: '3056',
      name: 'SIVIA',
      code: '07',
      parentId: '3049'
    }
  ],
  3058: [
    {
      id: '3060',
      name: 'ANCO',
      code: '02',
      parentId: '3058'
    },
    {
      id: '3061',
      name: 'AYNA',
      code: '03',
      parentId: '3058'
    },
    {
      id: '3062',
      name: 'CHILCAS',
      code: '04',
      parentId: '3058'
    },
    {
      id: '3063',
      name: 'CHUNGUI',
      code: '05',
      parentId: '3058'
    },
    {
      id: '3064',
      name: 'LUIS CARRANZA',
      code: '06',
      parentId: '3058'
    },
    {
      id: '3059',
      name: 'SAN MIGUEL',
      code: '01',
      parentId: '3058'
    },
    {
      id: '3065',
      name: 'SANTA ROSA',
      code: '07',
      parentId: '3058'
    },
    {
      id: '3066',
      name: 'TAMBO',
      code: '08',
      parentId: '3058'
    }
  ],
  3067: [
    {
      id: '3069',
      name: 'AUCARA',
      code: '02',
      parentId: '3067'
    },
    {
      id: '3070',
      name: 'CABANA',
      code: '03',
      parentId: '3067'
    },
    {
      id: '3071',
      name: 'CARMEN SALCEDO',
      code: '04',
      parentId: '3067'
    },
    {
      id: '3072',
      name: 'CHAVIQA',
      code: '05',
      parentId: '3067'
    },
    {
      id: '3073',
      name: 'CHIPAO',
      code: '06',
      parentId: '3067'
    },
    {
      id: '3074',
      name: 'HUAC-HUAS',
      code: '07',
      parentId: '3067'
    },
    {
      id: '3075',
      name: 'LARAMATE',
      code: '08',
      parentId: '3067'
    },
    {
      id: '3076',
      name: 'LEONCIO PRADO',
      code: '09',
      parentId: '3067'
    },
    {
      id: '3077',
      name: 'LLAUTA',
      code: '10',
      parentId: '3067'
    },
    {
      id: '3078',
      name: 'LUCANAS',
      code: '11',
      parentId: '3067'
    },
    {
      id: '3079',
      name: 'OCAQA',
      code: '12',
      parentId: '3067'
    },
    {
      id: '3080',
      name: 'OTOCA',
      code: '13',
      parentId: '3067'
    },
    {
      id: '3068',
      name: 'PUQUIO',
      code: '01',
      parentId: '3067'
    },
    {
      id: '3081',
      name: 'SAISA',
      code: '14',
      parentId: '3067'
    },
    {
      id: '3082',
      name: 'SAN CRISTOBAL',
      code: '15',
      parentId: '3067'
    },
    {
      id: '3083',
      name: 'SAN JUAN',
      code: '16',
      parentId: '3067'
    },
    {
      id: '3084',
      name: 'SAN PEDRO',
      code: '17',
      parentId: '3067'
    },
    {
      id: '3085',
      name: 'SAN PEDRO DE PALCO',
      code: '18',
      parentId: '3067'
    },
    {
      id: '3086',
      name: 'SANCOS',
      code: '19',
      parentId: '3067'
    },
    {
      id: '3087',
      name: 'SANTA ANA DE HUAYCAHUACHO',
      code: '20',
      parentId: '3067'
    },
    {
      id: '3088',
      name: 'SANTA LUCIA',
      code: '21',
      parentId: '3067'
    }
  ],
  3089: [
    {
      id: '3091',
      name: 'CHUMPI',
      code: '02',
      parentId: '3089'
    },
    {
      id: '3090',
      name: 'CORACORA',
      code: '01',
      parentId: '3089'
    },
    {
      id: '3092',
      name: 'CORONEL CASTAQEDA',
      code: '03',
      parentId: '3089'
    },
    {
      id: '3093',
      name: 'PACAPAUSA',
      code: '04',
      parentId: '3089'
    },
    {
      id: '3094',
      name: 'PULLO',
      code: '05',
      parentId: '3089'
    },
    {
      id: '3095',
      name: 'PUYUSCA',
      code: '06',
      parentId: '3089'
    },
    {
      id: '3096',
      name: 'SAN FRANCISCO DE RAVACAYCO',
      code: '07',
      parentId: '3089'
    },
    {
      id: '3097',
      name: 'UPAHUACHO',
      code: '08',
      parentId: '3089'
    }
  ],
  3098: [
    {
      id: '3100',
      name: 'COLTA',
      code: '02',
      parentId: '3098'
    },
    {
      id: '3101',
      name: 'CORCULLA',
      code: '03',
      parentId: '3098'
    },
    {
      id: '3102',
      name: 'LAMPA',
      code: '04',
      parentId: '3098'
    },
    {
      id: '3103',
      name: 'MARCABAMBA',
      code: '05',
      parentId: '3098'
    },
    {
      id: '3104',
      name: 'OYOLO',
      code: '06',
      parentId: '3098'
    },
    {
      id: '3105',
      name: 'PARARCA',
      code: '07',
      parentId: '3098'
    },
    {
      id: '3099',
      name: 'PAUSA',
      code: '01',
      parentId: '3098'
    },
    {
      id: '3106',
      name: 'SAN JAVIER DE ALPABAMBA',
      code: '08',
      parentId: '3098'
    },
    {
      id: '3107',
      name: 'SAN JOSE DE USHUA',
      code: '09',
      parentId: '3098'
    },
    {
      id: '3108',
      name: 'SARA SARA',
      code: '10',
      parentId: '3098'
    }
  ],
  3109: [
    {
      id: '3111',
      name: 'BELEN',
      code: '02',
      parentId: '3109'
    },
    {
      id: '3112',
      name: 'CHALCOS',
      code: '03',
      parentId: '3109'
    },
    {
      id: '3113',
      name: 'CHILCAYOC',
      code: '04',
      parentId: '3109'
    },
    {
      id: '3114',
      name: 'HUACAQA',
      code: '05',
      parentId: '3109'
    },
    {
      id: '3115',
      name: 'MORCOLLA',
      code: '06',
      parentId: '3109'
    },
    {
      id: '3116',
      name: 'PAICO',
      code: '07',
      parentId: '3109'
    },
    {
      id: '3110',
      name: 'QUEROBAMBA',
      code: '01',
      parentId: '3109'
    },
    {
      id: '3117',
      name: 'SAN PEDRO DE LARCAY',
      code: '08',
      parentId: '3109'
    },
    {
      id: '3118',
      name: 'SAN SALVADOR DE QUIJE',
      code: '09',
      parentId: '3109'
    },
    {
      id: '3119',
      name: 'SANTIAGO DE PAUCARAY',
      code: '10',
      parentId: '3109'
    },
    {
      id: '3120',
      name: 'SORAS',
      code: '11',
      parentId: '3109'
    }
  ],
  3121: [
    {
      id: '3123',
      name: 'ALCAMENCA',
      code: '02',
      parentId: '3121'
    },
    {
      id: '3124',
      name: 'APONGO',
      code: '03',
      parentId: '3121'
    },
    {
      id: '3125',
      name: 'ASQUIPATA',
      code: '04',
      parentId: '3121'
    },
    {
      id: '3126',
      name: 'CANARIA',
      code: '05',
      parentId: '3121'
    },
    {
      id: '3127',
      name: 'CAYARA',
      code: '06',
      parentId: '3121'
    },
    {
      id: '3128',
      name: 'COLCA',
      code: '07',
      parentId: '3121'
    },
    {
      id: '3129',
      name: 'HUAMANQUIQUIA',
      code: '08',
      parentId: '3121'
    },
    {
      id: '3122',
      name: 'HUANCAPI',
      code: '01',
      parentId: '3121'
    },
    {
      id: '3130',
      name: 'HUANCARAYLLA',
      code: '09',
      parentId: '3121'
    },
    {
      id: '3131',
      name: 'HUAYA',
      code: '10',
      parentId: '3121'
    },
    {
      id: '3132',
      name: 'SARHUA',
      code: '11',
      parentId: '3121'
    },
    {
      id: '3133',
      name: 'VILCANCHOS',
      code: '12',
      parentId: '3121'
    }
  ],
  3134: [
    {
      id: '3136',
      name: 'ACCOMARCA',
      code: '02',
      parentId: '3134'
    },
    {
      id: '3137',
      name: 'CARHUANCA',
      code: '03',
      parentId: '3134'
    },
    {
      id: '3138',
      name: 'CONCEPCION',
      code: '04',
      parentId: '3134'
    },
    {
      id: '3139',
      name: 'HUAMBALPA',
      code: '05',
      parentId: '3134'
    },
    {
      id: '3140',
      name: 'INDEPENDENCIA',
      code: '06',
      parentId: '3134'
    },
    {
      id: '3141',
      name: 'SAURAMA',
      code: '07',
      parentId: '3134'
    },
    {
      id: '3135',
      name: 'VILCAS HUAMAN',
      code: '01',
      parentId: '3134'
    },
    {
      id: '3142',
      name: 'VISCHONGO',
      code: '08',
      parentId: '3134'
    }
  ],
  3144: [
    {
      id: '3146',
      name: 'ASUNCION',
      code: '02',
      parentId: '3144'
    },
    {
      id: '3145',
      name: 'CAJAMARCA',
      code: '01',
      parentId: '3144'
    },
    {
      id: '3147',
      name: 'CHETILLA',
      code: '03',
      parentId: '3144'
    },
    {
      id: '3148',
      name: 'COSPAN',
      code: '04',
      parentId: '3144'
    },
    {
      id: '3149',
      name: 'ENCAQADA',
      code: '05',
      parentId: '3144'
    },
    {
      id: '3150',
      name: 'JESUS',
      code: '06',
      parentId: '3144'
    },
    {
      id: '3151',
      name: 'LLACANORA',
      code: '07',
      parentId: '3144'
    },
    {
      id: '3152',
      name: 'LOS BAQOS DEL INCA',
      code: '08',
      parentId: '3144'
    },
    {
      id: '3153',
      name: 'MAGDALENA',
      code: '09',
      parentId: '3144'
    },
    {
      id: '3154',
      name: 'MATARA',
      code: '10',
      parentId: '3144'
    },
    {
      id: '3155',
      name: 'NAMORA',
      code: '11',
      parentId: '3144'
    },
    {
      id: '3156',
      name: 'SAN JUAN',
      code: '12',
      parentId: '3144'
    }
  ],
  3157: [
    {
      id: '3159',
      name: 'CACHACHI',
      code: '02',
      parentId: '3157'
    },
    {
      id: '3158',
      name: 'CAJABAMBA',
      code: '01',
      parentId: '3157'
    },
    {
      id: '3160',
      name: 'CONDEBAMBA',
      code: '03',
      parentId: '3157'
    },
    {
      id: '3161',
      name: 'SITACOCHA',
      code: '04',
      parentId: '3157'
    }
  ],
  3162: [
    {
      id: '3163',
      name: 'CELENDIN',
      code: '01',
      parentId: '3162'
    },
    {
      id: '3164',
      name: 'CHUMUCH',
      code: '02',
      parentId: '3162'
    },
    {
      id: '3165',
      name: 'CORTEGANA',
      code: '03',
      parentId: '3162'
    },
    {
      id: '3166',
      name: 'HUASMIN',
      code: '04',
      parentId: '3162'
    },
    {
      id: '3167',
      name: 'JORGE CHAVEZ',
      code: '05',
      parentId: '3162'
    },
    {
      id: '3168',
      name: 'JOSE GALVEZ',
      code: '06',
      parentId: '3162'
    },
    {
      id: '3174',
      name: 'LA LIBERTAD DE PALLAN',
      code: '12',
      parentId: '3162'
    },
    {
      id: '3169',
      name: 'MIGUEL IGLESIAS',
      code: '07',
      parentId: '3162'
    },
    {
      id: '3170',
      name: 'OXAMARCA',
      code: '08',
      parentId: '3162'
    },
    {
      id: '3171',
      name: 'SOROCHUCO',
      code: '09',
      parentId: '3162'
    },
    {
      id: '3172',
      name: 'SUCRE',
      code: '10',
      parentId: '3162'
    },
    {
      id: '3173',
      name: 'UTCO',
      code: '11',
      parentId: '3162'
    }
  ],
  3175: [
    {
      id: '3177',
      name: 'ANGUIA',
      code: '02',
      parentId: '3175'
    },
    {
      id: '3178',
      name: 'CHADIN',
      code: '03',
      parentId: '3175'
    },
    {
      id: '3194',
      name: 'CHALAMARCA',
      code: '19',
      parentId: '3175'
    },
    {
      id: '3179',
      name: 'CHIGUIRIP',
      code: '04',
      parentId: '3175'
    },
    {
      id: '3180',
      name: 'CHIMBAN',
      code: '05',
      parentId: '3175'
    },
    {
      id: '3181',
      name: 'CHOROPAMPA',
      code: '06',
      parentId: '3175'
    },
    {
      id: '3176',
      name: 'CHOTA',
      code: '01',
      parentId: '3175'
    },
    {
      id: '3182',
      name: 'COCHABAMBA',
      code: '07',
      parentId: '3175'
    },
    {
      id: '3183',
      name: 'CONCHAN',
      code: '08',
      parentId: '3175'
    },
    {
      id: '3184',
      name: 'HUAMBOS',
      code: '09',
      parentId: '3175'
    },
    {
      id: '3185',
      name: 'LAJAS',
      code: '10',
      parentId: '3175'
    },
    {
      id: '3186',
      name: 'LLAMA',
      code: '11',
      parentId: '3175'
    },
    {
      id: '3187',
      name: 'MIRACOSTA',
      code: '12',
      parentId: '3175'
    },
    {
      id: '3188',
      name: 'PACCHA',
      code: '13',
      parentId: '3175'
    },
    {
      id: '3189',
      name: 'PION',
      code: '14',
      parentId: '3175'
    },
    {
      id: '3190',
      name: 'QUEROCOTO',
      code: '15',
      parentId: '3175'
    },
    {
      id: '3191',
      name: 'SAN JUAN DE LICUPIS',
      code: '16',
      parentId: '3175'
    },
    {
      id: '3192',
      name: 'TACABAMBA',
      code: '17',
      parentId: '3175'
    },
    {
      id: '3193',
      name: 'TOCMOCHE',
      code: '18',
      parentId: '3175'
    }
  ],
  3195: [
    {
      id: '3197',
      name: 'CHILETE',
      code: '02',
      parentId: '3195'
    },
    {
      id: '3196',
      name: 'CONTUMAZA',
      code: '01',
      parentId: '3195'
    },
    {
      id: '3198',
      name: 'CUPISNIQUE',
      code: '03',
      parentId: '3195'
    },
    {
      id: '3199',
      name: 'GUZMANGO',
      code: '04',
      parentId: '3195'
    },
    {
      id: '3200',
      name: 'SAN BENITO',
      code: '05',
      parentId: '3195'
    },
    {
      id: '3201',
      name: 'SANTA CRUZ DE TOLED',
      code: '06',
      parentId: '3195'
    },
    {
      id: '3202',
      name: 'TANTARICA',
      code: '07',
      parentId: '3195'
    },
    {
      id: '3203',
      name: 'YONAN',
      code: '08',
      parentId: '3195'
    }
  ],
  3204: [
    {
      id: '3206',
      name: 'CALLAYUC',
      code: '02',
      parentId: '3204'
    },
    {
      id: '3207',
      name: 'CHOROS',
      code: '03',
      parentId: '3204'
    },
    {
      id: '3208',
      name: 'CUJILLO',
      code: '04',
      parentId: '3204'
    },
    {
      id: '3205',
      name: 'CUTERVO',
      code: '01',
      parentId: '3204'
    },
    {
      id: '3209',
      name: 'LA RAMADA',
      code: '05',
      parentId: '3204'
    },
    {
      id: '3210',
      name: 'PIMPINGOS',
      code: '06',
      parentId: '3204'
    },
    {
      id: '3211',
      name: 'QUEROCOTILLO',
      code: '07',
      parentId: '3204'
    },
    {
      id: '3212',
      name: 'SAN ANDRES DE CUTERVO',
      code: '08',
      parentId: '3204'
    },
    {
      id: '3213',
      name: 'SAN JUAN DE CUTERVO',
      code: '09',
      parentId: '3204'
    },
    {
      id: '3214',
      name: 'SAN LUIS DE LUCMA',
      code: '10',
      parentId: '3204'
    },
    {
      id: '3215',
      name: 'SANTA CRUZ',
      code: '11',
      parentId: '3204'
    },
    {
      id: '3216',
      name: 'SANTO DOMINGO DE LA CAPILLA',
      code: '12',
      parentId: '3204'
    },
    {
      id: '3217',
      name: 'SANTO TOMAS',
      code: '13',
      parentId: '3204'
    },
    {
      id: '3218',
      name: 'SOCOTA',
      code: '14',
      parentId: '3204'
    },
    {
      id: '3219',
      name: 'TORIBIO CASANOVA',
      code: '15',
      parentId: '3204'
    }
  ],
  3220: [
    {
      id: '3221',
      name: 'BAMBAMARCA',
      code: '01',
      parentId: '3220'
    },
    {
      id: '3222',
      name: 'CHUGUR',
      code: '02',
      parentId: '3220'
    },
    {
      id: '3223',
      name: 'HUALGAYOC',
      code: '03',
      parentId: '3220'
    }
  ],
  3224: [
    {
      id: '3226',
      name: 'BELLAVISTA',
      code: '02',
      parentId: '3224'
    },
    {
      id: '3227',
      name: 'CHONTALI',
      code: '03',
      parentId: '3224'
    },
    {
      id: '3228',
      name: 'COLASAY',
      code: '04',
      parentId: '3224'
    },
    {
      id: '3229',
      name: 'HUABAL',
      code: '05',
      parentId: '3224'
    },
    {
      id: '3225',
      name: 'JAEN',
      code: '01',
      parentId: '3224'
    },
    {
      id: '3230',
      name: 'LAS PIRIAS',
      code: '06',
      parentId: '3224'
    },
    {
      id: '3231',
      name: 'POMAHUACA',
      code: '07',
      parentId: '3224'
    },
    {
      id: '3232',
      name: 'PUCARA',
      code: '08',
      parentId: '3224'
    },
    {
      id: '3233',
      name: 'SALLIQUE',
      code: '09',
      parentId: '3224'
    },
    {
      id: '3234',
      name: 'SAN FELIPE',
      code: '10',
      parentId: '3224'
    },
    {
      id: '3235',
      name: 'SAN JOSE DEL ALTO',
      code: '11',
      parentId: '3224'
    },
    {
      id: '3236',
      name: 'SANTA ROSA',
      code: '12',
      parentId: '3224'
    }
  ],
  3237: [
    {
      id: '3239',
      name: 'CHIRINOS',
      code: '02',
      parentId: '3237'
    },
    {
      id: '3240',
      name: 'HUARANGO',
      code: '03',
      parentId: '3237'
    },
    {
      id: '3241',
      name: 'LA COIPA',
      code: '04',
      parentId: '3237'
    },
    {
      id: '3242',
      name: 'NAMBALLE',
      code: '05',
      parentId: '3237'
    },
    {
      id: '3238',
      name: 'SAN IGNACIO',
      code: '01',
      parentId: '3237'
    },
    {
      id: '3243',
      name: 'SAN JOSE DE LOURDES',
      code: '06',
      parentId: '3237'
    },
    {
      id: '3244',
      name: 'TABACONAS',
      code: '07',
      parentId: '3237'
    }
  ],
  3245: [
    {
      id: '3247',
      name: 'CHANCAY',
      code: '02',
      parentId: '3245'
    },
    {
      id: '3248',
      name: 'EDUARDO VILLANUEVA',
      code: '03',
      parentId: '3245'
    },
    {
      id: '3249',
      name: 'GREGORIO PITA',
      code: '04',
      parentId: '3245'
    },
    {
      id: '3250',
      name: 'ICHOCAN',
      code: '05',
      parentId: '3245'
    },
    {
      id: '3251',
      name: 'JOSE MANUEL QUIROZ',
      code: '06',
      parentId: '3245'
    },
    {
      id: '3252',
      name: 'JOSE SABOGAL',
      code: '07',
      parentId: '3245'
    },
    {
      id: '3246',
      name: 'PEDRO GALVEZ',
      code: '01',
      parentId: '3245'
    }
  ],
  3253: [
    {
      id: '3255',
      name: 'BOLIVAR',
      code: '02',
      parentId: '3253'
    },
    {
      id: '3256',
      name: 'CALQUIS',
      code: '03',
      parentId: '3253'
    },
    {
      id: '3257',
      name: 'CATILLUC',
      code: '04',
      parentId: '3253'
    },
    {
      id: '3258',
      name: 'EL PRADO',
      code: '05',
      parentId: '3253'
    },
    {
      id: '3259',
      name: 'LA FLORIDA',
      code: '06',
      parentId: '3253'
    },
    {
      id: '3260',
      name: 'LLAPA',
      code: '07',
      parentId: '3253'
    },
    {
      id: '3261',
      name: 'NANCHOC',
      code: '08',
      parentId: '3253'
    },
    {
      id: '3262',
      name: 'NIEPOS',
      code: '09',
      parentId: '3253'
    },
    {
      id: '3263',
      name: 'SAN GREGORIO',
      code: '10',
      parentId: '3253'
    },
    {
      id: '3254',
      name: 'SAN MIGUEL',
      code: '01',
      parentId: '3253'
    },
    {
      id: '3264',
      name: 'SAN SILVESTRE DE COCHAN',
      code: '11',
      parentId: '3253'
    },
    {
      id: '3265',
      name: 'TONGOD',
      code: '12',
      parentId: '3253'
    },
    {
      id: '3266',
      name: 'UNION AGUA BLANCA',
      code: '13',
      parentId: '3253'
    }
  ],
  3267: [
    {
      id: '3269',
      name: 'SAN BERNARDINO',
      code: '02',
      parentId: '3267'
    },
    {
      id: '3270',
      name: 'SAN LUIS',
      code: '03',
      parentId: '3267'
    },
    {
      id: '3268',
      name: 'SAN PABLO',
      code: '01',
      parentId: '3267'
    },
    {
      id: '3271',
      name: 'TUMBADEN',
      code: '04',
      parentId: '3267'
    }
  ],
  3272: [
    {
      id: '3274',
      name: 'ANDABAMBA',
      code: '02',
      parentId: '3272'
    },
    {
      id: '3275',
      name: 'CATACHE',
      code: '03',
      parentId: '3272'
    },
    {
      id: '3276',
      name: 'CHANCAYBAQOS',
      code: '04',
      parentId: '3272'
    },
    {
      id: '3277',
      name: 'LA ESPERANZA',
      code: '05',
      parentId: '3272'
    },
    {
      id: '3278',
      name: 'NINABAMBA',
      code: '06',
      parentId: '3272'
    },
    {
      id: '3279',
      name: 'PULAN',
      code: '07',
      parentId: '3272'
    },
    {
      id: '3273',
      name: 'SANTA CRUZ',
      code: '01',
      parentId: '3272'
    },
    {
      id: '3280',
      name: 'SAUCEPAMPA',
      code: '08',
      parentId: '3272'
    },
    {
      id: '3281',
      name: 'SEXI',
      code: '09',
      parentId: '3272'
    },
    {
      id: '3282',
      name: 'UTICYACU',
      code: '10',
      parentId: '3272'
    },
    {
      id: '3283',
      name: 'YAUYUCAN',
      code: '11',
      parentId: '3272'
    }
  ],
  3285: [
    {
      id: '3287',
      name: 'BELLAVISTA',
      code: '02',
      parentId: '3285'
    },
    {
      id: '3286',
      name: 'CALLAO',
      code: '01',
      parentId: '3285'
    },
    {
      id: '3288',
      name: 'CARMEN DE LA LEGUA REYNOSO',
      code: '03',
      parentId: '3285'
    },
    {
      id: '3289',
      name: 'LA PERLA',
      code: '04',
      parentId: '3285'
    },
    {
      id: '3290',
      name: 'LA PUNTA',
      code: '05',
      parentId: '3285'
    },
    {
      id: '3291',
      name: 'VENTANILLA',
      code: '06',
      parentId: '3285'
    }
  ],
  3293: [
    {
      id: '3295',
      name: 'CCORCA',
      code: '02',
      parentId: '3293'
    },
    {
      id: '3294',
      name: 'CUSCO',
      code: '01',
      parentId: '3293'
    },
    {
      id: '3296',
      name: 'POROY',
      code: '03',
      parentId: '3293'
    },
    {
      id: '3297',
      name: 'SAN JERONIMO',
      code: '04',
      parentId: '3293'
    },
    {
      id: '3298',
      name: 'SAN SEBASTIAN',
      code: '05',
      parentId: '3293'
    },
    {
      id: '3299',
      name: 'SANTIAGO',
      code: '06',
      parentId: '3293'
    },
    {
      id: '3300',
      name: 'SAYLLA',
      code: '07',
      parentId: '3293'
    },
    {
      id: '3301',
      name: 'WANCHAQ',
      code: '08',
      parentId: '3293'
    }
  ],
  3302: [
    {
      id: '3303',
      name: 'ACOMAYO',
      code: '01',
      parentId: '3302'
    },
    {
      id: '3304',
      name: 'ACOPIA',
      code: '02',
      parentId: '3302'
    },
    {
      id: '3305',
      name: 'ACOS',
      code: '03',
      parentId: '3302'
    },
    {
      id: '3306',
      name: 'MOSOC LLACTA',
      code: '04',
      parentId: '3302'
    },
    {
      id: '3307',
      name: 'POMACANCHI',
      code: '05',
      parentId: '3302'
    },
    {
      id: '3308',
      name: 'RONDOCAN',
      code: '06',
      parentId: '3302'
    },
    {
      id: '3309',
      name: 'SANGARARA',
      code: '07',
      parentId: '3302'
    }
  ],
  3310: [
    {
      id: '3312',
      name: 'ANCAHUASI',
      code: '02',
      parentId: '3310'
    },
    {
      id: '3311',
      name: 'ANTA',
      code: '01',
      parentId: '3310'
    },
    {
      id: '3313',
      name: 'CACHIMAYO',
      code: '03',
      parentId: '3310'
    },
    {
      id: '3314',
      name: 'CHINCHAYPUJIO',
      code: '04',
      parentId: '3310'
    },
    {
      id: '3315',
      name: 'HUAROCONDO',
      code: '05',
      parentId: '3310'
    },
    {
      id: '3316',
      name: 'LIMATAMBO',
      code: '06',
      parentId: '3310'
    },
    {
      id: '3317',
      name: 'MOLLEPATA',
      code: '07',
      parentId: '3310'
    },
    {
      id: '3318',
      name: 'PUCYURA',
      code: '08',
      parentId: '3310'
    },
    {
      id: '3319',
      name: 'ZURITE',
      code: '09',
      parentId: '3310'
    }
  ],
  3320: [
    {
      id: '3321',
      name: 'CALCA',
      code: '01',
      parentId: '3320'
    },
    {
      id: '3322',
      name: 'COYA',
      code: '02',
      parentId: '3320'
    },
    {
      id: '3323',
      name: 'LAMAY',
      code: '03',
      parentId: '3320'
    },
    {
      id: '3324',
      name: 'LARES',
      code: '04',
      parentId: '3320'
    },
    {
      id: '3325',
      name: 'PISAC',
      code: '05',
      parentId: '3320'
    },
    {
      id: '3326',
      name: 'SAN SALVADOR',
      code: '06',
      parentId: '3320'
    },
    {
      id: '3327',
      name: 'TARAY',
      code: '07',
      parentId: '3320'
    },
    {
      id: '3328',
      name: 'YANATILE',
      code: '08',
      parentId: '3320'
    }
  ],
  3329: [
    {
      id: '3331',
      name: 'CHECCA',
      code: '02',
      parentId: '3329'
    },
    {
      id: '3332',
      name: 'KUNTURKANKI',
      code: '03',
      parentId: '3329'
    },
    {
      id: '3333',
      name: 'LANGUI',
      code: '04',
      parentId: '3329'
    },
    {
      id: '3334',
      name: 'LAYO',
      code: '05',
      parentId: '3329'
    },
    {
      id: '3335',
      name: 'PAMPAMARCA',
      code: '06',
      parentId: '3329'
    },
    {
      id: '3336',
      name: 'QUEHUE',
      code: '07',
      parentId: '3329'
    },
    {
      id: '3337',
      name: 'TUPAC AMARU',
      code: '08',
      parentId: '3329'
    },
    {
      id: '3330',
      name: 'YANAOCA',
      code: '01',
      parentId: '3329'
    }
  ],
  3338: [
    {
      id: '3340',
      name: 'CHECACUPE',
      code: '02',
      parentId: '3338'
    },
    {
      id: '3341',
      name: 'COMBAPATA',
      code: '03',
      parentId: '3338'
    },
    {
      id: '3342',
      name: 'MARANGANI',
      code: '04',
      parentId: '3338'
    },
    {
      id: '3343',
      name: 'PITUMARCA',
      code: '05',
      parentId: '3338'
    },
    {
      id: '3344',
      name: 'SAN PABLO',
      code: '06',
      parentId: '3338'
    },
    {
      id: '3345',
      name: 'SAN PEDRO',
      code: '07',
      parentId: '3338'
    },
    {
      id: '3339',
      name: 'SICUANI',
      code: '01',
      parentId: '3338'
    },
    {
      id: '3346',
      name: 'TINTA',
      code: '08',
      parentId: '3338'
    }
  ],
  3347: [
    {
      id: '3349',
      name: 'CAPACMARCA',
      code: '02',
      parentId: '3347'
    },
    {
      id: '3350',
      name: 'CHAMACA',
      code: '03',
      parentId: '3347'
    },
    {
      id: '3351',
      name: 'COLQUEMARCA',
      code: '04',
      parentId: '3347'
    },
    {
      id: '3352',
      name: 'LIVITACA',
      code: '05',
      parentId: '3347'
    },
    {
      id: '3353',
      name: 'LLUSCO',
      code: '06',
      parentId: '3347'
    },
    {
      id: '3354',
      name: 'QUIQOTA',
      code: '07',
      parentId: '3347'
    },
    {
      id: '3348',
      name: 'SANTO TOMAS',
      code: '01',
      parentId: '3347'
    },
    {
      id: '3355',
      name: 'VELILLE',
      code: '08',
      parentId: '3347'
    }
  ],
  3356: [
    {
      id: '3364',
      name: 'ALTO PICHIGUA',
      code: '08',
      parentId: '3356'
    },
    {
      id: '3358',
      name: 'CONDOROMA',
      code: '02',
      parentId: '3356'
    },
    {
      id: '3359',
      name: 'COPORAQUE',
      code: '03',
      parentId: '3356'
    },
    {
      id: '3357',
      name: 'ESPINAR',
      code: '01',
      parentId: '3356'
    },
    {
      id: '3360',
      name: 'OCORURO',
      code: '04',
      parentId: '3356'
    },
    {
      id: '3361',
      name: 'PALLPATA',
      code: '05',
      parentId: '3356'
    },
    {
      id: '3362',
      name: 'PICHIGUA',
      code: '06',
      parentId: '3356'
    },
    {
      id: '3363',
      name: 'SUYCKUTAMBO',
      code: '07',
      parentId: '3356'
    }
  ],
  3365: [
    {
      id: '3367',
      name: 'ECHARATE',
      code: '02',
      parentId: '3365'
    },
    {
      id: '3368',
      name: 'HUAYOPATA',
      code: '03',
      parentId: '3365'
    },
    {
      id: '3369',
      name: 'MARANURA',
      code: '04',
      parentId: '3365'
    },
    {
      id: '3370',
      name: 'OCOBAMBA',
      code: '05',
      parentId: '3365'
    },
    {
      id: '3375',
      name: 'PICHARI',
      code: '10',
      parentId: '3365'
    },
    {
      id: '3371',
      name: 'QUELLOUNO',
      code: '06',
      parentId: '3365'
    },
    {
      id: '3372',
      name: 'QUIMBIRI',
      code: '07',
      parentId: '3365'
    },
    {
      id: '3366',
      name: 'SANTA ANA',
      code: '01',
      parentId: '3365'
    },
    {
      id: '3373',
      name: 'SANTA TERESA',
      code: '08',
      parentId: '3365'
    },
    {
      id: '3374',
      name: 'VILCABAMBA',
      code: '09',
      parentId: '3365'
    }
  ],
  3376: [
    {
      id: '3378',
      name: 'ACCHA',
      code: '02',
      parentId: '3376'
    },
    {
      id: '3379',
      name: 'CCAPI',
      code: '03',
      parentId: '3376'
    },
    {
      id: '3380',
      name: 'COLCHA',
      code: '04',
      parentId: '3376'
    },
    {
      id: '3381',
      name: 'HUANOQUITE',
      code: '05',
      parentId: '3376'
    },
    {
      id: '3382',
      name: 'OMACHA',
      code: '06',
      parentId: '3376'
    },
    {
      id: '3383',
      name: 'PACCARITAMBO',
      code: '07',
      parentId: '3376'
    },
    {
      id: '3377',
      name: 'PARURO',
      code: '01',
      parentId: '3376'
    },
    {
      id: '3384',
      name: 'PILLPINTO',
      code: '08',
      parentId: '3376'
    },
    {
      id: '3385',
      name: 'YAURISQUE',
      code: '09',
      parentId: '3376'
    }
  ],
  3386: [
    {
      id: '3388',
      name: 'CAICAY',
      code: '02',
      parentId: '3386'
    },
    {
      id: '3389',
      name: 'CHALLABAMBA',
      code: '03',
      parentId: '3386'
    },
    {
      id: '3390',
      name: 'COLQUEPATA',
      code: '04',
      parentId: '3386'
    },
    {
      id: '3391',
      name: 'HUANCARANI',
      code: '05',
      parentId: '3386'
    },
    {
      id: '3392',
      name: 'KOSQIPATA',
      code: '06',
      parentId: '3386'
    },
    {
      id: '3387',
      name: 'PAUCARTAMBO',
      code: '01',
      parentId: '3386'
    }
  ],
  3393: [
    {
      id: '3395',
      name: 'ANDAHUAYLILLAS',
      code: '02',
      parentId: '3393'
    },
    {
      id: '3396',
      name: 'CAMANTI',
      code: '03',
      parentId: '3393'
    },
    {
      id: '3397',
      name: 'CCARHUAYO',
      code: '04',
      parentId: '3393'
    },
    {
      id: '3398',
      name: 'CCATCA',
      code: '05',
      parentId: '3393'
    },
    {
      id: '3399',
      name: 'CUSIPATA',
      code: '06',
      parentId: '3393'
    },
    {
      id: '3400',
      name: 'HUARO',
      code: '07',
      parentId: '3393'
    },
    {
      id: '3401',
      name: 'LUCRE',
      code: '08',
      parentId: '3393'
    },
    {
      id: '3402',
      name: 'MARCAPATA',
      code: '09',
      parentId: '3393'
    },
    {
      id: '3403',
      name: 'OCONGATE',
      code: '10',
      parentId: '3393'
    },
    {
      id: '3404',
      name: 'OROPESA',
      code: '11',
      parentId: '3393'
    },
    {
      id: '3405',
      name: 'QUIQUIJANA',
      code: '12',
      parentId: '3393'
    },
    {
      id: '3394',
      name: 'URCOS',
      code: '01',
      parentId: '3393'
    }
  ],
  3406: [
    {
      id: '3408',
      name: 'CHINCHERO',
      code: '02',
      parentId: '3406'
    },
    {
      id: '3409',
      name: 'HUAYLLABAMBA',
      code: '03',
      parentId: '3406'
    },
    {
      id: '3410',
      name: 'MACHUPICCHU',
      code: '04',
      parentId: '3406'
    },
    {
      id: '3411',
      name: 'MARAS',
      code: '05',
      parentId: '3406'
    },
    {
      id: '3412',
      name: 'OLLANTAYTAMBO',
      code: '06',
      parentId: '3406'
    },
    {
      id: '3407',
      name: 'URUBAMBA',
      code: '01',
      parentId: '3406'
    },
    {
      id: '3413',
      name: 'YUCAY',
      code: '07',
      parentId: '3406'
    }
  ],
  3415: [
    {
      id: '3417',
      name: 'ACOBAMBILLA',
      code: '02',
      parentId: '3415'
    },
    {
      id: '3418',
      name: 'ACORIA',
      code: '03',
      parentId: '3415'
    },
    {
      id: '3433',
      name: 'ASCENSIÓN',
      code: '18',
      parentId: '3415'
    },
    {
      id: '3419',
      name: 'CONAYCA',
      code: '04',
      parentId: '3415'
    },
    {
      id: '3420',
      name: 'CUENCA',
      code: '05',
      parentId: '3415'
    },
    {
      id: '3421',
      name: 'HUACHOCOLPA',
      code: '06',
      parentId: '3415'
    },
    {
      id: '3416',
      name: 'HUANCAVELICA',
      code: '01',
      parentId: '3415'
    },
    {
      id: '3434',
      name: 'HUANDO',
      code: '19',
      parentId: '3415'
    },
    {
      id: '3422',
      name: 'HUAYLLAHUARA',
      code: '07',
      parentId: '3415'
    },
    {
      id: '3423',
      name: 'IZCUCHACA',
      code: '08',
      parentId: '3415'
    },
    {
      id: '3424',
      name: 'LARIA',
      code: '09',
      parentId: '3415'
    },
    {
      id: '3425',
      name: 'MANTA',
      code: '10',
      parentId: '3415'
    },
    {
      id: '3426',
      name: 'MARISCAL CACERES',
      code: '11',
      parentId: '3415'
    },
    {
      id: '3427',
      name: 'MOYA',
      code: '12',
      parentId: '3415'
    },
    {
      id: '3428',
      name: 'NUEVO OCCORO',
      code: '13',
      parentId: '3415'
    },
    {
      id: '3429',
      name: 'PALCA',
      code: '14',
      parentId: '3415'
    },
    {
      id: '3430',
      name: 'PILCHACA',
      code: '15',
      parentId: '3415'
    },
    {
      id: '3431',
      name: 'VILCA',
      code: '16',
      parentId: '3415'
    },
    {
      id: '3432',
      name: 'YAULI',
      code: '17',
      parentId: '3415'
    }
  ],
  3435: [
    {
      id: '3436',
      name: 'ACOBAMBA',
      code: '01',
      parentId: '3435'
    },
    {
      id: '3437',
      name: 'ANDABAMBA',
      code: '02',
      parentId: '3435'
    },
    {
      id: '3438',
      name: 'ANTA',
      code: '03',
      parentId: '3435'
    },
    {
      id: '3439',
      name: 'CAJA',
      code: '04',
      parentId: '3435'
    },
    {
      id: '3440',
      name: 'MARCAS',
      code: '05',
      parentId: '3435'
    },
    {
      id: '3441',
      name: 'PAUCARA',
      code: '06',
      parentId: '3435'
    },
    {
      id: '3442',
      name: 'POMACOCHA',
      code: '07',
      parentId: '3435'
    },
    {
      id: '3443',
      name: 'ROSARIO',
      code: '08',
      parentId: '3435'
    }
  ],
  3444: [
    {
      id: '3446',
      name: 'ANCHONGA',
      code: '02',
      parentId: '3444'
    },
    {
      id: '3447',
      name: 'CALLANMARCA',
      code: '03',
      parentId: '3444'
    },
    {
      id: '3448',
      name: 'CCOCHACCASA',
      code: '04',
      parentId: '3444'
    },
    {
      id: '3449',
      name: 'CHINCHO',
      code: '05',
      parentId: '3444'
    },
    {
      id: '3450',
      name: 'CONGALLA',
      code: '06',
      parentId: '3444'
    },
    {
      id: '3451',
      name: 'HUANCA-HUANCA',
      code: '07',
      parentId: '3444'
    },
    {
      id: '3452',
      name: 'HUAYLLAY GRANDE',
      code: '08',
      parentId: '3444'
    },
    {
      id: '3453',
      name: 'JULCAMARCA',
      code: '09',
      parentId: '3444'
    },
    {
      id: '3445',
      name: 'LIRCAY',
      code: '01',
      parentId: '3444'
    },
    {
      id: '3454',
      name: 'SAN ANTONIO DE ANTAPARCO',
      code: '10',
      parentId: '3444'
    },
    {
      id: '3455',
      name: 'SANTO TOMAS DE PATA',
      code: '11',
      parentId: '3444'
    },
    {
      id: '3456',
      name: 'SECCLLA',
      code: '12',
      parentId: '3444'
    }
  ],
  3457: [
    {
      id: '3459',
      name: 'ARMA',
      code: '02',
      parentId: '3457'
    },
    {
      id: '3460',
      name: 'AURAHUA',
      code: '03',
      parentId: '3457'
    },
    {
      id: '3461',
      name: 'CAPILLAS',
      code: '04',
      parentId: '3457'
    },
    {
      id: '3458',
      name: 'CASTROVIRREYNA',
      code: '01',
      parentId: '3457'
    },
    {
      id: '3462',
      name: 'CHUPAMARCA',
      code: '05',
      parentId: '3457'
    },
    {
      id: '3463',
      name: 'COCAS',
      code: '06',
      parentId: '3457'
    },
    {
      id: '3464',
      name: 'HUACHOS',
      code: '07',
      parentId: '3457'
    },
    {
      id: '3465',
      name: 'HUAMATAMBO',
      code: '08',
      parentId: '3457'
    },
    {
      id: '3466',
      name: 'MOLLEPAMPA',
      code: '09',
      parentId: '3457'
    },
    {
      id: '3467',
      name: 'SAN JUAN',
      code: '10',
      parentId: '3457'
    },
    {
      id: '3468',
      name: 'SANTA ANA',
      code: '11',
      parentId: '3457'
    },
    {
      id: '3469',
      name: 'TANTARA',
      code: '12',
      parentId: '3457'
    },
    {
      id: '3470',
      name: 'TICRAPO',
      code: '13',
      parentId: '3457'
    }
  ],
  3471: [
    {
      id: '3473',
      name: 'ANCO',
      code: '02',
      parentId: '3471'
    },
    {
      id: '3474',
      name: 'CHINCHIHUASI',
      code: '03',
      parentId: '3471'
    },
    {
      id: '3472',
      name: 'CHURCAMPA',
      code: '01',
      parentId: '3471'
    },
    {
      id: '3475',
      name: 'EL CARMEN',
      code: '04',
      parentId: '3471'
    },
    {
      id: '3476',
      name: 'LA MERCED',
      code: '05',
      parentId: '3471'
    },
    {
      id: '3477',
      name: 'LOCROJA',
      code: '06',
      parentId: '3471'
    },
    {
      id: '3481',
      name: 'PACHAMARCA',
      code: '10',
      parentId: '3471'
    },
    {
      id: '3478',
      name: 'PAUCARBAMBA',
      code: '07',
      parentId: '3471'
    },
    {
      id: '3479',
      name: 'SAN MIGUEL DE MAYOCC',
      code: '08',
      parentId: '3471'
    },
    {
      id: '3480',
      name: 'SAN PEDRO DE CORIS',
      code: '09',
      parentId: '3471'
    }
  ],
  3482: [
    {
      id: '3484',
      name: 'AYAVI',
      code: '02',
      parentId: '3482'
    },
    {
      id: '3485',
      name: 'CORDOVA',
      code: '03',
      parentId: '3482'
    },
    {
      id: '3486',
      name: 'HUAYACUNDO ARMA',
      code: '04',
      parentId: '3482'
    },
    {
      id: '3483',
      name: 'HUAYTARA',
      code: '01',
      parentId: '3482'
    },
    {
      id: '3487',
      name: 'LARAMARCA',
      code: '05',
      parentId: '3482'
    },
    {
      id: '3488',
      name: 'OCOYO',
      code: '06',
      parentId: '3482'
    },
    {
      id: '3489',
      name: 'PILPICHACA',
      code: '07',
      parentId: '3482'
    },
    {
      id: '3490',
      name: 'QUERCO',
      code: '08',
      parentId: '3482'
    },
    {
      id: '3491',
      name: 'QUITO-ARMA',
      code: '09',
      parentId: '3482'
    },
    {
      id: '3492',
      name: 'SAN ANTONIO DE CUSICANCHA',
      code: '10',
      parentId: '3482'
    },
    {
      id: '3493',
      name: 'SAN FRANCISCO DE SANGAYAICO',
      code: '11',
      parentId: '3482'
    },
    {
      id: '3494',
      name: 'SAN ISIDRO',
      code: '12',
      parentId: '3482'
    },
    {
      id: '3495',
      name: 'SANTIAGO DE CHOCORVOS',
      code: '13',
      parentId: '3482'
    },
    {
      id: '3496',
      name: 'SANTIAGO DE QUIRAHUARA',
      code: '14',
      parentId: '3482'
    },
    {
      id: '3497',
      name: 'SANTO DOMINGO DE CAPILLAS',
      code: '15',
      parentId: '3482'
    },
    {
      id: '3498',
      name: 'TAMBO',
      code: '16',
      parentId: '3482'
    }
  ],
  3499: [
    {
      id: '3501',
      name: 'ACOSTAMBO',
      code: '02',
      parentId: '3499'
    },
    {
      id: '3502',
      name: 'ACRAQUIA',
      code: '03',
      parentId: '3499'
    },
    {
      id: '3503',
      name: 'AHUAYCHA',
      code: '04',
      parentId: '3499'
    },
    {
      id: '3504',
      name: 'COLCABAMBA',
      code: '05',
      parentId: '3499'
    },
    {
      id: '3505',
      name: 'DANIEL HERNANDEZ',
      code: '06',
      parentId: '3499'
    },
    {
      id: '3506',
      name: 'HUACHOCOLPA',
      code: '07',
      parentId: '3499'
    },
    {
      id: '3507',
      name: 'HUANDO',
      code: '08',
      parentId: '3499'
    },
    {
      id: '3508',
      name: 'HUARIBAMBA',
      code: '09',
      parentId: '3499'
    },
    {
      id: '3511',
      name: 'PACHAMARCA',
      code: '12',
      parentId: '3499'
    },
    {
      id: '3500',
      name: 'PAMPAS',
      code: '01',
      parentId: '3499'
    },
    {
      id: '3510',
      name: 'PAZOS',
      code: '11',
      parentId: '3499'
    },
    {
      id: '3509',
      name: 'QAHUIMPUQUIO',
      code: '10',
      parentId: '3499'
    },
    {
      id: '3512',
      name: 'QUISHUAR',
      code: '13',
      parentId: '3499'
    },
    {
      id: '3513',
      name: 'SALCABAMBA',
      code: '14',
      parentId: '3499'
    },
    {
      id: '3514',
      name: 'SALCAHUASI',
      code: '15',
      parentId: '3499'
    },
    {
      id: '3515',
      name: 'SAN MARCOS DE ROCCHAC',
      code: '16',
      parentId: '3499'
    },
    {
      id: '3516',
      name: 'SURCUBAMBA',
      code: '17',
      parentId: '3499'
    },
    {
      id: '3517',
      name: 'TINTAY PUNCU',
      code: '18',
      parentId: '3499'
    }
  ],
  3519: [
    {
      id: '3521',
      name: 'AMARILIS',
      code: '02',
      parentId: '3519'
    },
    {
      id: '3522',
      name: 'CHINCHAO',
      code: '03',
      parentId: '3519'
    },
    {
      id: '3523',
      name: 'CHURUBAMBA',
      code: '04',
      parentId: '3519'
    },
    {
      id: '3520',
      name: 'HUANUCO',
      code: '01',
      parentId: '3519'
    },
    {
      id: '3524',
      name: 'MARGOS',
      code: '05',
      parentId: '3519'
    },
    {
      id: '3530',
      name: 'PILLCOMARCA',
      code: '11',
      parentId: '3519'
    },
    {
      id: '3525',
      name: 'QUISQUI',
      code: '06',
      parentId: '3519'
    },
    {
      id: '3526',
      name: 'SAN FRANCISCO DE CAYRAN',
      code: '07',
      parentId: '3519'
    },
    {
      id: '3527',
      name: 'SAN PEDRO DE CHAULAN',
      code: '08',
      parentId: '3519'
    },
    {
      id: '3528',
      name: 'SANTA MARIA DEL VALLE',
      code: '09',
      parentId: '3519'
    },
    {
      id: '3529',
      name: 'YARUMAYO',
      code: '10',
      parentId: '3519'
    }
  ],
  3531: [
    {
      id: '3532',
      name: 'AMBO',
      code: '01',
      parentId: '3531'
    },
    {
      id: '3533',
      name: 'CAYNA',
      code: '02',
      parentId: '3531'
    },
    {
      id: '3534',
      name: 'COLPAS',
      code: '03',
      parentId: '3531'
    },
    {
      id: '3535',
      name: 'CONCHAMARCA',
      code: '04',
      parentId: '3531'
    },
    {
      id: '3536',
      name: 'HUACAR',
      code: '05',
      parentId: '3531'
    },
    {
      id: '3537',
      name: 'SAN FRANCISCO',
      code: '06',
      parentId: '3531'
    },
    {
      id: '3538',
      name: 'SAN RAFAEL',
      code: '07',
      parentId: '3531'
    },
    {
      id: '3539',
      name: 'TOMAY KICHWA',
      code: '08',
      parentId: '3531'
    }
  ],
  3540: [
    {
      id: '3542',
      name: 'CHUQUIS',
      code: '07',
      parentId: '3540'
    },
    {
      id: '3541',
      name: 'LA UNION',
      code: '01',
      parentId: '3540'
    },
    {
      id: '3543',
      name: 'MARIAS',
      code: '11',
      parentId: '3540'
    },
    {
      id: '3544',
      name: 'PACHAS',
      code: '13',
      parentId: '3540'
    },
    {
      id: '3545',
      name: 'QUIVILLA',
      code: '16',
      parentId: '3540'
    },
    {
      id: '3546',
      name: 'RIPAN',
      code: '17',
      parentId: '3540'
    },
    {
      id: '3547',
      name: 'SHUNQUI',
      code: '21',
      parentId: '3540'
    },
    {
      id: '3548',
      name: 'SILLAPATA',
      code: '22',
      parentId: '3540'
    },
    {
      id: '3549',
      name: 'YANAS',
      code: '23',
      parentId: '3540'
    }
  ],
  3550: [
    {
      id: '3552',
      name: 'CANCHABAMBA',
      code: '02',
      parentId: '3550'
    },
    {
      id: '3553',
      name: 'COCHABAMBA',
      code: '03',
      parentId: '3550'
    },
    {
      id: '3551',
      name: 'HUACAYBAMBA',
      code: '01',
      parentId: '3550'
    },
    {
      id: '3554',
      name: 'PINRA',
      code: '04',
      parentId: '3550'
    }
  ],
  3555: [
    {
      id: '3557',
      name: 'ARANCAY',
      code: '02',
      parentId: '3555'
    },
    {
      id: '3558',
      name: 'CHAVIN DE PARIARCA',
      code: '03',
      parentId: '3555'
    },
    {
      id: '3559',
      name: 'JACAS GRANDE',
      code: '04',
      parentId: '3555'
    },
    {
      id: '3560',
      name: 'JIRCAN',
      code: '05',
      parentId: '3555'
    },
    {
      id: '3556',
      name: 'LLATA',
      code: '01',
      parentId: '3555'
    },
    {
      id: '3561',
      name: 'MIRAFLORES',
      code: '06',
      parentId: '3555'
    },
    {
      id: '3562',
      name: 'MONZON',
      code: '07',
      parentId: '3555'
    },
    {
      id: '3563',
      name: 'PUNCHAO',
      code: '08',
      parentId: '3555'
    },
    {
      id: '3564',
      name: 'PUQOS',
      code: '09',
      parentId: '3555'
    },
    {
      id: '3565',
      name: 'SINGA',
      code: '10',
      parentId: '3555'
    },
    {
      id: '3566',
      name: 'TANTAMAYO',
      code: '11',
      parentId: '3555'
    }
  ],
  3567: [
    {
      id: '3569',
      name: 'DANIEL ALOMIAS ROBLES',
      code: '02',
      parentId: '3567'
    },
    {
      id: '3570',
      name: 'HERMILIO VALDIZAN',
      code: '03',
      parentId: '3567'
    },
    {
      id: '3571',
      name: 'JOSE CRESPO Y CASTILLO',
      code: '04',
      parentId: '3567'
    },
    {
      id: '3572',
      name: 'LUYANDO',
      code: '05',
      parentId: '3567'
    },
    {
      id: '3573',
      name: 'MARIANO DAMASO BERAUN',
      code: '06',
      parentId: '3567'
    },
    {
      id: '3568',
      name: 'RUPA-RUPA',
      code: '01',
      parentId: '3567'
    }
  ],
  3574: [
    {
      id: '3576',
      name: 'CHOLON',
      code: '02',
      parentId: '3574'
    },
    {
      id: '3575',
      name: 'HUACRACHUCO',
      code: '01',
      parentId: '3574'
    },
    {
      id: '3577',
      name: 'SAN BUENAVENTURA',
      code: '03',
      parentId: '3574'
    }
  ],
  3578: [
    {
      id: '3580',
      name: 'CHAGLLA',
      code: '02',
      parentId: '3578'
    },
    {
      id: '3581',
      name: 'MOLINO',
      code: '03',
      parentId: '3578'
    },
    {
      id: '3579',
      name: 'PANAO',
      code: '01',
      parentId: '3578'
    },
    {
      id: '3582',
      name: 'UMARI',
      code: '04',
      parentId: '3578'
    }
  ],
  3583: [
    {
      id: '3585',
      name: 'CODO DEL POZUZO',
      code: '02',
      parentId: '3583'
    },
    {
      id: '3586',
      name: 'HONORIA',
      code: '03',
      parentId: '3583'
    },
    {
      id: '3584',
      name: 'PUERTO INCA',
      code: '01',
      parentId: '3583'
    },
    {
      id: '3587',
      name: 'TOURNAVISTA',
      code: '04',
      parentId: '3583'
    },
    {
      id: '3588',
      name: 'YUYAPICHIS',
      code: '05',
      parentId: '3583'
    }
  ],
  3589: [
    {
      id: '3591',
      name: 'BAQOS',
      code: '02',
      parentId: '3589'
    },
    {
      id: '3590',
      name: 'JESUS',
      code: '01',
      parentId: '3589'
    },
    {
      id: '3592',
      name: 'JIVIA',
      code: '03',
      parentId: '3589'
    },
    {
      id: '3593',
      name: 'QUEROPALCA',
      code: '04',
      parentId: '3589'
    },
    {
      id: '3594',
      name: 'RONDOS',
      code: '05',
      parentId: '3589'
    },
    {
      id: '3595',
      name: 'SAN FRANCISCO DE ASIS',
      code: '06',
      parentId: '3589'
    },
    {
      id: '3596',
      name: 'SAN MIGUEL DE CAURI',
      code: '07',
      parentId: '3589'
    }
  ],
  3597: [
    {
      id: '3599',
      name: 'CAHUAC',
      code: '02',
      parentId: '3597'
    },
    {
      id: '3600',
      name: 'CHACABAMBA',
      code: '03',
      parentId: '3597'
    },
    {
      id: '3598',
      name: 'CHAVINILLO',
      code: '01',
      parentId: '3597'
    },
    {
      id: '3605',
      name: 'CHORAS',
      code: '08',
      parentId: '3597'
    },
    {
      id: '3601',
      name: 'CHUPAN',
      code: '04',
      parentId: '3597'
    },
    {
      id: '3602',
      name: 'JACAS CHICO',
      code: '05',
      parentId: '3597'
    },
    {
      id: '3603',
      name: 'OBAS',
      code: '06',
      parentId: '3597'
    },
    {
      id: '3604',
      name: 'PAMPAMARCA',
      code: '07',
      parentId: '3597'
    }
  ],
  3607: [
    {
      id: '3608',
      name: 'ICA',
      code: '01',
      parentId: '3607'
    },
    {
      id: '3609',
      name: 'LA TINGUIQA',
      code: '02',
      parentId: '3607'
    },
    {
      id: '3610',
      name: 'LOS AQUIJES',
      code: '03',
      parentId: '3607'
    },
    {
      id: '3611',
      name: 'OCUCAJE',
      code: '04',
      parentId: '3607'
    },
    {
      id: '3612',
      name: 'PACHACUTEC',
      code: '05',
      parentId: '3607'
    },
    {
      id: '3613',
      name: 'PARCONA',
      code: '06',
      parentId: '3607'
    },
    {
      id: '3614',
      name: 'PUEBLO NUEVO',
      code: '07',
      parentId: '3607'
    },
    {
      id: '3615',
      name: 'SALAS',
      code: '08',
      parentId: '3607'
    },
    {
      id: '3616',
      name: 'SAN JOSE DE LOS MOLINOS',
      code: '09',
      parentId: '3607'
    },
    {
      id: '3617',
      name: 'SAN JUAN BAUTISTA',
      code: '10',
      parentId: '3607'
    },
    {
      id: '3618',
      name: 'SANTIAGO',
      code: '11',
      parentId: '3607'
    },
    {
      id: '3619',
      name: 'SUBTANJALLA',
      code: '12',
      parentId: '3607'
    },
    {
      id: '3620',
      name: 'TATE',
      code: '13',
      parentId: '3607'
    },
    {
      id: '3621',
      name: 'YAUCA DEL ROSARIO  1/',
      code: '14',
      parentId: '3607'
    }
  ],
  3622: [
    {
      id: '3624',
      name: 'ALTO LARAN',
      code: '02',
      parentId: '3622'
    },
    {
      id: '3625',
      name: 'CHAVIN',
      code: '03',
      parentId: '3622'
    },
    {
      id: '3623',
      name: 'CHINCHA ALTA',
      code: '01',
      parentId: '3622'
    },
    {
      id: '3626',
      name: 'CHINCHA BAJA',
      code: '04',
      parentId: '3622'
    },
    {
      id: '3627',
      name: 'EL CARMEN',
      code: '05',
      parentId: '3622'
    },
    {
      id: '3628',
      name: 'GROCIO PRADO',
      code: '06',
      parentId: '3622'
    },
    {
      id: '3629',
      name: 'PUEBLO NUEVO',
      code: '07',
      parentId: '3622'
    },
    {
      id: '3630',
      name: 'SAN JUAN DE YANAC',
      code: '08',
      parentId: '3622'
    },
    {
      id: '3631',
      name: 'SAN PEDRO DE HUACARPANA',
      code: '09',
      parentId: '3622'
    },
    {
      id: '3632',
      name: 'SUNAMPE',
      code: '10',
      parentId: '3622'
    },
    {
      id: '3633',
      name: 'TAMBO DE MORA',
      code: '11',
      parentId: '3622'
    }
  ],
  3634: [
    {
      id: '3636',
      name: 'CHANGUILLO',
      code: '02',
      parentId: '3634'
    },
    {
      id: '3637',
      name: 'EL INGENIO',
      code: '03',
      parentId: '3634'
    },
    {
      id: '3638',
      name: 'MARCONA',
      code: '04',
      parentId: '3634'
    },
    {
      id: '3635',
      name: 'NAZCA',
      code: '01',
      parentId: '3634'
    },
    {
      id: '3639',
      name: 'VISTA ALEGRE',
      code: '05',
      parentId: '3634'
    }
  ],
  3640: [
    {
      id: '3642',
      name: 'LLIPATA',
      code: '02',
      parentId: '3640'
    },
    {
      id: '3641',
      name: 'PALPA',
      code: '01',
      parentId: '3640'
    },
    {
      id: '3643',
      name: 'RIO GRANDE',
      code: '03',
      parentId: '3640'
    },
    {
      id: '3644',
      name: 'SANTA CRUZ',
      code: '04',
      parentId: '3640'
    },
    {
      id: '3645',
      name: 'TIBILLO',
      code: '05',
      parentId: '3640'
    }
  ],
  3646: [
    {
      id: '3648',
      name: 'HUANCANO',
      code: '02',
      parentId: '3646'
    },
    {
      id: '3649',
      name: 'HUMAY',
      code: '03',
      parentId: '3646'
    },
    {
      id: '3650',
      name: 'INDEPENDENCIA',
      code: '04',
      parentId: '3646'
    },
    {
      id: '3651',
      name: 'PARACAS',
      code: '05',
      parentId: '3646'
    },
    {
      id: '3647',
      name: 'PISCO',
      code: '01',
      parentId: '3646'
    },
    {
      id: '3652',
      name: 'SAN ANDRES',
      code: '06',
      parentId: '3646'
    },
    {
      id: '3653',
      name: 'SAN CLEMENTE',
      code: '07',
      parentId: '3646'
    },
    {
      id: '3654',
      name: 'TUPAC AMARU INCA',
      code: '08',
      parentId: '3646'
    }
  ],
  3656: [
    {
      id: '3658',
      name: 'CARHUACALLANGA',
      code: '04',
      parentId: '3656'
    },
    {
      id: '3659',
      name: 'CHACAPAMPA',
      code: '05',
      parentId: '3656'
    },
    {
      id: '3660',
      name: 'CHICCHE',
      code: '06',
      parentId: '3656'
    },
    {
      id: '3661',
      name: 'CHILCA',
      code: '07',
      parentId: '3656'
    },
    {
      id: '3662',
      name: 'CHONGOS ALTO',
      code: '08',
      parentId: '3656'
    },
    {
      id: '3663',
      name: 'CHUPURO',
      code: '11',
      parentId: '3656'
    },
    {
      id: '3664',
      name: 'COLCA',
      code: '12',
      parentId: '3656'
    },
    {
      id: '3665',
      name: 'CULLHUAS',
      code: '13',
      parentId: '3656'
    },
    {
      id: '3666',
      name: 'EL TAMBO',
      code: '14',
      parentId: '3656'
    },
    {
      id: '3667',
      name: 'HUACRAPUQUIO',
      code: '16',
      parentId: '3656'
    },
    {
      id: '3668',
      name: 'HUALHUAS',
      code: '17',
      parentId: '3656'
    },
    {
      id: '3669',
      name: 'HUANCAN',
      code: '19',
      parentId: '3656'
    },
    {
      id: '3657',
      name: 'HUANCAYO',
      code: '01',
      parentId: '3656'
    },
    {
      id: '3670',
      name: 'HUASICANCHA',
      code: '20',
      parentId: '3656'
    },
    {
      id: '3671',
      name: 'HUAYUCACHI',
      code: '21',
      parentId: '3656'
    },
    {
      id: '3672',
      name: 'INGENIO',
      code: '22',
      parentId: '3656'
    },
    {
      id: '3673',
      name: 'PARIAHUANCA',
      code: '24',
      parentId: '3656'
    },
    {
      id: '3674',
      name: 'PILCOMAYO',
      code: '25',
      parentId: '3656'
    },
    {
      id: '3675',
      name: 'PUCARA',
      code: '26',
      parentId: '3656'
    },
    {
      id: '3676',
      name: 'QUICHUAY',
      code: '27',
      parentId: '3656'
    },
    {
      id: '3677',
      name: 'QUILCAS',
      code: '28',
      parentId: '3656'
    },
    {
      id: '3678',
      name: 'SAN AGUSTIN',
      code: '29',
      parentId: '3656'
    },
    {
      id: '3679',
      name: 'SAN JERONIMO DE TUNAN',
      code: '30',
      parentId: '3656'
    },
    {
      id: '3683',
      name: 'SANTO DOMINGO DE ACOBAMBA',
      code: '35',
      parentId: '3656'
    },
    {
      id: '3681',
      name: 'SAPALLANGA',
      code: '33',
      parentId: '3656'
    },
    {
      id: '3680',
      name: 'SAQO',
      code: '32',
      parentId: '3656'
    },
    {
      id: '3682',
      name: 'SICAYA',
      code: '34',
      parentId: '3656'
    },
    {
      id: '3684',
      name: 'VIQUES',
      code: '36',
      parentId: '3656'
    }
  ],
  3685: [
    {
      id: '3687',
      name: 'ACO',
      code: '02',
      parentId: '3685'
    },
    {
      id: '3688',
      name: 'ANDAMARCA',
      code: '03',
      parentId: '3685'
    },
    {
      id: '3689',
      name: 'CHAMBARA',
      code: '04',
      parentId: '3685'
    },
    {
      id: '3690',
      name: 'COCHAS',
      code: '05',
      parentId: '3685'
    },
    {
      id: '3691',
      name: 'COMAS',
      code: '06',
      parentId: '3685'
    },
    {
      id: '3686',
      name: 'CONCEPCION',
      code: '01',
      parentId: '3685'
    },
    {
      id: '3692',
      name: 'HEROINAS TOLEDO',
      code: '07',
      parentId: '3685'
    },
    {
      id: '3693',
      name: 'MANZANARES',
      code: '08',
      parentId: '3685'
    },
    {
      id: '3694',
      name: 'MARISCAL CASTILLA',
      code: '09',
      parentId: '3685'
    },
    {
      id: '3695',
      name: 'MATAHUASI',
      code: '10',
      parentId: '3685'
    },
    {
      id: '3696',
      name: 'MITO',
      code: '11',
      parentId: '3685'
    },
    {
      id: '3697',
      name: 'NUEVE DE JULIO',
      code: '12',
      parentId: '3685'
    },
    {
      id: '3698',
      name: 'ORCOTUNA',
      code: '13',
      parentId: '3685'
    },
    {
      id: '3699',
      name: 'SAN JOSE DE QUERO',
      code: '14',
      parentId: '3685'
    },
    {
      id: '3700',
      name: 'SANTA ROSA DE OCOPA',
      code: '15',
      parentId: '3685'
    }
  ],
  3701: [
    {
      id: '3702',
      name: 'CHANCHAMAYO',
      code: '01',
      parentId: '3701'
    },
    {
      id: '3703',
      name: 'PERENE',
      code: '02',
      parentId: '3701'
    },
    {
      id: '3704',
      name: 'PICHANAQUI',
      code: '03',
      parentId: '3701'
    },
    {
      id: '3705',
      name: 'SAN LUIS DE SHUARO',
      code: '04',
      parentId: '3701'
    },
    {
      id: '3706',
      name: 'SAN RAMON',
      code: '05',
      parentId: '3701'
    },
    {
      id: '3707',
      name: 'VITOC',
      code: '06',
      parentId: '3701'
    }
  ],
  3708: [
    {
      id: '3710',
      name: 'ACOLLA',
      code: '02',
      parentId: '3708'
    },
    {
      id: '3711',
      name: 'APATA',
      code: '03',
      parentId: '3708'
    },
    {
      id: '3712',
      name: 'ATAURA',
      code: '04',
      parentId: '3708'
    },
    {
      id: '3713',
      name: 'CANCHAYLLO',
      code: '05',
      parentId: '3708'
    },
    {
      id: '3714',
      name: 'CURICACA',
      code: '06',
      parentId: '3708'
    },
    {
      id: '3715',
      name: 'EL MANTARO',
      code: '07',
      parentId: '3708'
    },
    {
      id: '3716',
      name: 'HUAMALI',
      code: '08',
      parentId: '3708'
    },
    {
      id: '3717',
      name: 'HUARIPAMPA',
      code: '09',
      parentId: '3708'
    },
    {
      id: '3718',
      name: 'HUERTAS',
      code: '10',
      parentId: '3708'
    },
    {
      id: '3719',
      name: 'JANJAILLO',
      code: '11',
      parentId: '3708'
    },
    {
      id: '3709',
      name: 'JAUJA',
      code: '01',
      parentId: '3708'
    },
    {
      id: '3720',
      name: 'JULCAN',
      code: '12',
      parentId: '3708'
    },
    {
      id: '3721',
      name: 'LEONOR ORDOQEZ',
      code: '13',
      parentId: '3708'
    },
    {
      id: '3722',
      name: 'LLOCLLAPAMPA',
      code: '14',
      parentId: '3708'
    },
    {
      id: '3723',
      name: 'MARCO',
      code: '15',
      parentId: '3708'
    },
    {
      id: '3724',
      name: 'MASMA',
      code: '16',
      parentId: '3708'
    },
    {
      id: '3725',
      name: 'MASMA CHICCHE',
      code: '17',
      parentId: '3708'
    },
    {
      id: '3726',
      name: 'MOLINOS',
      code: '18',
      parentId: '3708'
    },
    {
      id: '3727',
      name: 'MONOBAMBA',
      code: '19',
      parentId: '3708'
    },
    {
      id: '3728',
      name: 'MUQUI',
      code: '20',
      parentId: '3708'
    },
    {
      id: '3729',
      name: 'MUQUIYAUYO',
      code: '21',
      parentId: '3708'
    },
    {
      id: '3730',
      name: 'PACA',
      code: '22',
      parentId: '3708'
    },
    {
      id: '3731',
      name: 'PACCHA',
      code: '23',
      parentId: '3708'
    },
    {
      id: '3732',
      name: 'PANCAN',
      code: '24',
      parentId: '3708'
    },
    {
      id: '3733',
      name: 'PARCO',
      code: '25',
      parentId: '3708'
    },
    {
      id: '3734',
      name: 'POMACANCHA',
      code: '26',
      parentId: '3708'
    },
    {
      id: '3735',
      name: 'RICRAN',
      code: '27',
      parentId: '3708'
    },
    {
      id: '3736',
      name: 'SAN LORENZO',
      code: '28',
      parentId: '3708'
    },
    {
      id: '3737',
      name: 'SAN PEDRO DE CHUNAN',
      code: '29',
      parentId: '3708'
    },
    {
      id: '3738',
      name: 'SAUSA',
      code: '30',
      parentId: '3708'
    },
    {
      id: '3739',
      name: 'SINCOS',
      code: '31',
      parentId: '3708'
    },
    {
      id: '3740',
      name: 'TUNAN MARCA',
      code: '32',
      parentId: '3708'
    },
    {
      id: '3741',
      name: 'YAULI',
      code: '33',
      parentId: '3708'
    },
    {
      id: '3742',
      name: 'YAUYOS',
      code: '34',
      parentId: '3708'
    }
  ],
  3743: [
    {
      id: '3745',
      name: 'CARHUAMAYO',
      code: '02',
      parentId: '3743'
    },
    {
      id: '3744',
      name: 'JUNIN',
      code: '01',
      parentId: '3743'
    },
    {
      id: '3746',
      name: 'ONDORES',
      code: '03',
      parentId: '3743'
    },
    {
      id: '3747',
      name: 'ULCUMAYO',
      code: '04',
      parentId: '3743'
    }
  ],
  3748: [
    {
      id: '3750',
      name: 'COVIRIALI',
      code: '02',
      parentId: '3748'
    },
    {
      id: '3751',
      name: 'LLAYLLA',
      code: '03',
      parentId: '3748'
    },
    {
      id: '3752',
      name: 'MAZAMARI',
      code: '04',
      parentId: '3748'
    },
    {
      id: '3753',
      name: 'PAMPA HERMOSA',
      code: '05',
      parentId: '3748'
    },
    {
      id: '3754',
      name: 'PANGOA',
      code: '06',
      parentId: '3748'
    },
    {
      id: '3755',
      name: 'RIO NEGRO',
      code: '07',
      parentId: '3748'
    },
    {
      id: '3756',
      name: 'RIO TAMBO',
      code: '08',
      parentId: '3748'
    },
    {
      id: '3749',
      name: 'SATIPO',
      code: '01',
      parentId: '3748'
    }
  ],
  3757: [
    {
      id: '3759',
      name: 'ACOBAMBA',
      code: '02',
      parentId: '3757'
    },
    {
      id: '3760',
      name: 'HUARICOLCA',
      code: '03',
      parentId: '3757'
    },
    {
      id: '3761',
      name: 'HUASAHUASI',
      code: '04',
      parentId: '3757'
    },
    {
      id: '3762',
      name: 'LA UNION',
      code: '05',
      parentId: '3757'
    },
    {
      id: '3763',
      name: 'PALCA',
      code: '06',
      parentId: '3757'
    },
    {
      id: '3764',
      name: 'PALCAMAYO',
      code: '07',
      parentId: '3757'
    },
    {
      id: '3765',
      name: 'SAN PEDRO DE CAJAS',
      code: '08',
      parentId: '3757'
    },
    {
      id: '3766',
      name: 'TAPO',
      code: '09',
      parentId: '3757'
    },
    {
      id: '3758',
      name: 'TARMA',
      code: '01',
      parentId: '3757'
    }
  ],
  3767: [
    {
      id: '3769',
      name: 'CHACAPALPA',
      code: '02',
      parentId: '3767'
    },
    {
      id: '3770',
      name: 'HUAY-HUAY',
      code: '03',
      parentId: '3767'
    },
    {
      id: '3768',
      name: 'LA OROYA',
      code: '01',
      parentId: '3767'
    },
    {
      id: '3771',
      name: 'MARCAPOMACOCHA',
      code: '04',
      parentId: '3767'
    },
    {
      id: '3772',
      name: 'MOROCOCHA',
      code: '05',
      parentId: '3767'
    },
    {
      id: '3773',
      name: 'PACCHA',
      code: '06',
      parentId: '3767'
    },
    {
      id: '3774',
      name: 'SANTA BARBARA DE CARHUACAYAN',
      code: '07',
      parentId: '3767'
    },
    {
      id: '3775',
      name: 'SANTA ROSA DE SACCO',
      code: '08',
      parentId: '3767'
    },
    {
      id: '3776',
      name: 'SUITUCANCHA',
      code: '09',
      parentId: '3767'
    },
    {
      id: '3777',
      name: 'YAULI',
      code: '10',
      parentId: '3767'
    }
  ],
  3778: [
    {
      id: '3780',
      name: 'AHUAC',
      code: '02',
      parentId: '3778'
    },
    {
      id: '3781',
      name: 'CHONGOS BAJO',
      code: '03',
      parentId: '3778'
    },
    {
      id: '3779',
      name: 'CHUPACA',
      code: '01',
      parentId: '3778'
    },
    {
      id: '3782',
      name: 'HUACHAC',
      code: '04',
      parentId: '3778'
    },
    {
      id: '3783',
      name: 'HUAMANCACA CHICO',
      code: '05',
      parentId: '3778'
    },
    {
      id: '3784',
      name: 'SAN JUAN DE ISCOS',
      code: '06',
      parentId: '3778'
    },
    {
      id: '3785',
      name: 'SAN JUAN DE JARPA',
      code: '07',
      parentId: '3778'
    },
    {
      id: '3786',
      name: 'TRES DE DICIEMBRE',
      code: '08',
      parentId: '3778'
    },
    {
      id: '3787',
      name: 'YANACANCHA',
      code: '09',
      parentId: '3778'
    }
  ],
  3789: [
    {
      id: '3791',
      name: 'EL PORVENIR',
      code: '02',
      parentId: '3789'
    },
    {
      id: '3792',
      name: 'FLORENCIA DE MORA',
      code: '03',
      parentId: '3789'
    },
    {
      id: '3793',
      name: 'HUANCHACO',
      code: '04',
      parentId: '3789'
    },
    {
      id: '3794',
      name: 'LA ESPERANZA',
      code: '05',
      parentId: '3789'
    },
    {
      id: '3795',
      name: 'LAREDO',
      code: '06',
      parentId: '3789'
    },
    {
      id: '3796',
      name: 'MOCHE',
      code: '07',
      parentId: '3789'
    },
    {
      id: '3797',
      name: 'POROTO',
      code: '08',
      parentId: '3789'
    },
    {
      id: '3798',
      name: 'SALAVERRY',
      code: '09',
      parentId: '3789'
    },
    {
      id: '3799',
      name: 'SIMBAL',
      code: '10',
      parentId: '3789'
    },
    {
      id: '3790',
      name: 'TRUJILLO',
      code: '01',
      parentId: '3789'
    },
    {
      id: '3800',
      name: 'VICTOR LARCO HERRERA',
      code: '11',
      parentId: '3789'
    }
  ],
  3801: [
    {
      id: '3802',
      name: 'ASCOPE',
      code: '01',
      parentId: '3801'
    },
    {
      id: '3809',
      name: 'CASA GRANDE',
      code: '08',
      parentId: '3801'
    },
    {
      id: '3803',
      name: 'CHICAMA',
      code: '02',
      parentId: '3801'
    },
    {
      id: '3804',
      name: 'CHOCOPE',
      code: '03',
      parentId: '3801'
    },
    {
      id: '3805',
      name: 'MAGDALENA DE CAO',
      code: '04',
      parentId: '3801'
    },
    {
      id: '3806',
      name: 'PAIJAN',
      code: '05',
      parentId: '3801'
    },
    {
      id: '3807',
      name: 'RAZURI',
      code: '06',
      parentId: '3801'
    },
    {
      id: '3808',
      name: 'SANTIAGO DE CAO',
      code: '07',
      parentId: '3801'
    }
  ],
  3810: [
    {
      id: '3812',
      name: 'BAMBAMARCA',
      code: '02',
      parentId: '3810'
    },
    {
      id: '3811',
      name: 'BOLIVAR',
      code: '01',
      parentId: '3810'
    },
    {
      id: '3813',
      name: 'CONDORMARCA',
      code: '03',
      parentId: '3810'
    },
    {
      id: '3814',
      name: 'LONGOTEA',
      code: '04',
      parentId: '3810'
    },
    {
      id: '3815',
      name: 'UCHUMARCA',
      code: '05',
      parentId: '3810'
    },
    {
      id: '3816',
      name: 'UCUNCHA',
      code: '06',
      parentId: '3810'
    }
  ],
  3817: [
    {
      id: '3818',
      name: 'CHEPEN',
      code: '01',
      parentId: '3817'
    },
    {
      id: '3819',
      name: 'PACANGA',
      code: '02',
      parentId: '3817'
    },
    {
      id: '3820',
      name: 'PUEBLO NUEVO',
      code: '03',
      parentId: '3817'
    }
  ],
  3821: [
    {
      id: '3823',
      name: 'CALAMARCA',
      code: '02',
      parentId: '3821'
    },
    {
      id: '3824',
      name: 'CARABAMBA',
      code: '03',
      parentId: '3821'
    },
    {
      id: '3825',
      name: 'HUASO',
      code: '04',
      parentId: '3821'
    },
    {
      id: '3822',
      name: 'JULCAN',
      code: '01',
      parentId: '3821'
    }
  ],
  3826: [
    {
      id: '3828',
      name: 'AGALLPAMPA',
      code: '02',
      parentId: '3826'
    },
    {
      id: '3829',
      name: 'CHARAT',
      code: '04',
      parentId: '3826'
    },
    {
      id: '3830',
      name: 'HUARANCHAL',
      code: '05',
      parentId: '3826'
    },
    {
      id: '3831',
      name: 'LA CUESTA',
      code: '06',
      parentId: '3826'
    },
    {
      id: '3832',
      name: 'MACHE',
      code: '08',
      parentId: '3826'
    },
    {
      id: '3827',
      name: 'OTUZCO',
      code: '01',
      parentId: '3826'
    },
    {
      id: '3833',
      name: 'PARANDAY',
      code: '10',
      parentId: '3826'
    },
    {
      id: '3834',
      name: 'SALPO',
      code: '11',
      parentId: '3826'
    },
    {
      id: '3835',
      name: 'SINSICAP',
      code: '13',
      parentId: '3826'
    },
    {
      id: '3836',
      name: 'USQUIL',
      code: '14',
      parentId: '3826'
    }
  ],
  3837: [
    {
      id: '3839',
      name: 'GUADALUPE',
      code: '02',
      parentId: '3837'
    },
    {
      id: '3840',
      name: 'JEQUETEPEQUE',
      code: '03',
      parentId: '3837'
    },
    {
      id: '3841',
      name: 'PACASMAYO',
      code: '04',
      parentId: '3837'
    },
    {
      id: '3842',
      name: 'SAN JOSE',
      code: '05',
      parentId: '3837'
    },
    {
      id: '3838',
      name: 'SAN PEDRO DE LLOC',
      code: '01',
      parentId: '3837'
    }
  ],
  3843: [
    {
      id: '3845',
      name: 'BULDIBUYO',
      code: '02',
      parentId: '3843'
    },
    {
      id: '3846',
      name: 'CHILLIA',
      code: '03',
      parentId: '3843'
    },
    {
      id: '3847',
      name: 'HUANCASPATA',
      code: '04',
      parentId: '3843'
    },
    {
      id: '3848',
      name: 'HUAYLILLAS',
      code: '05',
      parentId: '3843'
    },
    {
      id: '3849',
      name: 'HUAYO',
      code: '06',
      parentId: '3843'
    },
    {
      id: '3850',
      name: 'ONGON',
      code: '07',
      parentId: '3843'
    },
    {
      id: '3851',
      name: 'PARCOY',
      code: '08',
      parentId: '3843'
    },
    {
      id: '3852',
      name: 'PATAZ',
      code: '09',
      parentId: '3843'
    },
    {
      id: '3853',
      name: 'PIAS',
      code: '10',
      parentId: '3843'
    },
    {
      id: '3854',
      name: 'SANTIAGO DE CHALLAS',
      code: '11',
      parentId: '3843'
    },
    {
      id: '3855',
      name: 'TAURIJA',
      code: '12',
      parentId: '3843'
    },
    {
      id: '3844',
      name: 'TAYABAMBA',
      code: '01',
      parentId: '3843'
    },
    {
      id: '3856',
      name: 'URPAY',
      code: '13',
      parentId: '3843'
    }
  ],
  3857: [
    {
      id: '3859',
      name: 'CHUGAY',
      code: '02',
      parentId: '3857'
    },
    {
      id: '3860',
      name: 'COCHORCO',
      code: '03',
      parentId: '3857'
    },
    {
      id: '3861',
      name: 'CURGOS',
      code: '04',
      parentId: '3857'
    },
    {
      id: '3858',
      name: 'HUAMACHUCO',
      code: '01',
      parentId: '3857'
    },
    {
      id: '3862',
      name: 'MARCABAL',
      code: '05',
      parentId: '3857'
    },
    {
      id: '3863',
      name: 'SANAGORAN',
      code: '06',
      parentId: '3857'
    },
    {
      id: '3864',
      name: 'SARIN',
      code: '07',
      parentId: '3857'
    },
    {
      id: '3865',
      name: 'SARTIMBAMBA',
      code: '08',
      parentId: '3857'
    }
  ],
  3866: [
    {
      id: '3868',
      name: 'ANGASMARCA',
      code: '02',
      parentId: '3866'
    },
    {
      id: '3869',
      name: 'CACHICADAN',
      code: '03',
      parentId: '3866'
    },
    {
      id: '3870',
      name: 'MOLLEBAMBA',
      code: '04',
      parentId: '3866'
    },
    {
      id: '3871',
      name: 'MOLLEPATA',
      code: '05',
      parentId: '3866'
    },
    {
      id: '3872',
      name: 'QUIRUVILCA',
      code: '06',
      parentId: '3866'
    },
    {
      id: '3873',
      name: 'SANTA CRUZ DE CHUCA',
      code: '07',
      parentId: '3866'
    },
    {
      id: '3867',
      name: 'SANTIAGO DE CHUCO',
      code: '01',
      parentId: '3866'
    },
    {
      id: '3874',
      name: 'SITABAMBA',
      code: '08',
      parentId: '3866'
    }
  ],
  3875: [
    {
      id: '3876',
      name: 'CASCAS',
      code: '01',
      parentId: '3875'
    },
    {
      id: '3877',
      name: 'LUCMA',
      code: '02',
      parentId: '3875'
    },
    {
      id: '3878',
      name: 'MARMOT',
      code: '03',
      parentId: '3875'
    },
    {
      id: '3879',
      name: 'SAYAPULLO',
      code: '04',
      parentId: '3875'
    }
  ],
  3880: [
    {
      id: '3882',
      name: 'CHAO',
      code: '02',
      parentId: '3880'
    },
    {
      id: '3883',
      name: 'GUADALUPITO',
      code: '03',
      parentId: '3880'
    },
    {
      id: '3881',
      name: 'VIRU',
      code: '01',
      parentId: '3880'
    }
  ],
  3885: [
    {
      id: '3901',
      name: 'CAYALTÍ',
      code: '16',
      parentId: '3885'
    },
    {
      id: '3886',
      name: 'CHICLAYO',
      code: '01',
      parentId: '3885'
    },
    {
      id: '3887',
      name: 'CHONGOYAPE',
      code: '02',
      parentId: '3885'
    },
    {
      id: '3888',
      name: 'ETEN',
      code: '03',
      parentId: '3885'
    },
    {
      id: '3889',
      name: 'ETEN PUERTO',
      code: '04',
      parentId: '3885'
    },
    {
      id: '3890',
      name: 'JOSE LEONARDO ORTIZ',
      code: '05',
      parentId: '3885'
    },
    {
      id: '3891',
      name: 'LA VICTORIA',
      code: '06',
      parentId: '3885'
    },
    {
      id: '3892',
      name: 'LAGUNAS',
      code: '07',
      parentId: '3885'
    },
    {
      id: '3893',
      name: 'MONSEFU',
      code: '08',
      parentId: '3885'
    },
    {
      id: '3894',
      name: 'NUEVA ARICA',
      code: '09',
      parentId: '3885'
    },
    {
      id: '3895',
      name: 'OYOTUN',
      code: '10',
      parentId: '3885'
    },
    {
      id: '3902',
      name: 'PATAPO',
      code: '17',
      parentId: '3885'
    },
    {
      id: '3896',
      name: 'PICSI',
      code: '11',
      parentId: '3885'
    },
    {
      id: '3897',
      name: 'PIMENTEL',
      code: '12',
      parentId: '3885'
    },
    {
      id: '3903',
      name: 'POMALCA',
      code: '18',
      parentId: '3885'
    },
    {
      id: '3904',
      name: 'PUCALÁ',
      code: '19',
      parentId: '3885'
    },
    {
      id: '3898',
      name: 'REQUE',
      code: '13',
      parentId: '3885'
    },
    {
      id: '3899',
      name: 'SANTA ROSA',
      code: '14',
      parentId: '3885'
    },
    {
      id: '3900',
      name: 'SAQA',
      code: '15',
      parentId: '3885'
    },
    {
      id: '3905',
      name: 'TUMÁN',
      code: '20',
      parentId: '3885'
    }
  ],
  3906: [
    {
      id: '3908',
      name: 'CAQARIS',
      code: '02',
      parentId: '3906'
    },
    {
      id: '3907',
      name: 'FERREQAFE',
      code: '01',
      parentId: '3906'
    },
    {
      id: '3909',
      name: 'INCAHUASI',
      code: '03',
      parentId: '3906'
    },
    {
      id: '3910',
      name: 'MANUEL ANTONIO MESONES MURO',
      code: '04',
      parentId: '3906'
    },
    {
      id: '3911',
      name: 'PITIPO',
      code: '05',
      parentId: '3906'
    },
    {
      id: '3912',
      name: 'PUEBLO NUEVO',
      code: '06',
      parentId: '3906'
    }
  ],
  3913: [
    {
      id: '3915',
      name: 'CHOCHOPE',
      code: '02',
      parentId: '3913'
    },
    {
      id: '3916',
      name: 'ILLIMO',
      code: '03',
      parentId: '3913'
    },
    {
      id: '3917',
      name: 'JAYANCA',
      code: '04',
      parentId: '3913'
    },
    {
      id: '3914',
      name: 'LAMBAYEQUE',
      code: '01',
      parentId: '3913'
    },
    {
      id: '3918',
      name: 'MOCHUMI',
      code: '05',
      parentId: '3913'
    },
    {
      id: '3919',
      name: 'MORROPE',
      code: '06',
      parentId: '3913'
    },
    {
      id: '3920',
      name: 'MOTUPE',
      code: '07',
      parentId: '3913'
    },
    {
      id: '3921',
      name: 'OLMOS',
      code: '08',
      parentId: '3913'
    },
    {
      id: '3922',
      name: 'PACORA',
      code: '09',
      parentId: '3913'
    },
    {
      id: '3923',
      name: 'SALAS',
      code: '10',
      parentId: '3913'
    },
    {
      id: '3924',
      name: 'SAN JOSE',
      code: '11',
      parentId: '3913'
    },
    {
      id: '3925',
      name: 'TUCUME',
      code: '12',
      parentId: '3913'
    }
  ],
  3927: [
    {
      id: '3929',
      name: 'ANCON',
      code: '02',
      parentId: '3927'
    },
    {
      id: '3930',
      name: 'ATE',
      code: '03',
      parentId: '3927'
    },
    {
      id: '3931',
      name: 'BARRANCO',
      code: '04',
      parentId: '3927'
    },
    {
      id: '3932',
      name: 'BREÑA',
      code: '05',
      parentId: '3927'
    },
    {
      id: '3933',
      name: 'CARABAYLLO',
      code: '06',
      parentId: '3927'
    },
    {
      id: '3928',
      name: 'CERCADO DE LIMA',
      code: '01',
      parentId: '3927'
    },
    {
      id: '3934',
      name: 'CHACLACAYO',
      code: '07',
      parentId: '3927'
    },
    {
      id: '3935',
      name: 'CHORRILLOS',
      code: '08',
      parentId: '3927'
    },
    {
      id: '3936',
      name: 'CIENEGUILLA',
      code: '09',
      parentId: '3927'
    },
    {
      id: '3937',
      name: 'COMAS',
      code: '10',
      parentId: '3927'
    },
    {
      id: '3938',
      name: 'EL AGUSTINO',
      code: '11',
      parentId: '3927'
    },
    {
      id: '3939',
      name: 'INDEPENDENCIA',
      code: '12',
      parentId: '3927'
    },
    {
      id: '3940',
      name: 'JESUS MARIA',
      code: '13',
      parentId: '3927'
    },
    {
      id: '3941',
      name: 'LA MOLINA',
      code: '14',
      parentId: '3927'
    },
    {
      id: '3942',
      name: 'LA VICTORIA',
      code: '15',
      parentId: '3927'
    },
    {
      id: '3943',
      name: 'LINCE',
      code: '16',
      parentId: '3927'
    },
    {
      id: '3944',
      name: 'LOS OLIVOS',
      code: '17',
      parentId: '3927'
    },
    {
      id: '3945',
      name: 'LURIGANCHO',
      code: '18',
      parentId: '3927'
    },
    {
      id: '3946',
      name: 'LURIN',
      code: '19',
      parentId: '3927'
    },
    {
      id: '3947',
      name: 'MAGDALENA DEL MAR',
      code: '20',
      parentId: '3927'
    },
    {
      id: '3949',
      name: 'MIRAFLORES',
      code: '22',
      parentId: '3927'
    },
    {
      id: '3950',
      name: 'PACHACAMAC',
      code: '23',
      parentId: '3927'
    },
    {
      id: '3951',
      name: 'PUCUSANA',
      code: '24',
      parentId: '3927'
    },
    {
      id: '3948',
      name: 'PUEBLO LIBRE',
      code: '21',
      parentId: '3927'
    },
    {
      id: '3952',
      name: 'PUENTE PIEDRA',
      code: '25',
      parentId: '3927'
    },
    {
      id: '3953',
      name: 'PUNTA HERMOSA',
      code: '26',
      parentId: '3927'
    },
    {
      id: '3954',
      name: 'PUNTA NEGRA',
      code: '27',
      parentId: '3927'
    },
    {
      id: '3955',
      name: 'RIMAC',
      code: '28',
      parentId: '3927'
    },
    {
      id: '3956',
      name: 'SAN BARTOLO',
      code: '29',
      parentId: '3927'
    },
    {
      id: '3957',
      name: 'SAN BORJA',
      code: '30',
      parentId: '3927'
    },
    {
      id: '3958',
      name: 'SAN ISIDRO',
      code: '31',
      parentId: '3927'
    },
    {
      id: '3959',
      name: 'SAN JUAN DE LURIGANCHO',
      code: '32',
      parentId: '3927'
    },
    {
      id: '3960',
      name: 'SAN JUAN DE MIRAFLORES',
      code: '33',
      parentId: '3927'
    },
    {
      id: '3961',
      name: 'SAN LUIS',
      code: '34',
      parentId: '3927'
    },
    {
      id: '3962',
      name: 'SAN MARTIN DE PORRES',
      code: '35',
      parentId: '3927'
    },
    {
      id: '3963',
      name: 'SAN MIGUEL',
      code: '36',
      parentId: '3927'
    },
    {
      id: '3964',
      name: 'SANTA ANITA',
      code: '37',
      parentId: '3927'
    },
    {
      id: '3965',
      name: 'SANTA MARIA DEL MAR',
      code: '38',
      parentId: '3927'
    },
    {
      id: '3966',
      name: 'SANTA ROSA',
      code: '39',
      parentId: '3927'
    },
    {
      id: '3967',
      name: 'SANTIAGO DE SURCO',
      code: '40',
      parentId: '3927'
    },
    {
      id: '3968',
      name: 'SURQUILLO',
      code: '41',
      parentId: '3927'
    },
    {
      id: '3969',
      name: 'VILLA EL SALVADOR',
      code: '42',
      parentId: '3927'
    },
    {
      id: '3970',
      name: 'VILLA MARIA DEL TRIUNFO',
      code: '43',
      parentId: '3927'
    }
  ],
  3971: [
    {
      id: '3972',
      name: 'BARRANCA',
      code: '01',
      parentId: '3971'
    },
    {
      id: '3973',
      name: 'PARAMONGA',
      code: '02',
      parentId: '3971'
    },
    {
      id: '3974',
      name: 'PATIVILCA',
      code: '03',
      parentId: '3971'
    },
    {
      id: '3975',
      name: 'SUPE',
      code: '04',
      parentId: '3971'
    },
    {
      id: '3976',
      name: 'SUPE PUERTO',
      code: '05',
      parentId: '3971'
    }
  ],
  3977: [
    {
      id: '3978',
      name: 'CAJATAMBO',
      code: '01',
      parentId: '3977'
    },
    {
      id: '3979',
      name: 'COPA',
      code: '02',
      parentId: '3977'
    },
    {
      id: '3980',
      name: 'GORGOR',
      code: '03',
      parentId: '3977'
    },
    {
      id: '3981',
      name: 'HUANCAPON',
      code: '04',
      parentId: '3977'
    },
    {
      id: '3982',
      name: 'MANAS',
      code: '05',
      parentId: '3977'
    }
  ],
  3983: [
    {
      id: '3985',
      name: 'ARAHUAY',
      code: '02',
      parentId: '3983'
    },
    {
      id: '3984',
      name: 'CANTA',
      code: '01',
      parentId: '3983'
    },
    {
      id: '3986',
      name: 'HUAMANTANGA',
      code: '03',
      parentId: '3983'
    },
    {
      id: '3987',
      name: 'HUAROS',
      code: '04',
      parentId: '3983'
    },
    {
      id: '3988',
      name: 'LACHAQUI',
      code: '05',
      parentId: '3983'
    },
    {
      id: '3989',
      name: 'SAN BUENAVENTURA',
      code: '06',
      parentId: '3983'
    },
    {
      id: '3990',
      name: 'SANTA ROSA DE QUIVES',
      code: '07',
      parentId: '3983'
    }
  ],
  3991: [
    {
      id: '3993',
      name: 'ASIA',
      code: '02',
      parentId: '3991'
    },
    {
      id: '3994',
      name: 'CALANGO',
      code: '03',
      parentId: '3991'
    },
    {
      id: '3995',
      name: 'CERRO AZUL',
      code: '04',
      parentId: '3991'
    },
    {
      id: '3996',
      name: 'CHILCA',
      code: '05',
      parentId: '3991'
    },
    {
      id: '3997',
      name: 'COAYLLO',
      code: '06',
      parentId: '3991'
    },
    {
      id: '3998',
      name: 'IMPERIAL',
      code: '07',
      parentId: '3991'
    },
    {
      id: '3999',
      name: 'LUNAHUANA',
      code: '08',
      parentId: '3991'
    },
    {
      id: '4000',
      name: 'MALA',
      code: '09',
      parentId: '3991'
    },
    {
      id: '4001',
      name: 'NUEVO IMPERIAL',
      code: '10',
      parentId: '3991'
    },
    {
      id: '4002',
      name: 'PACARAN',
      code: '11',
      parentId: '3991'
    },
    {
      id: '4003',
      name: 'QUILMANA',
      code: '12',
      parentId: '3991'
    },
    {
      id: '4004',
      name: 'SAN ANTONIO',
      code: '13',
      parentId: '3991'
    },
    {
      id: '4005',
      name: 'SAN LUIS',
      code: '14',
      parentId: '3991'
    },
    {
      id: '3992',
      name: 'SAN VICENTE DE CAÑETE',
      code: '01',
      parentId: '3991'
    },
    {
      id: '4006',
      name: 'SANTA CRUZ DE FLORES',
      code: '15',
      parentId: '3991'
    },
    {
      id: '4007',
      name: 'ZUQIGA',
      code: '16',
      parentId: '3991'
    }
  ],
  4008: [
    {
      id: '4010',
      name: 'ATAVILLOS ALTO',
      code: '02',
      parentId: '4008'
    },
    {
      id: '4011',
      name: 'ATAVILLOS BAJO',
      code: '03',
      parentId: '4008'
    },
    {
      id: '4012',
      name: 'AUCALLAMA',
      code: '04',
      parentId: '4008'
    },
    {
      id: '4013',
      name: 'CHANCAY',
      code: '05',
      parentId: '4008'
    },
    {
      id: '4009',
      name: 'HUARAL',
      code: '01',
      parentId: '4008'
    },
    {
      id: '4014',
      name: 'IHUARI',
      code: '06',
      parentId: '4008'
    },
    {
      id: '4015',
      name: 'LAMPIAN',
      code: '07',
      parentId: '4008'
    },
    {
      id: '4016',
      name: 'PACARAOS',
      code: '08',
      parentId: '4008'
    },
    {
      id: '4017',
      name: 'SAN MIGUEL DE ACOS',
      code: '09',
      parentId: '4008'
    },
    {
      id: '4018',
      name: 'SANTA CRUZ DE ANDAMARCA',
      code: '10',
      parentId: '4008'
    },
    {
      id: '4019',
      name: 'SUMBILCA',
      code: '11',
      parentId: '4008'
    },
    {
      id: '4020',
      name: 'VEINTISIETE DE NOVIEMBRE',
      code: '12',
      parentId: '4008'
    }
  ],
  4021: [
    {
      id: '4023',
      name: 'ANTIOQUIA',
      code: '02',
      parentId: '4021'
    },
    {
      id: '4024',
      name: 'CALLAHUANCA',
      code: '03',
      parentId: '4021'
    },
    {
      id: '4025',
      name: 'CARAMPOMA',
      code: '04',
      parentId: '4021'
    },
    {
      id: '4026',
      name: 'CHICLA',
      code: '05',
      parentId: '4021'
    },
    {
      id: '4027',
      name: 'CUENCA',
      code: '06',
      parentId: '4021'
    },
    {
      id: '4028',
      name: 'HUACHUPAMPA',
      code: '07',
      parentId: '4021'
    },
    {
      id: '4029',
      name: 'HUANZA',
      code: '08',
      parentId: '4021'
    },
    {
      id: '4030',
      name: 'HUAROCHIRI',
      code: '09',
      parentId: '4021'
    },
    {
      id: '4031',
      name: 'LAHUAYTAMBO',
      code: '10',
      parentId: '4021'
    },
    {
      id: '4032',
      name: 'LANGA',
      code: '11',
      parentId: '4021'
    },
    {
      id: '4033',
      name: 'LARAOS',
      code: '12',
      parentId: '4021'
    },
    {
      id: '4034',
      name: 'MARIATANA',
      code: '13',
      parentId: '4021'
    },
    {
      id: '4022',
      name: 'MATUCANA',
      code: '01',
      parentId: '4021'
    },
    {
      id: '4035',
      name: 'RICARDO PALMA',
      code: '14',
      parentId: '4021'
    },
    {
      id: '4036',
      name: 'SAN ANDRES DE TUPICOCHA',
      code: '15',
      parentId: '4021'
    },
    {
      id: '4037',
      name: 'SAN ANTONIO',
      code: '16',
      parentId: '4021'
    },
    {
      id: '4038',
      name: 'SAN BARTOLOME',
      code: '17',
      parentId: '4021'
    },
    {
      id: '4039',
      name: 'SAN DAMIAN',
      code: '18',
      parentId: '4021'
    },
    {
      id: '4040',
      name: 'SAN JUAN DE IRIS',
      code: '19',
      parentId: '4021'
    },
    {
      id: '4041',
      name: 'SAN JUAN DE TANTARANCHE',
      code: '20',
      parentId: '4021'
    },
    {
      id: '4042',
      name: 'SAN LORENZO DE QUINTI',
      code: '21',
      parentId: '4021'
    },
    {
      id: '4043',
      name: 'SAN MATEO',
      code: '22',
      parentId: '4021'
    },
    {
      id: '4044',
      name: 'SAN MATEO DE OTAO',
      code: '23',
      parentId: '4021'
    },
    {
      id: '4045',
      name: 'SAN PEDRO DE CASTA',
      code: '24',
      parentId: '4021'
    },
    {
      id: '4046',
      name: 'SAN PEDRO DE HUANCAYRE',
      code: '25',
      parentId: '4021'
    },
    {
      id: '4047',
      name: 'SANGALLAYA',
      code: '26',
      parentId: '4021'
    },
    {
      id: '4048',
      name: 'SANTA CRUZ DE COCACHACRA',
      code: '27',
      parentId: '4021'
    },
    {
      id: '4049',
      name: 'SANTA EULALIA',
      code: '28',
      parentId: '4021'
    },
    {
      id: '4050',
      name: 'SANTIAGO DE ANCHUCAYA',
      code: '29',
      parentId: '4021'
    },
    {
      id: '4051',
      name: 'SANTIAGO DE TUNA',
      code: '30',
      parentId: '4021'
    },
    {
      id: '4052',
      name: 'SANTO DOMINGO DE LOS OLLEROS',
      code: '31',
      parentId: '4021'
    },
    {
      id: '4053',
      name: 'SURCO',
      code: '32',
      parentId: '4021'
    }
  ],
  4054: [
    {
      id: '4056',
      name: 'AMBAR',
      code: '02',
      parentId: '4054'
    },
    {
      id: '4057',
      name: 'CALETA DE CARQUIN',
      code: '03',
      parentId: '4054'
    },
    {
      id: '4058',
      name: 'CHECRAS',
      code: '04',
      parentId: '4054'
    },
    {
      id: '4055',
      name: 'HUACHO',
      code: '01',
      parentId: '4054'
    },
    {
      id: '4059',
      name: 'HUALMAY',
      code: '05',
      parentId: '4054'
    },
    {
      id: '4060',
      name: 'HUAURA',
      code: '06',
      parentId: '4054'
    },
    {
      id: '4061',
      name: 'LEONCIO PRADO',
      code: '07',
      parentId: '4054'
    },
    {
      id: '4062',
      name: 'PACCHO',
      code: '08',
      parentId: '4054'
    },
    {
      id: '4063',
      name: 'SANTA LEONOR',
      code: '09',
      parentId: '4054'
    },
    {
      id: '4064',
      name: 'SANTA MARIA',
      code: '10',
      parentId: '4054'
    },
    {
      id: '4065',
      name: 'SAYAN',
      code: '11',
      parentId: '4054'
    },
    {
      id: '4066',
      name: 'VEGUETA',
      code: '12',
      parentId: '4054'
    }
  ],
  4067: [
    {
      id: '4069',
      name: 'ANDAJES',
      code: '02',
      parentId: '4067'
    },
    {
      id: '4070',
      name: 'CAUJUL',
      code: '03',
      parentId: '4067'
    },
    {
      id: '4071',
      name: 'COCHAMARCA',
      code: '04',
      parentId: '4067'
    },
    {
      id: '4072',
      name: 'NAVAN',
      code: '05',
      parentId: '4067'
    },
    {
      id: '4068',
      name: 'OYON',
      code: '01',
      parentId: '4067'
    },
    {
      id: '4073',
      name: 'PACHANGARA',
      code: '06',
      parentId: '4067'
    }
  ],
  4074: [
    {
      id: '4076',
      name: 'ALIS',
      code: '02',
      parentId: '4074'
    },
    {
      id: '4077',
      name: 'AYAUCA',
      code: '03',
      parentId: '4074'
    },
    {
      id: '4078',
      name: 'AYAVIRI',
      code: '04',
      parentId: '4074'
    },
    {
      id: '4079',
      name: 'AZANGARO',
      code: '05',
      parentId: '4074'
    },
    {
      id: '4080',
      name: 'CACRA',
      code: '06',
      parentId: '4074'
    },
    {
      id: '4081',
      name: 'CARANIA',
      code: '07',
      parentId: '4074'
    },
    {
      id: '4082',
      name: 'CATAHUASI',
      code: '08',
      parentId: '4074'
    },
    {
      id: '4083',
      name: 'CHOCOS',
      code: '09',
      parentId: '4074'
    },
    {
      id: '4084',
      name: 'COCHAS',
      code: '10',
      parentId: '4074'
    },
    {
      id: '4085',
      name: 'COLONIA',
      code: '11',
      parentId: '4074'
    },
    {
      id: '4086',
      name: 'HONGOS',
      code: '12',
      parentId: '4074'
    },
    {
      id: '4087',
      name: 'HUAMPARA',
      code: '13',
      parentId: '4074'
    },
    {
      id: '4088',
      name: 'HUANCAYA',
      code: '14',
      parentId: '4074'
    },
    {
      id: '4089',
      name: 'HUANGASCAR',
      code: '15',
      parentId: '4074'
    },
    {
      id: '4090',
      name: 'HUANTAN',
      code: '16',
      parentId: '4074'
    },
    {
      id: '4091',
      name: 'HUAQEC',
      code: '17',
      parentId: '4074'
    },
    {
      id: '4092',
      name: 'LARAOS',
      code: '18',
      parentId: '4074'
    },
    {
      id: '4093',
      name: 'LINCHA',
      code: '19',
      parentId: '4074'
    },
    {
      id: '4094',
      name: 'MADEAN',
      code: '20',
      parentId: '4074'
    },
    {
      id: '4095',
      name: 'MIRAFLORES',
      code: '21',
      parentId: '4074'
    },
    {
      id: '4096',
      name: 'OMAS',
      code: '22',
      parentId: '4074'
    },
    {
      id: '4097',
      name: 'PUTINZA',
      code: '23',
      parentId: '4074'
    },
    {
      id: '4098',
      name: 'QUINCHES',
      code: '24',
      parentId: '4074'
    },
    {
      id: '4099',
      name: 'QUINOCAY',
      code: '25',
      parentId: '4074'
    },
    {
      id: '4100',
      name: 'SAN JOAQUIN',
      code: '26',
      parentId: '4074'
    },
    {
      id: '4101',
      name: 'SAN PEDRO DE PILAS',
      code: '27',
      parentId: '4074'
    },
    {
      id: '4102',
      name: 'TANTA',
      code: '28',
      parentId: '4074'
    },
    {
      id: '4103',
      name: 'TAURIPAMPA',
      code: '29',
      parentId: '4074'
    },
    {
      id: '4104',
      name: 'TOMAS',
      code: '30',
      parentId: '4074'
    },
    {
      id: '4105',
      name: 'TUPE',
      code: '31',
      parentId: '4074'
    },
    {
      id: '4106',
      name: 'VIQAC',
      code: '32',
      parentId: '4074'
    },
    {
      id: '4107',
      name: 'VITIS',
      code: '33',
      parentId: '4074'
    },
    {
      id: '4075',
      name: 'YAUYOS',
      code: '01',
      parentId: '4074'
    }
  ],
  4109: [
    {
      id: '4111',
      name: 'ALTO NANAY',
      code: '02',
      parentId: '4109'
    },
    {
      id: '4121',
      name: 'BELÉN',
      code: '12',
      parentId: '4109'
    },
    {
      id: '4112',
      name: 'FERNANDO LORES',
      code: '03',
      parentId: '4109'
    },
    {
      id: '4113',
      name: 'INDIANA',
      code: '04',
      parentId: '4109'
    },
    {
      id: '4110',
      name: 'IQUITOS',
      code: '01',
      parentId: '4109'
    },
    {
      id: '4114',
      name: 'LAS AMAZONAS',
      code: '05',
      parentId: '4109'
    },
    {
      id: '4115',
      name: 'MAZAN',
      code: '06',
      parentId: '4109'
    },
    {
      id: '4116',
      name: 'NAPO',
      code: '07',
      parentId: '4109'
    },
    {
      id: '4117',
      name: 'PUNCHANA',
      code: '08',
      parentId: '4109'
    },
    {
      id: '4118',
      name: 'PUTUMAYO',
      code: '09',
      parentId: '4109'
    },
    {
      id: '4122',
      name: 'SAN JUAN BAUTISTA',
      code: '13',
      parentId: '4109'
    },
    {
      id: '4119',
      name: 'TORRES CAUSANA',
      code: '10',
      parentId: '4109'
    },
    {
      id: '4120',
      name: 'YAQUERANA',
      code: '11',
      parentId: '4109'
    }
  ],
  4123: [
    {
      id: '4125',
      name: 'BALSAPUERTO',
      code: '02',
      parentId: '4123'
    },
    {
      id: '4126',
      name: 'BARRANCA',
      code: '03',
      parentId: '4123'
    },
    {
      id: '4127',
      name: 'CAHUAPANAS',
      code: '04',
      parentId: '4123'
    },
    {
      id: '4128',
      name: 'JEBEROS',
      code: '05',
      parentId: '4123'
    },
    {
      id: '4129',
      name: 'LAGUNAS',
      code: '06',
      parentId: '4123'
    },
    {
      id: '4130',
      name: 'MANSERICHE',
      code: '07',
      parentId: '4123'
    },
    {
      id: '4131',
      name: 'MORONA',
      code: '08',
      parentId: '4123'
    },
    {
      id: '4132',
      name: 'PASTAZA',
      code: '09',
      parentId: '4123'
    },
    {
      id: '4133',
      name: 'SANTA CRUZ',
      code: '10',
      parentId: '4123'
    },
    {
      id: '4134',
      name: 'TENIENTE CESAR LOPEZ ROJAS',
      code: '11',
      parentId: '4123'
    },
    {
      id: '4124',
      name: 'YURIMAGUAS',
      code: '01',
      parentId: '4123'
    }
  ],
  4135: [
    {
      id: '4136',
      name: 'NAUTA',
      code: '01',
      parentId: '4135'
    },
    {
      id: '4137',
      name: 'PARINARI',
      code: '02',
      parentId: '4135'
    },
    {
      id: '4138',
      name: 'TIGRE',
      code: '03',
      parentId: '4135'
    },
    {
      id: '4139',
      name: 'TROMPETEROS',
      code: '04',
      parentId: '4135'
    },
    {
      id: '4140',
      name: 'URARINAS',
      code: '05',
      parentId: '4135'
    }
  ],
  4141: [
    {
      id: '4143',
      name: 'PEBAS',
      code: '02',
      parentId: '4141'
    },
    {
      id: '4142',
      name: 'RAMON CASTILLA',
      code: '01',
      parentId: '4141'
    },
    {
      id: '4145',
      name: 'SAN PABLO',
      code: '04',
      parentId: '4141'
    },
    {
      id: '4144',
      name: 'YAVARI',
      code: '03',
      parentId: '4141'
    }
  ],
  4146: [
    {
      id: '4148',
      name: 'ALTO TAPICHE',
      code: '02',
      parentId: '4146'
    },
    {
      id: '4149',
      name: 'CAPELO',
      code: '03',
      parentId: '4146'
    },
    {
      id: '4150',
      name: 'EMILIO SAN MARTIN',
      code: '04',
      parentId: '4146'
    },
    {
      id: '4156',
      name: 'JENARO HERRERA',
      code: '10',
      parentId: '4146'
    },
    {
      id: '4151',
      name: 'MAQUIA',
      code: '05',
      parentId: '4146'
    },
    {
      id: '4152',
      name: 'PUINAHUA',
      code: '06',
      parentId: '4146'
    },
    {
      id: '4147',
      name: 'REQUENA',
      code: '01',
      parentId: '4146'
    },
    {
      id: '4153',
      name: 'SAQUENA',
      code: '07',
      parentId: '4146'
    },
    {
      id: '4154',
      name: 'SOPLIN',
      code: '08',
      parentId: '4146'
    },
    {
      id: '4155',
      name: 'TAPICHE',
      code: '09',
      parentId: '4146'
    },
    {
      id: '4157',
      name: 'YAQUERANA',
      code: '11',
      parentId: '4146'
    }
  ],
  4158: [
    {
      id: '4159',
      name: 'CONTAMANA',
      code: '01',
      parentId: '4158'
    },
    {
      id: '4160',
      name: 'INAHUAYA',
      code: '02',
      parentId: '4158'
    },
    {
      id: '4161',
      name: 'PADRE MARQUEZ',
      code: '03',
      parentId: '4158'
    },
    {
      id: '4162',
      name: 'PAMPA HERMOSA',
      code: '04',
      parentId: '4158'
    },
    {
      id: '4163',
      name: 'SARAYACU',
      code: '05',
      parentId: '4158'
    },
    {
      id: '4164',
      name: 'VARGAS GUERRA',
      code: '06',
      parentId: '4158'
    }
  ],
  4166: [
    {
      id: '4168',
      name: 'INAMBARI',
      code: '02',
      parentId: '4166'
    },
    {
      id: '4170',
      name: 'LABERINTO',
      code: '04',
      parentId: '4166'
    },
    {
      id: '4169',
      name: 'LAS PIEDRAS',
      code: '03',
      parentId: '4166'
    },
    {
      id: '4167',
      name: 'TAMBOPATA',
      code: '01',
      parentId: '4166'
    }
  ],
  4171: [
    {
      id: '4173',
      name: 'FITZCARRALD',
      code: '02',
      parentId: '4171'
    },
    {
      id: '4175',
      name: 'HUEPETUHE',
      code: '04',
      parentId: '4171'
    },
    {
      id: '4174',
      name: 'MADRE DE DIOS',
      code: '03',
      parentId: '4171'
    },
    {
      id: '4172',
      name: 'MANU',
      code: '01',
      parentId: '4171'
    }
  ],
  4176: [
    {
      id: '4178',
      name: 'IBERIA',
      code: '02',
      parentId: '4176'
    },
    {
      id: '4177',
      name: 'IQAPARI',
      code: '01',
      parentId: '4176'
    },
    {
      id: '4179',
      name: 'TAHUAMANU',
      code: '03',
      parentId: '4176'
    }
  ],
  4181: [
    {
      id: '4183',
      name: 'CARUMAS',
      code: '02',
      parentId: '4181'
    },
    {
      id: '4184',
      name: 'CUCHUMBAYA',
      code: '03',
      parentId: '4181'
    },
    {
      id: '4182',
      name: 'MOQUEGUA',
      code: '01',
      parentId: '4181'
    },
    {
      id: '4185',
      name: 'SAMEGUA',
      code: '04',
      parentId: '4181'
    },
    {
      id: '4186',
      name: 'SAN CRISTOBAL',
      code: '05',
      parentId: '4181'
    },
    {
      id: '4187',
      name: 'TORATA',
      code: '06',
      parentId: '4181'
    }
  ],
  4188: [
    {
      id: '4190',
      name: 'CHOJATA',
      code: '02',
      parentId: '4188'
    },
    {
      id: '4191',
      name: 'COALAQUE',
      code: '03',
      parentId: '4188'
    },
    {
      id: '4192',
      name: 'ICHUQA',
      code: '04',
      parentId: '4188'
    },
    {
      id: '4193',
      name: 'LA CAPILLA',
      code: '05',
      parentId: '4188'
    },
    {
      id: '4194',
      name: 'LLOQUE',
      code: '06',
      parentId: '4188'
    },
    {
      id: '4195',
      name: 'MATALAQUE',
      code: '07',
      parentId: '4188'
    },
    {
      id: '4189',
      name: 'OMATE',
      code: '01',
      parentId: '4188'
    },
    {
      id: '4196',
      name: 'PUQUINA',
      code: '08',
      parentId: '4188'
    },
    {
      id: '4197',
      name: 'QUINISTAQUILLAS',
      code: '09',
      parentId: '4188'
    },
    {
      id: '4198',
      name: 'UBINAS',
      code: '10',
      parentId: '4188'
    },
    {
      id: '4199',
      name: 'YUNGA',
      code: '11',
      parentId: '4188'
    }
  ],
  4200: [
    {
      id: '4202',
      name: 'EL ALGARROBAL',
      code: '02',
      parentId: '4200'
    },
    {
      id: '4201',
      name: 'ILO',
      code: '01',
      parentId: '4200'
    },
    {
      id: '4203',
      name: 'PACOCHA',
      code: '03',
      parentId: '4200'
    }
  ],
  4205: [
    {
      id: '4206',
      name: 'CHAUPIMARCA',
      code: '01',
      parentId: '4205'
    },
    {
      id: '4207',
      name: 'HUACHON',
      code: '02',
      parentId: '4205'
    },
    {
      id: '4208',
      name: 'HUARIACA',
      code: '03',
      parentId: '4205'
    },
    {
      id: '4209',
      name: 'HUAYLLAY',
      code: '04',
      parentId: '4205'
    },
    {
      id: '4210',
      name: 'NINACACA',
      code: '05',
      parentId: '4205'
    },
    {
      id: '4211',
      name: 'PALLANCHACRA',
      code: '06',
      parentId: '4205'
    },
    {
      id: '4212',
      name: 'PAUCARTAMBO',
      code: '07',
      parentId: '4205'
    },
    {
      id: '4213',
      name: 'SAN FCO.DE ASIS DE YARUSYACAN',
      code: '08',
      parentId: '4205'
    },
    {
      id: '4214',
      name: 'SIMON BOLIVAR',
      code: '09',
      parentId: '4205'
    },
    {
      id: '4215',
      name: 'TICLACAYAN',
      code: '10',
      parentId: '4205'
    },
    {
      id: '4216',
      name: 'TINYAHUARCO',
      code: '11',
      parentId: '4205'
    },
    {
      id: '4217',
      name: 'VICCO',
      code: '12',
      parentId: '4205'
    },
    {
      id: '4218',
      name: 'YANACANCHA',
      code: '13',
      parentId: '4205'
    }
  ],
  4219: [
    {
      id: '4221',
      name: 'CHACAYAN',
      code: '02',
      parentId: '4219'
    },
    {
      id: '4222',
      name: 'GOYLLARISQUIZGA',
      code: '03',
      parentId: '4219'
    },
    {
      id: '4223',
      name: 'PAUCAR',
      code: '04',
      parentId: '4219'
    },
    {
      id: '4224',
      name: 'SAN PEDRO DE PILLAO',
      code: '05',
      parentId: '4219'
    },
    {
      id: '4225',
      name: 'SANTA ANA DE TUSI',
      code: '06',
      parentId: '4219'
    },
    {
      id: '4226',
      name: 'TAPUC',
      code: '07',
      parentId: '4219'
    },
    {
      id: '4227',
      name: 'VILCABAMBA',
      code: '08',
      parentId: '4219'
    },
    {
      id: '4220',
      name: 'YANAHUANCA',
      code: '01',
      parentId: '4219'
    }
  ],
  4228: [
    {
      id: '4230',
      name: 'CHONTABAMBA',
      code: '02',
      parentId: '4228'
    },
    {
      id: '4231',
      name: 'HUANCABAMBA',
      code: '03',
      parentId: '4228'
    },
    {
      id: '4229',
      name: 'OXAPAMPA',
      code: '01',
      parentId: '4228'
    },
    {
      id: '4232',
      name: 'PALCAZU',
      code: '04',
      parentId: '4228'
    },
    {
      id: '4233',
      name: 'POZUZO',
      code: '05',
      parentId: '4228'
    },
    {
      id: '4234',
      name: 'PUERTO BERMUDEZ',
      code: '06',
      parentId: '4228'
    },
    {
      id: '4235',
      name: 'VILLA RICA',
      code: '07',
      parentId: '4228'
    }
  ],
  4237: [
    {
      id: '4239',
      name: 'CASTILLA',
      code: '04',
      parentId: '4237'
    },
    {
      id: '4240',
      name: 'CATACAOS',
      code: '05',
      parentId: '4237'
    },
    {
      id: '4241',
      name: 'CURA MORI',
      code: '07',
      parentId: '4237'
    },
    {
      id: '4242',
      name: 'EL TALLAN',
      code: '08',
      parentId: '4237'
    },
    {
      id: '4243',
      name: 'LA ARENA',
      code: '09',
      parentId: '4237'
    },
    {
      id: '4244',
      name: 'LA UNION',
      code: '10',
      parentId: '4237'
    },
    {
      id: '4245',
      name: 'LAS LOMAS',
      code: '11',
      parentId: '4237'
    },
    {
      id: '4238',
      name: 'PIURA',
      code: '01',
      parentId: '4237'
    },
    {
      id: '4246',
      name: 'TAMBO GRANDE',
      code: '14',
      parentId: '4237'
    }
  ],
  4247: [
    {
      id: '4248',
      name: 'AYABACA',
      code: '01',
      parentId: '4247'
    },
    {
      id: '4249',
      name: 'FRIAS',
      code: '02',
      parentId: '4247'
    },
    {
      id: '4250',
      name: 'JILILI',
      code: '03',
      parentId: '4247'
    },
    {
      id: '4251',
      name: 'LAGUNAS',
      code: '04',
      parentId: '4247'
    },
    {
      id: '4252',
      name: 'MONTERO',
      code: '05',
      parentId: '4247'
    },
    {
      id: '4253',
      name: 'PACAIPAMPA',
      code: '06',
      parentId: '4247'
    },
    {
      id: '4254',
      name: 'PAIMAS',
      code: '07',
      parentId: '4247'
    },
    {
      id: '4255',
      name: 'SAPILLICA',
      code: '08',
      parentId: '4247'
    },
    {
      id: '4256',
      name: 'SICCHEZ',
      code: '09',
      parentId: '4247'
    },
    {
      id: '4257',
      name: 'SUYO',
      code: '10',
      parentId: '4247'
    }
  ],
  4258: [
    {
      id: '4260',
      name: 'CANCHAQUE',
      code: '02',
      parentId: '4258'
    },
    {
      id: '4261',
      name: 'EL CARMEN DE LA FRONTERA',
      code: '03',
      parentId: '4258'
    },
    {
      id: '4259',
      name: 'HUANCABAMBA',
      code: '01',
      parentId: '4258'
    },
    {
      id: '4262',
      name: 'HUARMACA',
      code: '04',
      parentId: '4258'
    },
    {
      id: '4263',
      name: 'LALAQUIZ',
      code: '05',
      parentId: '4258'
    },
    {
      id: '4264',
      name: 'SAN MIGUEL DE EL FAIQUE',
      code: '06',
      parentId: '4258'
    },
    {
      id: '4265',
      name: 'SONDOR',
      code: '07',
      parentId: '4258'
    },
    {
      id: '4266',
      name: 'SONDORILLO',
      code: '08',
      parentId: '4258'
    }
  ],
  4267: [
    {
      id: '4269',
      name: 'BUENOS AIRES',
      code: '02',
      parentId: '4267'
    },
    {
      id: '4270',
      name: 'CHALACO',
      code: '03',
      parentId: '4267'
    },
    {
      id: '4268',
      name: 'CHULUCANAS',
      code: '01',
      parentId: '4267'
    },
    {
      id: '4271',
      name: 'LA MATANZA',
      code: '04',
      parentId: '4267'
    },
    {
      id: '4272',
      name: 'MORROPON',
      code: '05',
      parentId: '4267'
    },
    {
      id: '4273',
      name: 'SALITRAL',
      code: '06',
      parentId: '4267'
    },
    {
      id: '4274',
      name: 'SAN JUAN DE BIGOTE',
      code: '07',
      parentId: '4267'
    },
    {
      id: '4275',
      name: 'SANTA CATALINA DE MOSSA',
      code: '08',
      parentId: '4267'
    },
    {
      id: '4276',
      name: 'SANTO DOMINGO',
      code: '09',
      parentId: '4267'
    },
    {
      id: '4277',
      name: 'YAMANGO',
      code: '10',
      parentId: '4267'
    }
  ],
  4278: [
    {
      id: '4280',
      name: 'AMOTAPE',
      code: '02',
      parentId: '4278'
    },
    {
      id: '4281',
      name: 'ARENAL',
      code: '03',
      parentId: '4278'
    },
    {
      id: '4282',
      name: 'COLAN',
      code: '04',
      parentId: '4278'
    },
    {
      id: '4283',
      name: 'LA HUACA',
      code: '05',
      parentId: '4278'
    },
    {
      id: '4279',
      name: 'PAITA',
      code: '01',
      parentId: '4278'
    },
    {
      id: '4284',
      name: 'TAMARINDO',
      code: '06',
      parentId: '4278'
    },
    {
      id: '4285',
      name: 'VICHAYAL',
      code: '07',
      parentId: '4278'
    }
  ],
  4286: [
    {
      id: '4288',
      name: 'BELLAVISTA',
      code: '02',
      parentId: '4286'
    },
    {
      id: '4289',
      name: 'IGNACIO ESCUDERO',
      code: '03',
      parentId: '4286'
    },
    {
      id: '4290',
      name: 'LANCONES',
      code: '04',
      parentId: '4286'
    },
    {
      id: '4291',
      name: 'MARCAVELICA',
      code: '05',
      parentId: '4286'
    },
    {
      id: '4292',
      name: 'MIGUEL CHECA',
      code: '06',
      parentId: '4286'
    },
    {
      id: '4293',
      name: 'QUERECOTILLO',
      code: '07',
      parentId: '4286'
    },
    {
      id: '4294',
      name: 'SALITRAL',
      code: '08',
      parentId: '4286'
    },
    {
      id: '4287',
      name: 'SULLANA',
      code: '01',
      parentId: '4286'
    }
  ],
  4295: [
    {
      id: '4297',
      name: 'EL ALTO',
      code: '02',
      parentId: '4295'
    },
    {
      id: '4298',
      name: 'LA BREA',
      code: '03',
      parentId: '4295'
    },
    {
      id: '4299',
      name: 'LOBITOS',
      code: '04',
      parentId: '4295'
    },
    {
      id: '4300',
      name: 'LOS ORGANOS',
      code: '05',
      parentId: '4295'
    },
    {
      id: '4301',
      name: 'MANCORA',
      code: '06',
      parentId: '4295'
    },
    {
      id: '4296',
      name: 'PARIQAS',
      code: '01',
      parentId: '4295'
    }
  ],
  4302: [
    {
      id: '4304',
      name: 'BELLAVISTA DE LA UNION',
      code: '02',
      parentId: '4302'
    },
    {
      id: '4305',
      name: 'BERNAL',
      code: '03',
      parentId: '4302'
    },
    {
      id: '4306',
      name: 'CRISTO NOS VALGA',
      code: '04',
      parentId: '4302'
    },
    {
      id: '4308',
      name: 'RINCONADA LLICUAR',
      code: '06',
      parentId: '4302'
    },
    {
      id: '4303',
      name: 'SECHURA',
      code: '01',
      parentId: '4302'
    },
    {
      id: '4307',
      name: 'VICE',
      code: '05',
      parentId: '4302'
    }
  ],
  4310: [
    {
      id: '4312',
      name: 'ACORA',
      code: '02',
      parentId: '4310'
    },
    {
      id: '4313',
      name: 'AMANTANI',
      code: '03',
      parentId: '4310'
    },
    {
      id: '4314',
      name: 'ATUNCOLLA',
      code: '04',
      parentId: '4310'
    },
    {
      id: '4315',
      name: 'CAPACHICA',
      code: '05',
      parentId: '4310'
    },
    {
      id: '4316',
      name: 'CHUCUITO',
      code: '06',
      parentId: '4310'
    },
    {
      id: '4317',
      name: 'COATA',
      code: '07',
      parentId: '4310'
    },
    {
      id: '4318',
      name: 'HUATA',
      code: '08',
      parentId: '4310'
    },
    {
      id: '4319',
      name: 'MAQAZO',
      code: '09',
      parentId: '4310'
    },
    {
      id: '4320',
      name: 'PAUCARCOLLA',
      code: '10',
      parentId: '4310'
    },
    {
      id: '4321',
      name: 'PICHACANI',
      code: '11',
      parentId: '4310'
    },
    {
      id: '4322',
      name: 'PLATERIA',
      code: '12',
      parentId: '4310'
    },
    {
      id: '4311',
      name: 'PUNO',
      code: '01',
      parentId: '4310'
    },
    {
      id: '4323',
      name: 'SAN ANTONIO',
      code: '13',
      parentId: '4310'
    },
    {
      id: '4324',
      name: 'TIQUILLACA',
      code: '14',
      parentId: '4310'
    },
    {
      id: '4325',
      name: 'VILQUE',
      code: '15',
      parentId: '4310'
    }
  ],
  4326: [
    {
      id: '4328',
      name: 'ACHAYA',
      code: '02',
      parentId: '4326'
    },
    {
      id: '4329',
      name: 'ARAPA',
      code: '03',
      parentId: '4326'
    },
    {
      id: '4330',
      name: 'ASILLO',
      code: '04',
      parentId: '4326'
    },
    {
      id: '4327',
      name: 'AZANGARO',
      code: '01',
      parentId: '4326'
    },
    {
      id: '4331',
      name: 'CAMINACA',
      code: '05',
      parentId: '4326'
    },
    {
      id: '4332',
      name: 'CHUPA',
      code: '06',
      parentId: '4326'
    },
    {
      id: '4333',
      name: 'JOSE DOMINGO CHOQUEHUANCA',
      code: '07',
      parentId: '4326'
    },
    {
      id: '4334',
      name: 'MUQANI',
      code: '08',
      parentId: '4326'
    },
    {
      id: '4335',
      name: 'POTONI',
      code: '09',
      parentId: '4326'
    },
    {
      id: '4336',
      name: 'SAMAN',
      code: '10',
      parentId: '4326'
    },
    {
      id: '4337',
      name: 'SAN ANTON',
      code: '11',
      parentId: '4326'
    },
    {
      id: '4338',
      name: 'SAN JOSE',
      code: '12',
      parentId: '4326'
    },
    {
      id: '4339',
      name: 'SAN JUAN DE SALINAS',
      code: '13',
      parentId: '4326'
    },
    {
      id: '4340',
      name: 'SANTIAGO DE PUPUJA',
      code: '14',
      parentId: '4326'
    },
    {
      id: '4341',
      name: 'TIRAPATA',
      code: '15',
      parentId: '4326'
    }
  ],
  4342: [
    {
      id: '4344',
      name: 'AJOYANI',
      code: '02',
      parentId: '4342'
    },
    {
      id: '4345',
      name: 'AYAPATA',
      code: '03',
      parentId: '4342'
    },
    {
      id: '4346',
      name: 'COASA',
      code: '04',
      parentId: '4342'
    },
    {
      id: '4347',
      name: 'CORANI',
      code: '05',
      parentId: '4342'
    },
    {
      id: '4348',
      name: 'CRUCERO',
      code: '06',
      parentId: '4342'
    },
    {
      id: '4349',
      name: 'ITUATA',
      code: '07',
      parentId: '4342'
    },
    {
      id: '4343',
      name: 'MACUSANI',
      code: '01',
      parentId: '4342'
    },
    {
      id: '4350',
      name: 'OLLACHEA',
      code: '08',
      parentId: '4342'
    },
    {
      id: '4351',
      name: 'SAN GABAN',
      code: '09',
      parentId: '4342'
    },
    {
      id: '4352',
      name: 'USICAYOS',
      code: '10',
      parentId: '4342'
    }
  ],
  4353: [
    {
      id: '4355',
      name: 'DESAGUADERO',
      code: '02',
      parentId: '4353'
    },
    {
      id: '4356',
      name: 'HUACULLANI',
      code: '03',
      parentId: '4353'
    },
    {
      id: '4354',
      name: 'JULI',
      code: '01',
      parentId: '4353'
    },
    {
      id: '4357',
      name: 'KELLUYO',
      code: '04',
      parentId: '4353'
    },
    {
      id: '4358',
      name: 'PISACOMA',
      code: '05',
      parentId: '4353'
    },
    {
      id: '4359',
      name: 'POMATA',
      code: '06',
      parentId: '4353'
    },
    {
      id: '4360',
      name: 'ZEPITA',
      code: '07',
      parentId: '4353'
    }
  ],
  4361: [
    {
      id: '4363',
      name: 'CAPAZO',
      code: '02',
      parentId: '4361'
    },
    {
      id: '4366',
      name: 'CONDURIRI',
      code: '05',
      parentId: '4361'
    },
    {
      id: '4362',
      name: 'ILAVE',
      code: '01',
      parentId: '4361'
    },
    {
      id: '4364',
      name: 'PILCUYO',
      code: '03',
      parentId: '4361'
    },
    {
      id: '4365',
      name: 'SANTA ROSA',
      code: '04',
      parentId: '4361'
    }
  ],
  4367: [
    {
      id: '4369',
      name: 'COJATA',
      code: '02',
      parentId: '4367'
    },
    {
      id: '4368',
      name: 'HUANCANE',
      code: '01',
      parentId: '4367'
    },
    {
      id: '4370',
      name: 'HUATASANI',
      code: '03',
      parentId: '4367'
    },
    {
      id: '4371',
      name: 'INCHUPALLA',
      code: '04',
      parentId: '4367'
    },
    {
      id: '4372',
      name: 'PUSI',
      code: '05',
      parentId: '4367'
    },
    {
      id: '4373',
      name: 'ROSASPATA',
      code: '06',
      parentId: '4367'
    },
    {
      id: '4374',
      name: 'TARACO',
      code: '07',
      parentId: '4367'
    },
    {
      id: '4375',
      name: 'VILQUE CHICO',
      code: '08',
      parentId: '4367'
    }
  ],
  4376: [
    {
      id: '4378',
      name: 'CABANILLA',
      code: '02',
      parentId: '4376'
    },
    {
      id: '4379',
      name: 'CALAPUJA',
      code: '03',
      parentId: '4376'
    },
    {
      id: '4377',
      name: 'LAMPA',
      code: '01',
      parentId: '4376'
    },
    {
      id: '4380',
      name: 'NICASIO',
      code: '04',
      parentId: '4376'
    },
    {
      id: '4381',
      name: 'OCUVIRI',
      code: '05',
      parentId: '4376'
    },
    {
      id: '4382',
      name: 'PALCA',
      code: '06',
      parentId: '4376'
    },
    {
      id: '4383',
      name: 'PARATIA',
      code: '07',
      parentId: '4376'
    },
    {
      id: '4384',
      name: 'PUCARA',
      code: '08',
      parentId: '4376'
    },
    {
      id: '4385',
      name: 'SANTA LUCIA',
      code: '09',
      parentId: '4376'
    },
    {
      id: '4386',
      name: 'VILAVILA',
      code: '10',
      parentId: '4376'
    }
  ],
  4387: [
    {
      id: '4389',
      name: 'ANTAUTA',
      code: '02',
      parentId: '4387'
    },
    {
      id: '4388',
      name: 'AYAVIRI',
      code: '01',
      parentId: '4387'
    },
    {
      id: '4390',
      name: 'CUPI',
      code: '03',
      parentId: '4387'
    },
    {
      id: '4391',
      name: 'LLALLI',
      code: '04',
      parentId: '4387'
    },
    {
      id: '4392',
      name: 'MACARI',
      code: '05',
      parentId: '4387'
    },
    {
      id: '4393',
      name: 'NUQOA',
      code: '06',
      parentId: '4387'
    },
    {
      id: '4394',
      name: 'ORURILLO',
      code: '07',
      parentId: '4387'
    },
    {
      id: '4395',
      name: 'SANTA ROSA',
      code: '08',
      parentId: '4387'
    },
    {
      id: '4396',
      name: 'UMACHIRI',
      code: '09',
      parentId: '4387'
    }
  ],
  4397: [
    {
      id: '4399',
      name: 'CONIMA',
      code: '02',
      parentId: '4397'
    },
    {
      id: '4400',
      name: 'HUAYRAPATA',
      code: '03',
      parentId: '4397'
    },
    {
      id: '4398',
      name: 'MOHO',
      code: '01',
      parentId: '4397'
    },
    {
      id: '4401',
      name: 'TILALI',
      code: '04',
      parentId: '4397'
    }
  ],
  4402: [
    {
      id: '4404',
      name: 'ANANEA',
      code: '02',
      parentId: '4402'
    },
    {
      id: '4405',
      name: 'PEDRO VILCA APAZA',
      code: '03',
      parentId: '4402'
    },
    {
      id: '4403',
      name: 'PUTINA',
      code: '01',
      parentId: '4402'
    },
    {
      id: '4406',
      name: 'QUILCAPUNCU',
      code: '04',
      parentId: '4402'
    },
    {
      id: '4407',
      name: 'SINA',
      code: '05',
      parentId: '4402'
    }
  ],
  4408: [
    {
      id: '4410',
      name: 'CABANA',
      code: '02',
      parentId: '4408'
    },
    {
      id: '4411',
      name: 'CABANILLAS',
      code: '03',
      parentId: '4408'
    },
    {
      id: '4412',
      name: 'CARACOTO',
      code: '04',
      parentId: '4408'
    },
    {
      id: '4409',
      name: 'JULIACA',
      code: '01',
      parentId: '4408'
    }
  ],
  4413: [
    {
      id: '4422',
      name: 'ALTO INAMBARI',
      code: '09',
      parentId: '4413'
    },
    {
      id: '4415',
      name: 'CUYOCUYO',
      code: '02',
      parentId: '4413'
    },
    {
      id: '4416',
      name: 'LIMBANI',
      code: '03',
      parentId: '4413'
    },
    {
      id: '4417',
      name: 'PATAMBUCO',
      code: '04',
      parentId: '4413'
    },
    {
      id: '4418',
      name: 'PHARA',
      code: '05',
      parentId: '4413'
    },
    {
      id: '4419',
      name: 'QUIACA',
      code: '06',
      parentId: '4413'
    },
    {
      id: '4420',
      name: 'SAN JUAN DEL ORO',
      code: '07',
      parentId: '4413'
    },
    {
      id: '4414',
      name: 'SANDIA',
      code: '01',
      parentId: '4413'
    },
    {
      id: '4421',
      name: 'YANAHUAYA',
      code: '08',
      parentId: '4413'
    }
  ],
  4423: [
    {
      id: '4425',
      name: 'ANAPIA',
      code: '02',
      parentId: '4423'
    },
    {
      id: '4426',
      name: 'COPANI',
      code: '03',
      parentId: '4423'
    },
    {
      id: '4427',
      name: 'CUTURAPI',
      code: '04',
      parentId: '4423'
    },
    {
      id: '4428',
      name: 'OLLARAYA',
      code: '05',
      parentId: '4423'
    },
    {
      id: '4429',
      name: 'TINICACHI',
      code: '06',
      parentId: '4423'
    },
    {
      id: '4430',
      name: 'UNICACHI',
      code: '07',
      parentId: '4423'
    },
    {
      id: '4424',
      name: 'YUNGUYO',
      code: '01',
      parentId: '4423'
    }
  ],
  4432: [
    {
      id: '4434',
      name: 'CALZADA',
      code: '02',
      parentId: '4432'
    },
    {
      id: '4435',
      name: 'HABANA',
      code: '03',
      parentId: '4432'
    },
    {
      id: '4436',
      name: 'JEPELACIO',
      code: '04',
      parentId: '4432'
    },
    {
      id: '4433',
      name: 'MOYOBAMBA',
      code: '01',
      parentId: '4432'
    },
    {
      id: '4437',
      name: 'SORITOR',
      code: '05',
      parentId: '4432'
    },
    {
      id: '4438',
      name: 'YANTALO',
      code: '06',
      parentId: '4432'
    }
  ],
  4439: [
    {
      id: '4441',
      name: 'ALTO BIAVO',
      code: '02',
      parentId: '4439'
    },
    {
      id: '4442',
      name: 'BAJO BIAVO',
      code: '03',
      parentId: '4439'
    },
    {
      id: '4440',
      name: 'BELLAVISTA',
      code: '01',
      parentId: '4439'
    },
    {
      id: '4443',
      name: 'HUALLAGA',
      code: '04',
      parentId: '4439'
    },
    {
      id: '4444',
      name: 'SAN PABLO',
      code: '05',
      parentId: '4439'
    },
    {
      id: '4445',
      name: 'SAN RAFAEL',
      code: '06',
      parentId: '4439'
    }
  ],
  4446: [
    {
      id: '4448',
      name: 'AGUA BLANCA',
      code: '02',
      parentId: '4446'
    },
    {
      id: '4447',
      name: 'SAN JOSE DE SISA',
      code: '01',
      parentId: '4446'
    },
    {
      id: '4449',
      name: 'SAN MARTIN',
      code: '03',
      parentId: '4446'
    },
    {
      id: '4450',
      name: 'SANTA ROSA',
      code: '04',
      parentId: '4446'
    },
    {
      id: '4451',
      name: 'SHATOJA',
      code: '05',
      parentId: '4446'
    }
  ],
  4452: [
    {
      id: '4454',
      name: 'ALTO SAPOSOA',
      code: '02',
      parentId: '4452'
    },
    {
      id: '4455',
      name: 'EL ESLABON',
      code: '03',
      parentId: '4452'
    },
    {
      id: '4456',
      name: 'PISCOYACU',
      code: '04',
      parentId: '4452'
    },
    {
      id: '4457',
      name: 'SACANCHE',
      code: '05',
      parentId: '4452'
    },
    {
      id: '4453',
      name: 'SAPOSOA',
      code: '01',
      parentId: '4452'
    },
    {
      id: '4458',
      name: 'TINGO DE SAPOSOA',
      code: '06',
      parentId: '4452'
    }
  ],
  4459: [
    {
      id: '4461',
      name: 'ALONSO DE ALVARADO',
      code: '02',
      parentId: '4459'
    },
    {
      id: '4462',
      name: 'BARRANQUITA',
      code: '03',
      parentId: '4459'
    },
    {
      id: '4463',
      name: 'CAYNARACHI',
      code: '04',
      parentId: '4459'
    },
    {
      id: '4464',
      name: 'CUQUMBUQUI',
      code: '05',
      parentId: '4459'
    },
    {
      id: '4460',
      name: 'LAMAS',
      code: '01',
      parentId: '4459'
    },
    {
      id: '4465',
      name: 'PINTO RECODO',
      code: '06',
      parentId: '4459'
    },
    {
      id: '4466',
      name: 'RUMISAPA',
      code: '07',
      parentId: '4459'
    },
    {
      id: '4467',
      name: 'SAN ROQUE DE CUMBAZA',
      code: '08',
      parentId: '4459'
    },
    {
      id: '4468',
      name: 'SHANAO',
      code: '09',
      parentId: '4459'
    },
    {
      id: '4469',
      name: 'TABALOSOS',
      code: '10',
      parentId: '4459'
    },
    {
      id: '4470',
      name: 'ZAPATERO',
      code: '11',
      parentId: '4459'
    }
  ],
  4471: [
    {
      id: '4473',
      name: 'CAMPANILLA',
      code: '02',
      parentId: '4471'
    },
    {
      id: '4474',
      name: 'HUICUNGO',
      code: '03',
      parentId: '4471'
    },
    {
      id: '4472',
      name: 'JUANJUI',
      code: '01',
      parentId: '4471'
    },
    {
      id: '4475',
      name: 'PACHIZA',
      code: '04',
      parentId: '4471'
    },
    {
      id: '4476',
      name: 'PAJARILLO',
      code: '05',
      parentId: '4471'
    }
  ],
  4477: [
    {
      id: '4479',
      name: 'BUENOS AIRES',
      code: '02',
      parentId: '4477'
    },
    {
      id: '4480',
      name: 'CASPISAPA',
      code: '03',
      parentId: '4477'
    },
    {
      id: '4478',
      name: 'PICOTA',
      code: '01',
      parentId: '4477'
    },
    {
      id: '4481',
      name: 'PILLUANA',
      code: '04',
      parentId: '4477'
    },
    {
      id: '4482',
      name: 'PUCACACA',
      code: '05',
      parentId: '4477'
    },
    {
      id: '4483',
      name: 'SAN CRISTOBAL',
      code: '06',
      parentId: '4477'
    },
    {
      id: '4484',
      name: 'SAN HILARION',
      code: '07',
      parentId: '4477'
    },
    {
      id: '4485',
      name: 'SHAMBOYACU',
      code: '08',
      parentId: '4477'
    },
    {
      id: '4486',
      name: 'TINGO DE PONASA',
      code: '09',
      parentId: '4477'
    },
    {
      id: '4487',
      name: 'TRES UNIDOS',
      code: '10',
      parentId: '4477'
    }
  ],
  4488: [
    {
      id: '4490',
      name: 'AWAJUN',
      code: '02',
      parentId: '4488'
    },
    {
      id: '4491',
      name: 'ELIAS SOPLIN VARGAS',
      code: '03',
      parentId: '4488'
    },
    {
      id: '4492',
      name: 'NUEVA CAJAMARCA',
      code: '04',
      parentId: '4488'
    },
    {
      id: '4493',
      name: 'PARDO MIGUEL',
      code: '05',
      parentId: '4488'
    },
    {
      id: '4494',
      name: 'POSIC',
      code: '06',
      parentId: '4488'
    },
    {
      id: '4489',
      name: 'RIOJA',
      code: '01',
      parentId: '4488'
    },
    {
      id: '4495',
      name: 'SAN FERNANDO',
      code: '07',
      parentId: '4488'
    },
    {
      id: '4496',
      name: 'YORONGOS',
      code: '08',
      parentId: '4488'
    },
    {
      id: '4497',
      name: 'YURACYACU',
      code: '09',
      parentId: '4488'
    }
  ],
  4498: [
    {
      id: '4500',
      name: 'ALBERTO LEVEAU',
      code: '02',
      parentId: '4498'
    },
    {
      id: '4501',
      name: 'CACATACHI',
      code: '03',
      parentId: '4498'
    },
    {
      id: '4502',
      name: 'CHAZUTA',
      code: '04',
      parentId: '4498'
    },
    {
      id: '4503',
      name: 'CHIPURANA',
      code: '05',
      parentId: '4498'
    },
    {
      id: '4504',
      name: 'EL PORVENIR',
      code: '06',
      parentId: '4498'
    },
    {
      id: '4505',
      name: 'HUIMBAYOC',
      code: '07',
      parentId: '4498'
    },
    {
      id: '4506',
      name: 'JUAN GUERRA',
      code: '08',
      parentId: '4498'
    },
    {
      id: '4507',
      name: 'LA BANDA DE SHILCAYO',
      code: '09',
      parentId: '4498'
    },
    {
      id: '4508',
      name: 'MORALES',
      code: '10',
      parentId: '4498'
    },
    {
      id: '4509',
      name: 'PAPAPLAYA',
      code: '11',
      parentId: '4498'
    },
    {
      id: '4510',
      name: 'SAN ANTONIO',
      code: '12',
      parentId: '4498'
    },
    {
      id: '4511',
      name: 'SAUCE',
      code: '13',
      parentId: '4498'
    },
    {
      id: '4512',
      name: 'SHAPAJA',
      code: '14',
      parentId: '4498'
    },
    {
      id: '4499',
      name: 'TARAPOTO',
      code: '01',
      parentId: '4498'
    }
  ],
  4513: [
    {
      id: '4515',
      name: 'NUEVO PROGRESO',
      code: '02',
      parentId: '4513'
    },
    {
      id: '4516',
      name: 'POLVORA',
      code: '03',
      parentId: '4513'
    },
    {
      id: '4517',
      name: 'SHUNTE',
      code: '04',
      parentId: '4513'
    },
    {
      id: '4514',
      name: 'TOCACHE',
      code: '01',
      parentId: '4513'
    },
    {
      id: '4518',
      name: 'UCHIZA',
      code: '05',
      parentId: '4513'
    }
  ],
  4520: [
    {
      id: '4522',
      name: 'ALTO DE LA ALIANZA',
      code: '02',
      parentId: '4520'
    },
    {
      id: '4523',
      name: 'CALANA',
      code: '03',
      parentId: '4520'
    },
    {
      id: '4524',
      name: 'CIUDAD NUEVA',
      code: '04',
      parentId: '4520'
    },
    {
      id: '4530',
      name: 'COR GREGORIO ALBARRACÍN',
      code: '10',
      parentId: '4520'
    },
    {
      id: '4525',
      name: 'INCLAN',
      code: '05',
      parentId: '4520'
    },
    {
      id: '4526',
      name: 'PACHIA',
      code: '06',
      parentId: '4520'
    },
    {
      id: '4527',
      name: 'PALCA',
      code: '07',
      parentId: '4520'
    },
    {
      id: '4528',
      name: 'POCOLLAY',
      code: '08',
      parentId: '4520'
    },
    {
      id: '4529',
      name: 'SAMA',
      code: '09',
      parentId: '4520'
    },
    {
      id: '4521',
      name: 'TACNA',
      code: '01',
      parentId: '4520'
    }
  ],
  4531: [
    {
      id: '4533',
      name: 'CAIRANI',
      code: '02',
      parentId: '4531'
    },
    {
      id: '4534',
      name: 'CAMILACA',
      code: '03',
      parentId: '4531'
    },
    {
      id: '4532',
      name: 'CANDARAVE',
      code: '01',
      parentId: '4531'
    },
    {
      id: '4535',
      name: 'CURIBAYA',
      code: '04',
      parentId: '4531'
    },
    {
      id: '4536',
      name: 'HUANUARA',
      code: '05',
      parentId: '4531'
    },
    {
      id: '4537',
      name: 'QUILAHUANI',
      code: '06',
      parentId: '4531'
    }
  ],
  4538: [
    {
      id: '4540',
      name: 'ILABAYA',
      code: '02',
      parentId: '4538'
    },
    {
      id: '4541',
      name: 'ITE',
      code: '03',
      parentId: '4538'
    },
    {
      id: '4539',
      name: 'LOCUMBA',
      code: '01',
      parentId: '4538'
    }
  ],
  4542: [
    {
      id: '4544',
      name: 'CHUCATAMANI',
      code: '02',
      parentId: '4542'
    },
    {
      id: '4545',
      name: 'ESTIQUE',
      code: '03',
      parentId: '4542'
    },
    {
      id: '4546',
      name: 'ESTIQUE-PAMPA',
      code: '04',
      parentId: '4542'
    },
    {
      id: '4547',
      name: 'SITAJARA',
      code: '05',
      parentId: '4542'
    },
    {
      id: '4548',
      name: 'SUSAPAYA',
      code: '06',
      parentId: '4542'
    },
    {
      id: '4543',
      name: 'TARATA',
      code: '01',
      parentId: '4542'
    },
    {
      id: '4549',
      name: 'TARUCACHI',
      code: '07',
      parentId: '4542'
    },
    {
      id: '4550',
      name: 'TICACO',
      code: '08',
      parentId: '4542'
    }
  ],
  4552: [
    {
      id: '4554',
      name: 'CORRALES',
      code: '02',
      parentId: '4552'
    },
    {
      id: '4555',
      name: 'LA CRUZ',
      code: '03',
      parentId: '4552'
    },
    {
      id: '4556',
      name: 'PAMPAS DE HOSPITAL',
      code: '04',
      parentId: '4552'
    },
    {
      id: '4557',
      name: 'SAN JACINTO',
      code: '05',
      parentId: '4552'
    },
    {
      id: '4558',
      name: 'SAN JUAN DE LA VIRGEN',
      code: '06',
      parentId: '4552'
    },
    {
      id: '4553',
      name: 'TUMBES',
      code: '01',
      parentId: '4552'
    }
  ],
  4559: [
    {
      id: '4561',
      name: 'CASITAS',
      code: '02',
      parentId: '4559'
    },
    {
      id: '4560',
      name: 'ZORRITOS',
      code: '01',
      parentId: '4559'
    }
  ],
  4562: [
    {
      id: '4564',
      name: 'AGUAS VERDES',
      code: '02',
      parentId: '4562'
    },
    {
      id: '4565',
      name: 'MATAPALO',
      code: '03',
      parentId: '4562'
    },
    {
      id: '4566',
      name: 'PAPAYAL',
      code: '04',
      parentId: '4562'
    },
    {
      id: '4563',
      name: 'ZARUMILLA',
      code: '01',
      parentId: '4562'
    }
  ],
  4568: [
    {
      id: '4569',
      name: 'CALLERIA',
      code: '01',
      parentId: '4568'
    },
    {
      id: '4570',
      name: 'CAMPOVERDE',
      code: '02',
      parentId: '4568'
    },
    {
      id: '4571',
      name: 'IPARIA',
      code: '03',
      parentId: '4568'
    },
    {
      id: '4572',
      name: 'MASISEA',
      code: '04',
      parentId: '4568'
    },
    {
      id: '4574',
      name: 'NUEVA REQUENA',
      code: '06',
      parentId: '4568'
    },
    {
      id: '4573',
      name: 'YARINACOCHA',
      code: '05',
      parentId: '4568'
    }
  ],
  4575: [
    {
      id: '4576',
      name: 'RAYMONDI',
      code: '01',
      parentId: '4575'
    },
    {
      id: '4577',
      name: 'SEPAHUA',
      code: '02',
      parentId: '4575'
    },
    {
      id: '4578',
      name: 'TAHUANIA',
      code: '03',
      parentId: '4575'
    },
    {
      id: '4579',
      name: 'YURUA',
      code: '04',
      parentId: '4575'
    }
  ],
  4580: [
    {
      id: '4583',
      name: 'CURIMANA',
      code: '03',
      parentId: '4580'
    },
    {
      id: '4582',
      name: 'IRAZOLA',
      code: '02',
      parentId: '4580'
    },
    {
      id: '4581',
      name: 'PADRE ABAD',
      code: '01',
      parentId: '4580'
    }
  ],
  4584: [
    {
      id: '4585',
      name: 'PURUS',
      code: '01',
      parentId: '4584'
    }
  ]
}

export default data
