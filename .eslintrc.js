const RULES = {
  OFF: 'off',
  ERROR: 'error',
  WARN: 'warn'
}

module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true
  },
  settings: {
    'import/resolver': {
      alias: {
        extensions: ['.js', '.jsx'],
        map: [
          ['@components', './components'],
          ['@styles', './styles'],
          ['@context', './context'],
          ['@models', './models'],
          ['@services', './services'],
          ['@features', './features'],
          ['@utils', './utils'],
          ['@hooks', './hooks']
        ]
      }
    }
  },
  extends: [
    'next/core-web-vitals',
    'plugin:react/recommended',
    'standard'
  ],
  parser: '@babel/eslint-parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 13,
    sourceType: 'module'
  },
  plugins: [
    'react',
    '@babel'
  ],
  rules: {
    'react/react-in-jsx-scope': RULES.OFF
  }
}
