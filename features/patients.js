import { FirebaseFirestore } from '@services/firebase'
import {
  collection,
  onSnapshot,
  doc,
  updateDoc
} from 'firebase/firestore'

import ModelPatient from '@models/UserPatient'

class FeaturesPatients {
  constructor () {
    this.init = true
    this.collection = 'patients'
  }

  getPatients (callback) {
    const collectionPatients = collection(FirebaseFirestore, this.collection)
    return onSnapshot(collectionPatients, (snapshot) => {
      const listPatients = []
      snapshot.forEach((patient) => {
        listPatients.push(patient.data())
      })
      callback(listPatients)
    })
  }

  async createPatient (patient) {
    const objPatient = new ModelPatient(patient)
    console.log(objPatient.toJSON())
    /* const newMeet = doc(collection(FirebaseFirestore, this.collection))
    return await setDoc(newMeet, { ...objPatient, id: newMeet.id }) */
  }

  editPatient (patient) {
    try {
      const patientRef = doc(FirebaseFirestore, this.collection, patient.id)
      updateDoc(patientRef, patient)
      return true
    } catch (error) {
      console.error(error)
      return false
    }
  }

  initialPatientState () {
    return {
      id: '',
      numberHistory: '',
      refHistoryClinic: '',
      dni: '',
      photo: null,
      fullname: '',
      shortname: '',
      names: '',
      firtsSurname: '',
      secondSurname: '',
      department: '',
      province: '',
      district: '',
      ubigeo: [],
      address: '',
      dateOfBirth: null,
      age: '',
      gender: '',
      bloodType: '',
      occupation: '',
      phones: null,
      civilStatus: '',
      createdAt: null,
      inSession: false,
      lastSession: null,
      updatedAt: null,
      email: '',
      password: '',
      access: 'patient',
      type: 'patient'
    }
  }
}

export default new FeaturesPatients()
