/* eslint-disable no-undef */
import { FirebaseFirestore } from '@services/firebase'
import {
  collection,
  onSnapshot,
  query,
  setDoc,
  doc,
  deleteDoc,
  updateDoc
} from 'firebase/firestore'

import Meeting from '@models/meeting'

class FeaturesMeetings {
  constructor () {
    this.collection = 'meetings'
  }

  getMeetings (callback) {
    const queryResult = query(collection(FirebaseFirestore, this.collection))
    return onSnapshot(queryResult, (snapshot) => {
      const meetings = []
      snapshot.forEach((doc) => {
        meetings.push({ ...doc.data(), id: doc.id })
      })
      callback(meetings)
    })
  }

  // getMeeting() {}

  addMeeting (payload, successCallback, errorCallback) {
    const objMeeting = new Meeting(payload)
    const docRef = doc(collection(FirebaseFirestore, this.collection))
    setDoc(docRef, objMeeting.toJSON())
      .then(successCallback)
      .catch(errorCallback)
  }

  updateMeeting (payload, successCallback, errorCallback) {
    const objMeeting = new Meeting(payload)
    const docRef = doc(FirebaseFirestore, this.collection, payload.id)
    updateDoc(docRef, objMeeting.toJSON())
      .then(successCallback)
      .catch(errorCallback)
  }

  deleteMeeting (payload, successCallback, errorCallback) {
    const docRef = doc(FirebaseFirestore, this.collection, payload.id)
    deleteDoc(docRef)
      .then(successCallback)
      .catch(errorCallback)
  }
}

export default new FeaturesMeetings()
