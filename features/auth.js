import axios from 'axios'
import nookies from 'nookies'

class FeaturesAuth {
  constructor () {
    this.init = true
  }

  static async withAuthProps (context) {
    try {
      const { token } = nookies.get(context)

      const tokenIsValid = await axios.post('http://localhost:3000/api/token/verify', { token }, {
        headers: {
          'Content-Type': 'application/json'
        }
      })

      const { success } = tokenIsValid.data

      if (!success) {
        return {
          redirect: {
            destination: '/login',
            permanent: false
          }
        }
      }

      return {
        props: {}
      }
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error)
      return {}
    }
  }
}

export default FeaturesAuth
