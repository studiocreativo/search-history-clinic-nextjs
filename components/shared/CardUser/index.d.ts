import UserPatient from '@models/UserPatient'

import { CardUserActionProps } from './CardUserAction'

export interface CardUserProps extends CardUserActionProps {
  /**
   * Datos del Usuario.
   */
  children?: Array<React.ReactNode>;
  /**
   * Una instancia de un usuario.
   */
  user: UserPatient;
  cardActionItems: JSX.Element
}

export default function CardUser(props: CardUserProps): JSX.Element
