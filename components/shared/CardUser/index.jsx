import PropTypes from 'prop-types'

import {
  Card as MuiCard,
  CardHeader as MuiCardHeader,
  CardMedia as MuiCardMedia,
  CardContent as MuiCardContent
} from '@mui/material'

import styles from '@styles/shared/Card.module.scss'

const CardUser = ({
  children,
  user,
  cardActionItems
}) => {
  const { photo, names } = user

  return (
    <MuiCard className={styles.Card}>
      <MuiCardHeader
        action={cardActionItems}
      />
      <MuiCardMedia
        component="img"
        image={photo ?? '/avatar.jpg'}
        alt={names}
      />
      <MuiCardContent>
        {children}
      </MuiCardContent>
    </MuiCard>
  )
}

CardUser.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element).isRequired,
  user: PropTypes.object.isRequired,
  cardActionItems: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element
  ]).isRequired
}

CardUser.defaultProps = {
  onEdit: () => { },
  onDelete: () => { }
}

export default CardUser
