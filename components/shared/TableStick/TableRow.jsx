import PropTypes from 'prop-types'

import {
  TableRow as MuiTableRow,
  TableCell as MuiTableCell,
  Checkbox as MuiCheckbox
} from '@mui/material'

const TableRow = ({
  children,
  selectedProps: {
    selectable,
    isItemSelected,
    labelId
  },
  ...tableRowProps
}) => {
  return (
    <MuiTableRow
      {...tableRowProps}
      role={selectable ? 'checkbox' : undefined}
      aria-checked={selectable ? isItemSelected : undefined}
      selected={isItemSelected}
    >
      {
        selectable &&
        (
          <MuiTableCell padding="checkbox">
            <MuiCheckbox
              color="primary"
              checked={isItemSelected}
              inputProps={{
                'aria-labelledby': labelId
              }}
            />
          </MuiTableCell>
        )
      }
      {children}
    </MuiTableRow>
  )
}

TableRow.propTypes = {
  children: PropTypes.node.isRequired,
  selectedProps: PropTypes.shape({
    selectable: PropTypes.bool,
    isItemSelected: PropTypes.bool,
    labelId: PropTypes.string
  })
}

TableRow.defaultProps = {
  selectedProps: {
    selectable: false,
    isItemSelected: false
  }
}

export default TableRow
