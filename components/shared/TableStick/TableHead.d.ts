interface TableHeadData {
  id: string;
  label: string;
  numeric?: boolean;
}

interface SelectedProps {
  selected: boolean;
  numSelected: number;
  rowCount: number;
  onSelectAllClick: () => void;
}

interface OrderProps {
  order: 'asc' | 'desc';
  orderBy: string;
  onRequestSort: () => void;
}

export interface TableHeadProps {
  data: TableHeadData[];
  selectedProps: SelectedProps;
  orderProps: OrderProps;
}

export function TableHead(props: TableHeadProps): JSX.Element;