import PropTypes from 'prop-types'

import {
  TableHead as MuiTableHead,
  TableRow as MuiTableRow,
  TableSortLabel as MuiTableSortLabel,
  TableCell as MuiTableCell,
  Checkbox as MuiCheckbox,
  Box as MuiBox
} from '@mui/material'

import { visuallyHidden } from '@mui/utils'

const TableHead = ({
  data,
  selectedProps: {
    selectable,
    numSelected,
    rowCount,
    onSelectAllClick
  },
  orderProps: {
    sorteable,
    order,
    orderBy,
    onRequestSort
  }
}) => {
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property)
  }

  return (
    <MuiTableHead>
      <MuiTableRow>
        {
          selectable &&
          (
            <MuiTableCell padding="checkbox">
              <MuiCheckbox
                color="primary"
                indeterminate={numSelected > 0 && numSelected < rowCount}
                checked={rowCount > 0 && numSelected === rowCount}
                onChange={onSelectAllClick}
                inputProps={{
                  'aria-label': 'select all desserts'
                }}
              />
            </MuiTableCell>
          )
        }
        {
          data.map(({
            id,
            label,
            aling
          }) => (
            <MuiTableCell
              key={id}
              align={aling}
              sortDirection={orderBy === id ? order : false}
            >
              {!sorteable && label}
              {
                sorteable &&
                (
                  <MuiTableSortLabel
                    active={orderBy === id}
                    direction={orderBy === id ? order : 'asc'}
                    onClick={createSortHandler(id)}
                  >
                    {label}
                    {
                      (orderBy === id) &&
                      (
                        <MuiBox component="span" sx={visuallyHidden}>
                          {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                        </MuiBox>
                      )
                    }
                  </MuiTableSortLabel>
                )
              }
            </MuiTableCell>
          ))
        }
      </MuiTableRow>
    </MuiTableHead>
  )
}

TableHead.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  selectedProps: PropTypes.shape({
    selectable: PropTypes.bool,
    numSelected: PropTypes.number,
    rowCount: PropTypes.number,
    onSelectAllClick: PropTypes.func
  }),
  orderProps: PropTypes.shape({
    sorteable: PropTypes.bool,
    order: PropTypes.oneOf(['asc', 'desc']),
    orderBy: PropTypes.string,
    onRequestSort: PropTypes.func
  }).isRequired
}

TableHead.defaultProps = {
  selectedProps: {
    selectable: false
  }
}

export default TableHead
