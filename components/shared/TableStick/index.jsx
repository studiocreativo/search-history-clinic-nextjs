import { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

import {
  Paper as MuiPaper,
  Table as MuiTable,
  TableContainer as MuiTableContainer,
  TableBody as MuiTableBody,
  TablePagination as MuiTablePagination
} from '@mui/material'
import TableHead from './TableHead'

function descendingComparator (a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1
  }
  if (b[orderBy] > a[orderBy]) {
    return 1
  }
  return 0
}

function getComparator (order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy)
}

// This method is created for cross-browser compatibility, if you don't
// need to support IE11, you can use Array.prototype.sort() directly
function stableSort (array, comparator) {
  const stabilizedThis = array?.map((el, index) => [el, index])
  stabilizedThis?.sort((a, b) => {
    const order = comparator(a[0], b[0])
    if (order !== 0) {
      return order
    }
    return a[1] - b[1]
  })
  return stabilizedThis?.map((el) => el[0])
}

const Table = ({
  idKey,
  data,
  headers,
  RowBuilder,
  stickyHeader,
  tablePagination,
  rowsPerPageOptions,
  sorteable,
  sorteableExternal,
  selectable,
  selectedList,
  onSelectAllClick
}) => {
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(10)
  const [order, setOrder] = useState('asc')
  const [orderBy, setOrderBy] = useState(null)

  const isDataEmpty = data?.length === 0
  const normalizeSorteableData = sorteable ? stableSort(data, getComparator(order, orderBy)) : data

  useEffect(() => {
    setOrderBy(sorteableExternal)
  }, [sorteableExternal])

  const isSelected = (hash) => selectedList.indexOf(hash) !== -1

  const handlePagination = () => {
    if (tablePagination) {
      return (
        normalizeSorteableData
          ?.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          ?.map((item, index) => {
            return (
              <RowBuilder
                key={`${item[idKey]}-${index}`}
                dataRow={item}
                selectedProps={{
                  selectable: selectable,
                  isItemSelected: isSelected(item[idKey]),
                  labelId: `enhanced-table-checkbox-${index}`
                }}
              />
            )
          })
      )
    }

    return (
      normalizeSorteableData
        .map((item, index) => {
          return (
            <RowBuilder
              key={`${item[idKey]}-${index}`}
              dataRow={item}
              selectedProps={{
                selectable: selectable,
                isItemSelected: isSelected(item[idKey]),
                labelId: `enhanced-table-checkbox-${index}`
              }}
            />
          )
        })
    )
  }

  const handlePageChange = (_, newPage) => setPage(newPage)

  const handleRowsPerPageChange = ({ target: { value } }) => {
    setRowsPerPage(parseInt(value, 10))
    setPage(0)
  }

  const handleRequestSort = (_, property) => {
    const isAsc = order === 'asc'
    if (orderBy === property) {
      setOrder(isAsc ? 'desc' : 'asc')
    }
    const existsSorteableExternal = Boolean(sorteableExternal)
    if (!existsSorteableExternal) {
      setOrderBy(property)
    }
  }

  return (
    <MuiPaper>
      <MuiTableContainer sx={{ height: 550 }}>
        <MuiTable stickyHeader={stickyHeader}>
          <TableHead
            data={headers}
            orderProps={{
              sorteable,
              order,
              orderBy,
              onRequestSort: handleRequestSort
            }}
            selectedProps={{
              selectable: selectable,
              numSelected: selectedList.length,
              rowCount: data?.length,
              onSelectAllClick: onSelectAllClick
            }}
          />
          <MuiTableBody>
            {
              !isDataEmpty && handlePagination()
            }
          </MuiTableBody>
        </MuiTable>
      </MuiTableContainer>
      {
        tablePagination &&
        <MuiTablePagination
          rowsPerPageOptions={rowsPerPageOptions}
          component="div"
          count={data?.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handlePageChange}
          onRowsPerPageChange={handleRowsPerPageChange}
        />
      }
    </MuiPaper>
  )
}

Table.propTypes = {
  idKey: PropTypes.string,
  data: PropTypes.array.isRequired,
  headers: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    aling: PropTypes.string.isRequired
  })).isRequired,
  RowBuilder: PropTypes.func.isRequired,
  stickyHeader: PropTypes.bool,
  tablePagination: PropTypes.bool,
  rowsPerPageOptions: PropTypes.array,
  sorteable: PropTypes.bool,
  sorteableExternal: PropTypes.string,
  selectable: PropTypes.bool,
  selectedList: PropTypes.array,
  onSelectAllClick: PropTypes.func
}

Table.defaultProps = {
  idKey: 'id',
  stickyHeader: false,
  tablePagination: false,
  rowsPerPageOptions: [5, 10, 25],
  sorteable: false,
  selectable: false,
  selectedList: []
}

export default Table
