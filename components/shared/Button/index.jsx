import PropTypes from 'prop-types'
import clsx from 'clsx'

import { Button as MuiButton } from '@mui/material'

import styles from '@styles/components/Button.module.scss'

const Button = ({
  children,
  className,
  ...otherProps
}) => {
  const mixinStyles = clsx(styles.Button, className)
  return (
    <MuiButton
      className={mixinStyles}
      {...otherProps}
    >
      {children}
    </MuiButton>
  )
}

Button.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string
}

Button.defaultProps = {
  variant: 'contained',
  color: 'primary',
  size: 'large'
}

export default Button
