import { ButtonProps as MuiButtonProps } from '@mui/material/Button'

export interface ButtonProps extends MuiButtonProps {}

export default function Button(props: ButtonProps): JSX.Element
