import { TextFieldProps as MuiTextFieldProps } from '@mui/material/TextField'

export interface TextFieldProps extends Omit<MuiTextFieldProps, 'value' | 'onChange' | 'onKeyPress' | 'InputProps'> {}

export default function InputTags(props: TextFieldProps): JSX.Element