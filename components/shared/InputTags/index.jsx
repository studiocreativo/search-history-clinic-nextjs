import { useState } from 'react'
import PropTypes from 'prop-types'
import clsx from 'clsx'

import {
  TextField as MuiTextField,
  Chip as MuiChip
} from '@mui/material'

import styles from '@styles/shared/InputTags.module.scss'

const InputTags = ({
  className,
  value,
  onChange,
  ...otherProps
}) => {
  const mixinStyles = clsx(styles.InputTags, className)
  const [text, setText] = useState('')

  const handleChangeValue = ({ target: { value } }) => setText(value)
  const handleOnKeyPress = ({ key }) => {
    if (key === 'Enter') {
      const previousValue = value ?? []
      const chips = [...previousValue, text]
      onChange(chips)
      setText('')
    }
  }
  const handleDeleteChip = (item) => {
    const chips = [...value]
    chips.splice(chips.indexOf(item), 1)
    onChange(chips)
  }

  const mapValueToChips = value?.map((text) => (
    <MuiChip key={text} label={text} onDelete={handleDeleteChip} />
  ))

  return (
    <MuiTextField
      className={mixinStyles}
      value={text}
      onChange={handleChangeValue}
      onKeyPress={handleOnKeyPress}
      InputProps={{
        startAdornment: mapValueToChips
      }}
      {...otherProps}
    />
  )
}

InputTags.propTypes = {
  className: PropTypes.string,
  value: PropTypes.arrayOf(PropTypes.string),
  onChange: PropTypes.func
}

InputTags.defaultProps = {
  value: [],
  onChange: () => {}
}

export default InputTags
