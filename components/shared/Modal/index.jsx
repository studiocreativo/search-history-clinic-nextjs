import PropTypes from 'prop-types'

import {
  Dialog as MuiDialog,
  DialogActions as MuiDialogActions,
  DialogContent as MuiDialogContent,
  DialogTitle as MuiDialogTitle,
  IconButton as MuiIconButton
} from '@mui/material'
import {
  faTimes
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Button from '@components/shared/Button'

import styles from '@styles/shared/Modal.module.scss'

const ACTIONS_CANCEL = 'Cancelar'
const ACTIONS_SAVE = 'Guardar'

const Modal = ({
  title,
  maxWidth,
  open,
  onClose,
  onSubmit,
  textSubmitButton,
  children
}) => (
  <MuiDialog open={open} maxWidth={maxWidth} fullWidth className={styles.Modal}>
    <MuiDialogTitle>
      {title}
      <MuiIconButton
        aria-label="close"
        onClick={onClose}
      >
        <FontAwesomeIcon icon={faTimes} />
      </MuiIconButton>
    </MuiDialogTitle>
    <MuiDialogContent>
      {children}
    </MuiDialogContent>
    <MuiDialogActions>
      <Button variant="outlined" onClick={onClose}>{ACTIONS_CANCEL}</Button>
      <Button autoFocus onClick={onSubmit}>
        {textSubmitButton}
      </Button>
    </MuiDialogActions>
  </MuiDialog>
)

Modal.propTypes = {
  title: PropTypes.string.isRequired,
  maxWidth: PropTypes.string,
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  textSubmitButton: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element
  ])
}

Modal.defaultProps = {
  maxWidth: 'xs',
  onSubmit: () => {},
  textSubmitButton: ACTIONS_SAVE
}

export default Modal
