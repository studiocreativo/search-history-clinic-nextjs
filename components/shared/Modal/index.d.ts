export interface ModalProps {
  title: string;
  maxWidth?: string;
  open: boolean;
  onClose: () => void;
  onSubmit: () => void;
  textSubmitButton?: string;
  children: React.ReactNode;
}

export default function Modal(props: ModalProps): JSX.Element