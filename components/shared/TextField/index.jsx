import {
  TextField as MuiTextField,
  InputAdornment as MuiInputAdornment
} from '@mui/material'
import PropTypes from 'prop-types'

const TextField = ({
  children,
  InputAdornmentStart,
  InputAdornmentEnd,
  ...otherProps
}) => (
  <MuiTextField
    {...otherProps}
    InputProps={{
      startAdornment: (
        InputAdornmentStart && (
        <MuiInputAdornment position="start">
          {InputAdornmentStart}
        </MuiInputAdornment>
        )
      ),
      endAdornment: (
        InputAdornmentEnd && (
        <MuiInputAdornment position="end">
          {InputAdornmentEnd}
        </MuiInputAdornment>
        )
      )
    }}
  >
    {children}
  </MuiTextField>
)

TextField.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element
  ]),
  InputAdornmentStart: PropTypes.element,
  InputAdornmentEnd: PropTypes.element
}

TextField.defaultProps = {
  value: '',
  variant: 'outlined',
  type: 'text',
  size: 'medium'
}

export default TextField
