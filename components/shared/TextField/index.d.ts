import { TextFieldProps as MuiTextFieldProps } from '@mui/material'

export interface TextFieldProps extends Omit<MuiTextFieldProps, 'InputProps'> {}

export default function TextField(props: TextFieldProps): JSX.Element