import { useState } from 'react'
import PropTypes from 'prop-types'
import clsx from 'clsx'
import Box from '@material-ui/core/Box'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Container from '@material-ui/core/Container'
import MenuIcon from '@material-ui/icons/Menu'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import {
  Badge, Avatar, Menu, MenuItem
} from '@material-ui/core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faBell,
  faHome,
  faUserMd,
  faUserInjured,
  faCommentMedical
} from '@fortawesome/free-solid-svg-icons'
import Link from '@components/shared/Link'
import styles from '@styles/shared/Template.module.scss'
import AppBar from './AppBar'
import Drawer from './Drawer'
import NormalizeHeader from './NormalizeHeader'

const ListMenu = [
  {
    id: 1,
    label: 'Inicio',
    path: '/',
    icon: faHome
  },
  {
    id: 2,
    label: 'Doctores',
    path: '/doctors',
    icon: faUserMd
  },
  {
    id: 3,
    label: 'Pacientes',
    path: '/patients',
    icon: faUserInjured
  },
  {
    id: 4,
    label: 'Citas',
    path: '/meetings',
    icon: faCommentMedical
  }
]

const Template = ({ children, title }) => {
  const [openDrawer, setOpenDrawer] = useState(false)

  const menuIconStyles = clsx(
    styles.MenuIcon, {
      [styles['MenuIcon-open']]: openDrawer
    }
  )

  const handleDrawerOpen = () => setOpenDrawer(true)

  const handleDrawerClose = () => setOpenDrawer(false)

  // Profile
  const [anchorEl, setAnchorEl] = useState(null)
  const open = Boolean(anchorEl)
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = () => {
    setAnchorEl(null)
  }

  return (
    <Box className={styles.Container}>
      <AppBar position="fixed" open={openDrawer}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="Abrir Drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={menuIconStyles}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            component="h1"
            variant="h6"
            noWrap
            style={{
              flexGrow: 1
            }}
          >
            {title}
          </Typography>
          {/* <TextField
            size="small"
            placeholder="Buscar..."
            className={styles.SearchField}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <FontAwesomeIcon icon={faSearch} size="xs" />
                </InputAdornment>
              ),
            }}
          /> */}
          <Badge badgeContent={7} color="danger" className={styles.Notifications}>
            <FontAwesomeIcon icon={faBell} />
          </Badge>
          <IconButton onClick={handleClick} className={styles.ProfileAvatar}>
            <Avatar variant="rounded" src="https://mui.com/static/images/avatar/3.jpg" />
          </IconButton>
          <Menu
            anchorEl={anchorEl}
            open={open}
            onClose={handleClose}
            onClick={handleClose}
            className={styles.ProfileMenuAvatar}
          >
            <MenuItem>
              Add another account
            </MenuItem>
            <MenuItem>

              Settings
            </MenuItem>
            <MenuItem>

              Logout
            </MenuItem>
          </Menu>

        </Toolbar>
      </AppBar>
      <Drawer variant="permanent" open={openDrawer}>
        <NormalizeHeader>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
        </NormalizeHeader>
        <List>
          {ListMenu.map((el) => (
            <Link href={el.path} key={el.id}>
              <ListItem button>
                <ListItemIcon>
                  <FontAwesomeIcon icon={el.icon} />
                </ListItemIcon>
                <ListItemText primary={el.label} />
              </ListItem>
            </Link>
          ))}
        </List>
      </Drawer>
      <Box component="main" className={styles.WrapChildren} style={{ background: '#FAFAFA' }}>
        <NormalizeHeader />
        <Container maxWidth="lg" className={styles.Children}>
          {children}
        </Container>
      </Box>
    </Box>
  )
}

Template.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string
}

Template.defaultProps = {
  title: 'T&iacute;tulo'
}

export default Template
