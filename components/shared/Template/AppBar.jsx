import clsx from 'clsx'
import PropTypes from 'prop-types'
import MuiAppBar from '@material-ui/core/AppBar'
import styles from '@styles/shared/Template.module.scss'

const AppBar = ({ open, ...otherProps }) => {
  const classes = clsx(
    styles.AppBar, {
      [styles['AppBar-open']]: open
    }
  )

  return (<MuiAppBar className={classes} {...otherProps} />)
}

AppBar.propTypes = {
  open: PropTypes.bool.isRequired
}

export default AppBar
