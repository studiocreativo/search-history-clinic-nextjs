import styles from '@styles/shared/Template.module.scss'

const NormalizeHeader = ({ ...otherProps }) => (
  <div
    className={styles.NormalizeHeader}
    {...otherProps}
  />
)

export default NormalizeHeader
