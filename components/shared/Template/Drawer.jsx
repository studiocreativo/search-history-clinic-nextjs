import PropTypes from 'prop-types'
import clsx from 'clsx'
import MuiDrawer from '@material-ui/core/Drawer'
import styles from '@styles/shared/Template.module.scss'

const Drawer = ({ open, ...otherProps }) => {
  const classes = clsx(
    styles.Drawer, {
      [styles['Drawer-open']]: open,
      [styles['Drawer-closed']]: !open
    }
  )
  return (
    <MuiDrawer
      className={classes}
      {...otherProps}
    />
  )
}

Drawer.propTypes = {
  open: PropTypes.bool.isRequired
}

export default Drawer
