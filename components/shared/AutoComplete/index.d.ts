import { AutocompleteProps as MuiAutocompleteProps } from '@mui/material/Autocomplete'

export interface AutoCompleteProps extends MuiAutocompleteProps {}

export default function AutoComplete(props: AutoCompleteProps): JSX.Element