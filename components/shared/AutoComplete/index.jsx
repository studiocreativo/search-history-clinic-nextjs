import { Autocomplete as MuiAutocomplete } from '@mui/material'

const AutoComplete = ({ ...otherProps }) => (
  <MuiAutocomplete
    disablePortal
    {...otherProps}
  />
)

export default AutoComplete
