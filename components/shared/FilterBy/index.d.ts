interface FilterByOption {
  label: string;
  value: string;
}

export interface FilterByProps {
  onFilterBy: (filterBy: string) => void;
  options: FilterByOption[];
}

export default function FilterBy(props: FilterByProps): JSX.Element