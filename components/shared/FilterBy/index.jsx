import { useState } from 'react'
import PropTypes from 'prop-types'

import {
  MenuItem as MuiMenuItem,
  TextField as MuiTextField,
  InputAdornment as MuiInputAdornment
} from '@mui/material'

import styles from '@styles/shared/FilterBy.module.scss'

const FilterBy = ({
  options,
  onFilterBy,
  optionDefaultFilterBy
}) => {
  const [select, setSelect] = useState(optionDefaultFilterBy)
  const [open, setOpen] = useState(false)

  const InputAdornmentStart = (
    <MuiInputAdornment position="start">
      Ordenar por :
    </MuiInputAdornment>
  )

  const InputAdornmentEnd = (
    <MuiInputAdornment position="start" style={{ marginRight: '20px' }}>
      |
    </MuiInputAdornment>
  )

  const handleChange = ({ target: { value } }) => {
    setSelect(value)
    onFilterBy(value)
  }

  const handleSelectToogle = (e) => {
    e.stopPropagation()
    e.preventDefault()
    setOpen(!open)
  }

  return (
    <MuiTextField
      className={styles.Input}
      select
      SelectProps={{
        MenuProps: {
          PopoverClasses: {
            paper: styles.Popover
          }
        },
        open,
        onOpen: handleSelectToogle,
        onClose: handleSelectToogle
      }}
      InputProps={{
        startAdornment: InputAdornmentStart,
        endAdornment: InputAdornmentEnd
      }}
      value={select}
      onClick={handleSelectToogle}
      onChange={handleChange}
    >
      {
        optionDefaultFilterBy === 'none' &&
        (
          <MuiMenuItem value="none">
            Ninguno
          </MuiMenuItem>
        )
      }
      {
        options.map(({ value, label }) => (
          <MuiMenuItem key={value} value={value}>
            {label}
          </MuiMenuItem>
        ))
      }
    </MuiTextField>
  )
}

FilterBy.propTypes = {
  onFilterBy: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired
  })),
  optionDefaultFilterBy: PropTypes.string
}

FilterBy.defaultProps = {
  options: [],
  optionDefaultFilterBy: 'none'
}

export default FilterBy
