import { forwardRef } from 'react'
import PropTypes from 'prop-types'

import {
  Dialog as MuiDialog,
  Fade as MuiFade
} from '@mui/material'

import styles from '@styles/shared/Swal.module.scss'

const Transition = forwardRef((props, ref) => <MuiFade in ref={ref} {...props} />)

Transition.displayName = 'Transition'

const Swal = ({
  open,
  onClose,
  children
}) => (
  <div>
    <MuiDialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      onClose={onClose}
      maxWidth="xs"
      className={styles.Swal}
      disableEnforceFocus
    >
      {children}
    </MuiDialog>
  </div>
)

Swal.propTypes = {
  children: PropTypes.node.isRequired,
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired
}

export default Swal
