import { DialogProps } from '@mui/material'
export interface SwalProps extends DialogProps {
  children: React.ReactNode;
  open: boolean;
  onClose: () => void;
}

export default function Swal(props: SwalProps): JSX.Element