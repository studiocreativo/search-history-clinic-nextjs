import PropTypes from 'prop-types'
import {
  Toolbar,
  Typography,
  Grid,
  TextField,
  InputAdornment
} from '@material-ui/core'
import FilterBy from '@components/shared/FilterBy'
import Button from '@components/shared/Button'
import SwitchIcon from '@components/shared/SwitchIcon'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus, faSearch } from '@fortawesome/free-solid-svg-icons'
import styles from '@styles/shared/ToolBarMain.module.scss'

const ToolBarMain = ({
  title,
  onFilterBy,
  optionsFilter,
  buttonText,
  // Methods
  onSearchData,
  buttonOnClick,
  onSelectedView,
  optionDefaultFilterBy
}) => {
  const iconButtonPlus = (<FontAwesomeIcon icon={faPlus} size="xs" />)
  const existsTitle = Boolean(title)
  const justifyContentContainer = existsTitle ? 'normal' : 'flex-end'

  return (
    <Toolbar className={styles.ToolBar}>
      <Grid container spacing={2} direction="row" alignItems="flex-end" justifyContent={justifyContentContainer}>
        {
          title &&
          (
            <Grid item xs={12} lg={2}>
              <Typography component="h1" variant="h6" noWrap>{title}</Typography>
            </Grid>
          )
        }
        <Grid item xs={12} lg={10} >
          <Grid container direction="row" alignItems="center" justifyContent="flex-end">
            <TextField
              size="small"
              placeholder="Buscar..."
              className="input-search"
              onChange={(e) => onSearchData(e.target.value)}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <FontAwesomeIcon icon={faSearch} size="xs" />
                  </InputAdornment>
                )
              }}
            />
            <FilterBy onFilterBy={onFilterBy} options={optionsFilter} optionDefaultFilterBy={optionDefaultFilterBy} />
            <SwitchIcon onSelected={onSelectedView} />
            <Button onClick={buttonOnClick} startIcon={iconButtonPlus}>{buttonText}</Button>
          </Grid>
        </Grid>
      </Grid>
    </Toolbar>
  )
}

ToolBarMain.propTypes = {
  title: PropTypes.string,
  onFilterBy: PropTypes.func.isRequired,
  optionsFilter: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired
  })),
  buttonText: PropTypes.string.isRequired,
  onSearchData: PropTypes.func,
  onSelectedView: PropTypes.func.isRequired,
  buttonOnClick: PropTypes.func.isRequired,
  optionDefaultFilterBy: PropTypes.string
}

ToolBarMain.defaultProps = {
  optionsFilter: []
}

export default ToolBarMain
