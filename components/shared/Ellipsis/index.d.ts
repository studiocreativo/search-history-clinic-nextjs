const ORIENTATION_VERTICAL = 'vertical'
const ORIENTATION_HORIZONTAL = 'horizontal'

export interface EllipsisProps {
  children: React.ReactNode;
  orientation?: ORIENTATION_VERTICAL | ORIENTATION_HORIZONTAL;
}

export default function Ellipsis(props: EllipsisProps): JSX.Element