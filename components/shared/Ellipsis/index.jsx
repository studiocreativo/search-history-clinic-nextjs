import { forwardRef, useImperativeHandle, useState } from 'react'
import PropTypes from 'prop-types'

import {
  Menu as MuiMenu,
  IconButton as MuiIconButton
} from '@material-ui/core'
import {
  faEllipsisV as IconEllipsisV,
  faEllipsisH as IconEllipsisH
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import styles from '@styles/shared/Card.module.scss'

const ORIENTATION_VERTICAL = 'vertical'
const ORIENTATION_HORIZONTAL = 'horizontal'

const Ellipsis = forwardRef(({
  children,
  orientation
}, ref) => {
  const [anchorEl, setAnchorEl] = useState(null)
  const open = Boolean(anchorEl)
  const isVertical = orientation === ORIENTATION_VERTICAL

  const handleOpen = ({ currentTarget }) => setAnchorEl(currentTarget)
  const handleClose = () => setAnchorEl(null)

  useImperativeHandle(ref, () => ({
    onHandleClose () {
      handleClose()
    }
  }))

  return (
    <>
      <MuiIconButton onClick={handleOpen} className={styles.VerticalMenuItem}>
        {
          isVertical
            ? (<FontAwesomeIcon icon={IconEllipsisV} size="xs" />)
            : (<FontAwesomeIcon icon={IconEllipsisH} size="xs" />)
        }
      </MuiIconButton>
      <MuiMenu
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        PaperProps={{
          elevation: 0
        }}
        transformOrigin={{ horizontal: 'right', vertical: 'top' }}
        anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
        className={styles.MoreMenu}
      >
        {children}
      </MuiMenu>
    </>
  )
})

Ellipsis.displayName = 'Ellipsis'

Ellipsis.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element),
  orientation: PropTypes.oneOf([ORIENTATION_VERTICAL, ORIENTATION_HORIZONTAL])
}

Ellipsis.defaultProps = {
  children: null,
  orientation: ORIENTATION_HORIZONTAL
}

export default Ellipsis
