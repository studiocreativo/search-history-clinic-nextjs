export interface SwitchIconProps {
  initialValue?: string;
  onSelected: (value: string) => void;
}

export default function SwitchIcon(props: SwitchIconProps): JSX.Element