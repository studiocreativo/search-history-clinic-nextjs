import { useState } from 'react'
import PropTypes from 'prop-types'
import clsx from 'clsx'

import { IconButton as MuiIconButton } from '@mui/material'
import { faGripHorizontal, faBars } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import styles from '@styles/shared/SwitchIcon.module.scss'

const SELECT_LIST = 'select-list'
const SELECT_GRID = 'select-grid'
const COLOR_SELECTED_ICON = '#FFF'

const SwitchIcon = ({ initialValue, onSelected }) => {
  const [selected, setSelected] = useState(initialValue)
  const stylesContainer = clsx(
    styles.Container, {
      [styles.GridOn]: selected === SELECT_GRID
    }
  )

  const handleClick = (select) => {
    setSelected(select)
    onSelected(select)
  }

  return (
    <div className={stylesContainer}>
      <div className={styles.Background} />
      <MuiIconButton size="small" onClick={() => handleClick(SELECT_LIST)} className={styles.ContainerIcon} color="primary">
        <FontAwesomeIcon icon={faBars} color={selected === SELECT_LIST ? COLOR_SELECTED_ICON : ''} />
      </MuiIconButton>
      <MuiIconButton size="small" onClick={() => handleClick(SELECT_GRID)} className={styles.ContainerIcon} color="primary">
        <FontAwesomeIcon icon={faGripHorizontal} color={selected === SELECT_GRID ? COLOR_SELECTED_ICON : ''} />
      </MuiIconButton>
    </div>
  )
}

SwitchIcon.propTypes = {
  initialValue: PropTypes.string,
  onSelected: PropTypes.func.isRequired
}

SwitchIcon.defaultProps = {
  initialValue: SELECT_LIST
}

export default SwitchIcon
