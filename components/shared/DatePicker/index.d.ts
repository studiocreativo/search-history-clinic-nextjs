import { DatePickerProps as MuiDatePickerProps } from '@material-ui/lab'

export interface DatePickerProps extends Omit<MuiDatePickerProps, 'renderInput'> {}

export default function DatePicker(props: DatePickerProps): JSX.Element