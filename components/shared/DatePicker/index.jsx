import PropTypes from 'prop-types'
import AdapterDateFns from '@material-ui/lab/AdapterDateFns'
import { es } from 'date-fns/locale'

import { TextField as MuiTextField } from '@mui/material'
import {
  DatePicker as MuiDatePicker,
  LocalizationProvider as MuiLocalizationProvider
} from '@material-ui/lab'

const DatePicker = ({
  helperText,
  ...otherProps
}) => (
  <MuiLocalizationProvider dateAdapter={AdapterDateFns} locale={es}>
    <MuiDatePicker
      {...otherProps}
      renderInput={(params) => (
        <MuiTextField
          {...params}
          helperText={helperText}
          inputProps={{
            ...params.inputProps,
            placeholder: 'dd/mm/yyyy'
          }}
          fullWidth
        />
      )}
    />
  </MuiLocalizationProvider>
)

DatePicker.propTypes = {
  helperText: PropTypes.string
}

export default DatePicker
