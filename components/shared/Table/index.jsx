import { useState } from 'react'
import PropTypes from 'prop-types'
import Box from '@material-ui/core/Box'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TablePagination from '@material-ui/core/TablePagination'
import TableRow from '@material-ui/core/TableRow'
import TableSortLabel from '@material-ui/core/TableSortLabel'
import Paper from '@material-ui/core/Paper'
import Checkbox from '@material-ui/core/Checkbox'
import styles from '@styles/pages/doctor/LisDoctor.module.scss'

function descendingComparator (a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1
  }
  if (b[orderBy] > a[orderBy]) {
    return 1
  }
  return 0
}

function getComparator (order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy)
}

function TableListHeader ({
  onSelectAllClick,
  order,
  orderBy,
  numSelected,
  rowCount,
  tableHeadCell,
  onRequestSort
}) {
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property)
  }

  return (
    <TableHead className={styles.TableHead}>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            color="primary"
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{
              'aria-label': 'select all desserts'
            }}
          />
        </TableCell>
        {
          tableHeadCell.map((headCell) => (
            <TableCell
              key={headCell.id}
              align={headCell.numeric ? 'right' : 'left'}
              padding={headCell.disablePadding ? 'none' : 'normal'}
              sortDirection={orderBy === headCell.id ? order : false}
            >
              <TableSortLabel
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : 'asc'}
                onClick={createSortHandler(headCell.id)}
              >
                {headCell.label}
              </TableSortLabel>
            </TableCell>
          ))
        }
      </TableRow>
    </TableHead>
  )
}

TableListHeader.propTypes = {
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  numSelected: PropTypes.number.isRequired,
  rowCount: PropTypes.number.isRequired,
  tableHeadCell: PropTypes.array.isRequired,
  onRequestSort: PropTypes.func.isRequired
}

const TableList = (props) => {
  const [order, setOrder] = useState('asc')
  const [orderBy, setOrderBy] = useState('names')
  const [selected, setSelected] = useState([])
  const [page, setPage] = useState(0)
  const [dense] = useState(false)
  const [rowsPerPage, setRowsPerPage] = useState(5)

  const {
    // propierties
    data,
    tableHeadCell,
    tableRowCell: TableRowCell,
    // methods
    handleEditPatient,
    methods
  } = props

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc'
    setOrder(isAsc ? 'desc' : 'asc')
    setOrderBy(property)
  }

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = data.map((n) => n.id)
      setSelected(newSelecteds)
      return
    }
    setSelected([])
  }

  const handleClick = (event, id) => {
    const selectedIndex = selected.indexOf(id)
    let newSelected = []

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id)
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1))
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1))
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      )
    }

    setSelected(newSelected)
  }

  const handleChangePage = (event, newPage) => {
    setPage(newPage)
  }

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0)
  }

  const isSelected = (id) => selected.indexOf(id) !== -1

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - data.length) : 0

  return (
    <Box sx={{ width: '100%' }}>
      <Paper sx={{
        width: '100%', mb: 2, background: 'transparent', boxShadow: 'none'
      }}
      >
        <TableContainer className={styles.TableContainer}>
          <Table
            sx={{ minWidth: 750 }}
            aria-labelledby="tableTitle"
            size="large"
            className={styles.Table}
          >
            <TableListHeader
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={data.length}
              tableHeadCell={tableHeadCell}
            />
            <TableBody className={styles.TableBody}>
              {data.slice().sort(getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.id)
                  const labelId = `uuid-${index}`

                  return (
                    <TableRowCell
                      key={row.id}
                      row={row}
                      isItemSelected={isItemSelected}
                      labelId={labelId}
                      handleClick={handleClick}
                      handleEditPatient={handleEditPatient}
                      handleDeletePatient={methods.handleDeletePatient}
                      methods={methods}
                    />
                  )
                })}
              {emptyRows > 0 && (
                <TableRow
                  style={{
                    height: (dense ? 33 : 53) * emptyRows
                  }}
                >
                  <TableCell colSpan={12} style={{ textAlign: 'center' }}>
                    No se encontraron más datos.
                  </TableCell>
                </TableRow>
              )}
              {
                data.length === 0 && (
                  <TableRow>
                    <TableCell colSpan={12} style={{ textAlign: 'center' }}>
                      No hay datos.
                    </TableCell>
                  </TableRow>
                )
              }
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </Box>
  )
}

TableList.propTypes = {
  data: PropTypes.array.isRequired,
  tableHeadCell: PropTypes.arrayOf(PropTypes.object).isRequired,
  tableRowCell: PropTypes.func.isRequired,
  handleEditPatient: PropTypes.func,
  methods: PropTypes.object.isRequired
}

TableList.defaultProps = {
  tableHeadCell: [],
  methods: {}
}

/* TableList.defaultProps = {
  tableHeaderCell: [],
  rows: [],
  tableCell: null,
  methods: {}
} */

export default TableList
