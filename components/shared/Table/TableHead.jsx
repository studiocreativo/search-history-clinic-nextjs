import PropTypes from 'prop-types'

import {
  TableCell as MuiTableCell,
  TableHead as MuiTableHead,
  TableRow as MuiTableRow,
  TableSortLabel as MuiTableSortLabel,
  Checkbox as MuiCheckbox
} from '@mui/material'

import styles from '@styles/components/Table.module.scss'

const TableHeader = ({
  tableHeadCell,
  onSelectAllClick,
  order,
  orderBy,
  numSelected,
  rowCount,
  onRequestSort
}) => {
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property)
  }

  return (
    <MuiTableHead className={styles.TableHead}>
      <MuiTableRow>
        <MuiTableCell padding="checkbox">
          <MuiCheckbox
            color="primary"
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{
              'aria-label': 'select all desserts'
            }}
          />
        </MuiTableCell>
        {tableHeadCell.map((headCell) => (
          <MuiTableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'normal'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <MuiTableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
            </MuiTableSortLabel>
          </MuiTableCell>
        ))}
      </MuiTableRow>
    </MuiTableHead>
  )
}

TableHeader.propTypes = {
  tableHeadCell: PropTypes.array.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  numSelected: PropTypes.number.isRequired,
  rowCount: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired
}

export default TableHeader
