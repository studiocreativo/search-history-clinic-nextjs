import {
  Snackbar,
  Paper,
  Stack,
  IconButton
} from '@material-ui/core'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faExclamationTriangle,
  faTimesCircle,
  faTimes,
  faCheckCircle
} from '@fortawesome/free-solid-svg-icons'
import styles from '@styles/shared/Toast.module.scss'

const Toast = (props) => {
  const {
    children,
    type,
    textContent,
    open,
    vertical,
    horizontal,
    onClose
  } = props

  const getIconType = () => {
    switch (type) {
      case 'primary':
        return faCheckCircle
      case 'secondary':
        return faCheckCircle
      case 'success':
        return faCheckCircle
      case 'warning':
        return faExclamationTriangle
      case 'danger':
        return faTimesCircle
      case 'black':
        return faCheckCircle
      default:
        return faCheckCircle
    }
  }

  return (
    <Snackbar
      open={open}
      onClose={onClose}
      autoHideDuration={6000}
      anchorOrigin={{ vertical, horizontal }}
      className={styles.Toast}
    >
      <Paper>
        <Stack
          direction="row"
          spacing={2}
          alignItems="center"
          justifyContent="center"
          className={`toast toast-${type}`}
        >
          <FontAwesomeIcon icon={getIconType()} size="lg" />
          {children}
          {textContent && (<p>{textContent}</p>)}
          <IconButton onClick={onClose}>
            <FontAwesomeIcon icon={faTimes} size="xs" />
          </IconButton>
        </Stack>
      </Paper>
    </Snackbar>
  )
}

Toast.propTypes = {
  children: PropTypes.node,
  type: PropTypes.string,
  textContent: PropTypes.string,
  open: PropTypes.bool,
  vertical: PropTypes.string,
  horizontal: PropTypes.string,
  onClose: PropTypes.func
}

Toast.defaultProps = {
  children: null,
  textContent: '',
  vertical: 'top',
  horizontal: 'right',
  onClose: null
}

export default Toast
