import { useState } from 'react'

import TabPanel from './TabPanel'

import {
  Tabs as MuiTabs,
  Tab as MuiTab,
  Box as MuiBox
} from '@mui/material'

const a11yProps = (index) => ({
  id: `simple-tab-${index}`,
  'aria-controls': `simple-tabpanel-${index}`
})

const BasicTabs = () => {
  const [value, setValue] = useState(0)

  const handleChange = (_, newValue) => {
    setValue(newValue)
  }

  return (
    <MuiBox sx={{ width: '100%' }}>
      <MuiBox sx={{ borderBottom: 1, borderColor: 'divider' }}>
        <MuiTabs value={value} onChange={handleChange} aria-label="basic tabs example">
          <MuiTab label="Item One" {...a11yProps(0)} />
          <MuiTab label="Item Two" {...a11yProps(1)} />
          <MuiTab label="Item Three" {...a11yProps(2)} />
        </MuiTabs>
      </MuiBox>
      <TabPanel value={value} index={0}>
        Item One
      </TabPanel>
      <TabPanel value={value} index={1}>
        Item Two
      </TabPanel>
      <TabPanel value={value} index={2}>
        Item Three
      </TabPanel>
    </MuiBox>
  )
}

export default BasicTabs
