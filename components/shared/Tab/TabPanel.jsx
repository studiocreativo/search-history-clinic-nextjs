import PropTypes from 'prop-types'

import {
  Box as MuiBox,
  Typography as MuiTypography
} from '@mui/material'

const TabPanel = ({
  children,
  value,
  index,
  ...otherProps
}) => (
  <div
    role="tabpanel"
    hidden={value !== index}
    id={`simple-tabpanel-${index}`}
    aria-labelledby={`simple-tab-${index}`}
    {...otherProps}
  >
    {value === index && (
      <MuiBox sx={{ p: 3 }}>
        <MuiTypography>{children}</MuiTypography>
      </MuiBox>
    )}
  </div>
)

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired
}

export default TabPanel
