import { useContext, useState } from 'react'

import {
  Avatar,
  Grid,
  TextField as MuiTextField,
  Box,
  MenuItem
} from '@material-ui/core'

import Modal from '@components/shared/Modal'
import TextField from '@components/shared/TextField'
import AutoComplete from '@components/shared/AutoComplete'
import DatePicker from '@components/shared/DatePicker'
import FeaturesMeetings from '@features/meetings'
import Toast from '@components/shared/Toast'
import InputTags from '@components/shared/InputTags'

import { usePatients } from '../../context'
import MeetingContext, { initState } from '../../context/meeting/meetingContext'

import styles from '@styles/pages/meeting/ModalAddMetting.module.scss'

const ModalMeetings = () => {
  const patients = usePatients()
  const meeting = useContext(MeetingContext)

  /** ********* Toast *********** */
  const [toastOpen, setToastOpen] = useState({
    open: false,
    message: null,
    type: 'success'
  })

  const handleCloseToast = () => {
    setToastOpen({ ...toastOpen, open: false })
  }

  const onSelectedTags = (items) => {
    meeting.setMeeting({ ...meeting, ailments: items })
  }

  const addMeeting = () => {
    FeaturesMeetings.addMeeting(meeting, () => {
      meeting.setMeeting({ ...meeting, open: false })
      setToastOpen({
        open: true,
        type: 'success',
        message: 'Cita reservada satisfactoriamente'
      })
    }, () => {
      setToastOpen({
        open: true,
        type: 'danger',
        message: 'Error! no se ha podido crear la cita.'
      })
    })
  }

  const updateMeeting = () => {
    FeaturesMeetings.updateMeeting(meeting, () => {
      meeting.setMeeting({ ...meeting, open: false })
      setToastOpen({
        open: true,
        type: 'success',
        message: 'Cita actualizada satisfactoriamente'
      })
    }, () => {
      setToastOpen({
        open: true,
        type: 'danger',
        message: 'Error! no se ha podido actualizar la cita.'
      })
    })
  }

  const handleSubmit = () => {
    meeting.edit ? updateMeeting() : addMeeting()
  }

  return (
    <>
      <Modal
        open={meeting.open}
        title={meeting.title}
        maxWidth="xs"
        onSubmit={handleSubmit}
        onClose={() => meeting.setMeeting(initState)}
      >
        <Grid container spacing={2} className={styles.ModalAddMetting}>
          <Grid item xs={12}>
            <AutoComplete
              value={meeting.patient.fullname}
              loading
              options={patients.map((el) => ({ ...el, label: el.fullname }))}
              renderOption={(otherProps, option) => (
                <Box component="li" sx={{ '& > .MuiAvatar-root': { mr: 2, flexShrink: 0 } }} {...otherProps}>
                  <Avatar src={option.photo} alt={option.names} />
                  {option.fullname}
                </Box>
              )}
              renderInput={(params) => (
                <MuiTextField
                  {...params}
                  label="Paciente"
                  inputProps={{ ...params.inputProps }}
                />
              )}
              onChange={(event, newValue) => {
                meeting.setMeeting({ ...meeting, patient: newValue })
              }}
              isOptionEqualToValue={(option) => option.fullname}
            />
          </Grid>
          <Grid item xs={12}>
            <AutoComplete
              value={meeting.doctor}
              options={[]}
              label="Doctor"
              renderInput={(params) => (
                <MuiTextField
                  {...params}
                  label="Doctor"
                  inputProps={{ ...params.inputProps }}
                />
              )}
            />
          </Grid>
          <Grid item xs={12}>
            <DatePicker
              value={meeting.attentionDate}
              onChange={(newValue) => {
                meeting.setMeeting({ ...meeting, attentionDate: newValue })
              }}
              label="Fecha"
            />
          </Grid>
          <Grid item xs={12}>
            <InputTags
              onSelectedTags={onSelectedTags}
              label="Síntomas"
              fullWidth
              tags={meeting.ailments}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              fullWidth
              select
              label="Estado"
              value={meeting.status}
              onChange={(e) => meeting.setMeeting({ ...meeting, status: e.target.value })}
            >
              <MenuItem value="0">
                Ninguno
              </MenuItem>
              <MenuItem value="1">
                En espera
              </MenuItem>
              <MenuItem value="2">
                En proceso
              </MenuItem>
              <MenuItem value="3">
                Completado
              </MenuItem>
              <MenuItem value="4">
                Cancelado
              </MenuItem>
            </TextField>
          </Grid>
        </Grid>
      </Modal>
      <Toast
        open={toastOpen.open || false}
        type={toastOpen.type}
        textContent={toastOpen.message}
        onClose={handleCloseToast}
      />
    </>
  )
}

export default ModalMeetings
