import { useRef } from 'react'
import PropTypes from 'prop-types'

import {
  TableCell,
  MenuItem,
  ListItemIcon,
  Chip
} from '@material-ui/core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faPencilAlt,
  faTrash
} from '@fortawesome/free-solid-svg-icons'
import TableRow from '@components/shared/Table/TableRow'
import Ellipsis from '@components/shared/Ellipsis'

import dayjs from '../../utils/dayjs'
import { statusCode } from '../../helpers/meetings'

const MeetingsTableRowCell = (props) => {
  const ellipsisRef = useRef() // Ellipsis reference

  const {
    row,
    handleClick,
    isItemSelected,
    labelId,
    methods
  } = props

  /** ********* Meeting actions *********** */
  const updateMeeting = (row) => {
    methods.updateMeeting(row)
    ellipsisRef.current.onHandleClose()
  }

  const deleteMeeting = (row) => {
    methods.deleteMeeting(row)
    ellipsisRef.current.onHandleClose()
  }

  return (
    <>
      <TableRow
        row={row}
        handleClick={handleClick}
        labelId={labelId}
        isItemSelected={isItemSelected}
      >

        <TableCell align="right">
          {row.patient.fullname}
        </TableCell>
        <TableCell align="right">
          {row.doctor ? row.doctor.shortname : undefined}
        </TableCell>
        <TableCell align="right">
          {dayjs(row.attentionDate).add(1, 'day').format('DD/MM/YYYY')}
        </TableCell>
        <TableCell align="right">
          <Chip label={statusCode(row.status).text} color={statusCode(row.status).color} size="medium" />
        </TableCell>
        <TableCell align="right">
          {
            row.ailments.map((el) => (
              <Chip key={el} label={el}></Chip>
            ))
          }

        </TableCell>
        <TableCell align="right">
          <Ellipsis ref={ellipsisRef}>
            <MenuItem onClick={() => updateMeeting(row)}>
              <ListItemIcon>
                <FontAwesomeIcon icon={faPencilAlt} />
              </ListItemIcon>
              Editar
            </MenuItem>
            <MenuItem onClick={() => deleteMeeting(row)}>
              <ListItemIcon>
                <FontAwesomeIcon icon={faTrash} />
              </ListItemIcon>
              Eliminar
            </MenuItem>
          </Ellipsis>
        </TableCell>
      </TableRow>
    </>
  )
}

MeetingsTableRowCell.propTypes = {
  row: PropTypes.object.isRequired,
  handleClick: PropTypes.func.isRequired,
  isItemSelected: PropTypes.bool.isRequired,
  labelId: PropTypes.string.isRequired,
  methods: PropTypes.object
}

MeetingsTableRowCell.defaultProps = {
  methods: {}
}

export default MeetingsTableRowCell
