import Image from 'next/image'
import {
  MenuItem,
  IconButton,
  Box
} from '@material-ui/core'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faUser,
  faPhoneAlt,
  faEnvelope,
  faAddressCard,
  faCamera
} from '@fortawesome/free-solid-svg-icons'
import Modal from '@components/shared/Modal'
import TextField from '@components/shared/TextField'
import styles from '@styles/pages/doctor/AddDoctor.module.scss'

const AddDoctor = (props) => {
  const {
    open,
    handleCloseModal,
    doctor,
    disabled
  } = props

  return (
    <Modal open={open} handleCloseModal={handleCloseModal} title="Añadir Doctor">
      <Box component="div" className={styles.Avatar}>
        <Box component="div">
          <Image
            src="https://thumbs.dreamstime.com/z/unknown-male-avatar-profile-image-businessman-vector-unknown-male-avatar-profile-image-businessman-vector-profile-179373829.jpg"
            alt="hola"
            layout="fill"
            objectFit="cover"
          />
          <Box component="div">
            <TextField type="file" />
            <IconButton>
              <FontAwesomeIcon icon={faCamera} size="xs" />
            </IconButton>
          </Box>
        </Box>
      </Box>
      <TextField
        label="nombres"
        fullWidth
        autoFocus
        value={doctor.names}
        disabled={disabled}
        InputAdornmentStart={<FontAwesomeIcon icon={faUser} />}
        className={styles.Input}
      />
      <TextField
        label="Especialidad"
        fullWidth
        value={doctor.especiality}
        select
        disabled={disabled}
        className={styles.Input}
      >
        <MenuItem value="none">
          Urologo
        </MenuItem>
      </TextField>
      <TextField
        value={doctor.phone}
        label="N° de celular"
        type="tel"
        fullWidth
        disabled={disabled}
        InputAdornmentStart={<FontAwesomeIcon icon={faPhoneAlt} />}
        className={styles.Input}
      />
      <TextField
        value={doctor.email}
        label="Correo"
        type="email"
        fullWidth
        disabled={disabled}
        InputAdornmentStart={<FontAwesomeIcon icon={faEnvelope} />}
        className={styles.Input}
      />
      <TextField
        value={doctor.identity}
        label="DNI"
        type="text"
        fullWidth
        InputAdornmentStart={<FontAwesomeIcon icon={faAddressCard} />}
        disabled={disabled}
        className={styles.Input}
      />
    </Modal>
  )
}

AddDoctor.propTypes = {
  open: PropTypes.bool.isRequired,
  handleCloseModal: PropTypes.func.isRequired,
  doctor: PropTypes.object,
  disabled: PropTypes.bool
}

AddDoctor.defaultProps = {
  doctor: null,
  disabled: false
}

export default AddDoctor
