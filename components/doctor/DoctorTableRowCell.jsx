import {
  Grid,
  TableCell,
  Avatar,
  MenuItem,
  ListItemIcon
} from '@material-ui/core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faUserEdit,
  faTrash
} from '@fortawesome/free-solid-svg-icons'
import TableRow from '@components/shared/Table/TableRow'
import Ellipsis from '@components/shared/Ellipsis'
import PropTypes from 'prop-types'

const DoctorTableRowCell = (props) => {
  const {
    row,
    handleClick,
    isItemSelected,
    labelId
  } = props

  return (
    <TableRow
      row={row}
      handleClick={handleClick}
      labelId={labelId}
      isItemSelected={isItemSelected}
    >
      <TableCell
        component="th"
        id={labelId}
        scope="row"
        padding="none"
      >
        <Grid container spacing={1} alignItems="center">
          <Grid item lg={2}>
            <Avatar alt="asd" />
          </Grid>
          <Grid item lg={10}>
            {row.names}
          </Grid>
        </Grid>
      </TableCell>
      <TableCell align="right">{row.calories}</TableCell>
      <TableCell align="right">{row.fat}</TableCell>
      <TableCell align="right">{row.carbs}</TableCell>
      <TableCell align="right">
        <Ellipsis>
          <MenuItem>
            <ListItemIcon>
              <FontAwesomeIcon icon={faUserEdit} />
            </ListItemIcon>
            Editar
          </MenuItem>
          <MenuItem>
            <ListItemIcon>
              <FontAwesomeIcon icon={faTrash} />
            </ListItemIcon>
            Eliminar
          </MenuItem>
        </Ellipsis>
      </TableCell>
    </TableRow>
  )
}

DoctorTableRowCell.propTypes = {
  row: PropTypes.object.isRequired,
  handleClick: PropTypes.func.isRequired,
  isItemSelected: PropTypes.bool.isRequired,
  labelId: PropTypes.string.isRequired
}

DoctorTableRowCell.defaultProps = {}

export default DoctorTableRowCell
