import { useState } from 'react'
import {
  IconButton,
  Typography,
  Grid,
  TablePagination
} from '@material-ui/core'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faComment,
  faEye
} from '@fortawesome/free-solid-svg-icons'
import Card from '@components/shared/CardUser'

const DoctorGridView = (props) => {
  const {
    data,
    handleEditModal,
    handleViewModal
  } = props

  const [rowsPerPage, setRowsPerPage] = useState(8)
  const [page, setPage] = useState(0)

  /** ********* Set new page *********** */
  const handleChangePage = (event, newPage) => {
    setPage(newPage)
  }

  /** ********* Listening changes in row per page *********** */
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0)
  }

  return (
    <>
      <Grid container spacing={2}>
        {
          data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((el) => (
            <Grid item key={el.id} xs={12} sm={6} md={4} lg={3}>
              <Card handleEditModal={handleEditModal} data={el}>
                <Typography variant="h5" align="center">
                  {el.names}
                </Typography>
                <Typography variant="caption" display="block" color="text.primary">
                  {el.especiality}
                </Typography>
                <Typography variant="body2" align="center">
                  <IconButton
                    onClick={() => handleViewModal(el.id)}
                    className="btn-view"
                  >
                    <FontAwesomeIcon icon={faEye} size="xs" color="primary" />
                  </IconButton>
                  <IconButton className="btn-msg">
                    <FontAwesomeIcon icon={faComment} size="xs" />
                  </IconButton>
                </Typography>
              </Card>
            </Grid>
          ))
        }
      </Grid>
      <Grid
        container
        direction="row"
        justifyContent="flex-end"
        alignItems="center"
      >
        <TablePagination
          rowsPerPageOptions={[8, 16, 32]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
          showFirstButton
          showLastButton
        />
      </Grid>
    </>
  )
}

DoctorGridView.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  handleEditModal: PropTypes.func,
  handleViewModal: PropTypes.func
}

DoctorGridView.defaultProps = {
  handleEditModal: null,
  handleViewModal: null
}

export default DoctorGridView
