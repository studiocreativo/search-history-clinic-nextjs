import { useState, useRef, useContext } from 'react'
import PropTypes from 'prop-types'

import {
  IconButton,
  Typography,
  Grid,
  TablePagination,
  MenuItem,
  ListItemIcon
} from '@material-ui/core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faComment,
  faEye,
  faPencilAlt,
  faTrash,
  faCalendar
} from '@fortawesome/free-solid-svg-icons'

import Card from '@components/shared/CardUser'
import Ellipsis from '@components/shared/Ellipsis'
import MeetingContext, { initState } from '../../context/meeting/meetingContext'

const PatientGridView = ({
  data,
  onViewModal,
  onEditPatient,
  onDeletePatient
}) => {
  const childRef = useRef() // Ellipsis reference

  const [rowsPerPage, setRowsPerPage] = useState(8)
  const [page, setPage] = useState(0)

  const { setMeeting } = useContext(MeetingContext)

  /** ********* Set new page *********** */
  const handleChangePage = (_, newPage) => {
    setPage(newPage)
  }

  /** ********* Listening changes in row per page *********** */
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0)
  }

  const scheduleAppointment = (row) => {
    setMeeting({ ...initState, open: true, patient: row })
    childRef.current.onHandleClose()
  }

  const editPatient = (row) => {
    onEditPatient(row)
    childRef.current.onHandleClose()
  }
  const deletePatient = (row) => {
    onDeletePatient(row)
    childRef.current.onHandleClose()
  }

  return (
    <>
      <Grid container spacing={2}>
        {
          data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((el) => (
            <Grid item key={el.id} xs={12} sm={6} md={4} lg={3}>
              <Card
                cardActionItems={
                  <Ellipsis ref={childRef} orientation="vertical">
                    <MenuItem onClick={() => scheduleAppointment(el)}>
                      <ListItemIcon>
                        <FontAwesomeIcon icon={faCalendar} />
                      </ListItemIcon>
                      Agendar Cita
                    </MenuItem>
                    <MenuItem onClick={() => editPatient(el)}>
                      <ListItemIcon>
                        <FontAwesomeIcon icon={faPencilAlt} />
                      </ListItemIcon>
                      Editar
                    </MenuItem>
                    <MenuItem onClick={() => deletePatient(el)}>
                      <ListItemIcon>
                        <FontAwesomeIcon icon={faTrash} />
                      </ListItemIcon>
                      Eliminar
                    </MenuItem>
                  </Ellipsis>
                }
                user={el}
              >
                <Typography variant="h5" align="center">
                  {el.names}
                </Typography>
                <Typography variant="caption" display="block" color="text.secondary">
                  {el.firtsSurname}
                  {' '}
                  {el.secondSurname}
                </Typography>
                <Typography variant="caption" display="block" color="text.primary">
                  DNI:
                  {el.dni}
                </Typography>
                <Typography variant="body2" align="center">
                  <IconButton
                    onClick={() => onViewModal(el)}
                    className="btn-view"
                  >
                    <FontAwesomeIcon icon={faEye} size="xs" color="primary" />
                  </IconButton>
                  <IconButton className="btn-msg">
                    <FontAwesomeIcon icon={faComment} size="xs" />
                  </IconButton>
                </Typography>
              </Card>
            </Grid>
          ))
        }
      </Grid>
      <Grid
        container
        direction="row"
        justifyContent="flex-end"
        alignItems="center"
      >
        <TablePagination
          rowsPerPageOptions={[8, 16, 32]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
          showFirstButton
          showLastButton
        />
      </Grid>
    </>
  )
}

PatientGridView.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  onViewModal: PropTypes.func,
  onEditPatient: PropTypes.func,
  onDeletePatient: PropTypes.func.isRequired
}

PatientGridView.defaultProps = {
  onEditPatient: null,
  onViewModal: null
}

export default PatientGridView
