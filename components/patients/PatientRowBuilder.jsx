import PropTypes from 'prop-types'
import dayjs from 'dayjs'
import { colorizedAge } from '@utils/indicators'

import {
  TableCell as MuiTableCell,
  Grid as MuiGrid,
  Avatar as MuiAvatar,
  Chip as MuiChip,
  Typography as MuiTypography,
  Stack as MuiStack,
  IconButton as MuiIconButton
} from '@mui/material'
import {
  faPencilAlt,
  faTrash,
  faFileAlt,
  faIdCard,
  faCalendar
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import TableRow from '@components/shared/TableStick/TableRow'

const PatientRowBuilder = ({
  dataRow,
  selectedProps,
  onAddMeeting,
  onEditPatient,
  onDeletePatient
}) => {
  return (
    <TableRow
      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
      selectedProps={selectedProps}
      // onClick={(event) => handleClickRow(event, dataRow)}
      hover
    >
      <MuiTableCell component="th" scope="row">
        <MuiGrid container spacing={1} alignItems="center">
          <MuiGrid item lg={2}>
              <MuiAvatar
                src={dataRow.photo}
                alt={`Foto ${dataRow.shortname}`}
                variant="rounded"
                sx={{ width: 30, height: 30 }}
              />
          </MuiGrid>
          <MuiGrid item lg={10}>
            {dataRow.names}
            <MuiTypography variant="caption" display="block">
              {dataRow.firtsSurname}
              {' '}
              {dataRow.secondSurname}
            </MuiTypography>
          </MuiGrid>
        </MuiGrid>
      </MuiTableCell>
      <MuiTableCell align="left">
        <FontAwesomeIcon icon={faFileAlt} />
        {' '}
        {dataRow.numberHistory}
      </MuiTableCell>
      <MuiTableCell align="left">
        <FontAwesomeIcon icon={faIdCard} />
        {' '}
        {dataRow.dni}
      </MuiTableCell>
      <MuiTableCell align="left">{dataRow.gender === 'Masculino' ? 'M' : 'F' }</MuiTableCell>
      <MuiTableCell align="left">
        <MuiChip
          label={dataRow.age}
          color={colorizedAge(dataRow.age)}
          size="small" />
      </MuiTableCell>
      <MuiTableCell align="left">
        {dayjs(dataRow.dateOfBirth).add(1, 'day').format('DD/MM/YYYY')}
      </MuiTableCell>
      <MuiTableCell align="left">
        <MuiStack direction="row" spacing={1}>
          <MuiIconButton onClick={() => onAddMeeting(dataRow)}>
            <FontAwesomeIcon
              icon={faCalendar}
              size="xs"
            />
          </MuiIconButton>
          <MuiIconButton onClick={() => onEditPatient(dataRow)}>
            <FontAwesomeIcon
              icon={faPencilAlt}
              size="xs"
            />
          </MuiIconButton>
          <MuiIconButton onClick={() => onDeletePatient(dataRow)}>
            <FontAwesomeIcon
              icon={faTrash}
              size="xs"
            />
          </MuiIconButton>
        </MuiStack>
      </MuiTableCell>
    </TableRow>
  )
}

PatientRowBuilder.propTypes = {
  dataRow: PropTypes.object.isRequired,
  selectedProps: PropTypes.object,
  onAddMeeting: PropTypes.func.isRequired,
  onEditPatient: PropTypes.func.isRequired,
  onDeletePatient: PropTypes.func.isRequired
}

export default PatientRowBuilder
