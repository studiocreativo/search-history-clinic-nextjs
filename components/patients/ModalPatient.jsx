import Image from 'next/image'
import PropTypes from 'prop-types'
import {
  Grid,
  Box,
  IconButton,
  Autocomplete,
  TextField as MuiTextField,
  MenuItem
} from '@mui/material'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faEnvelope,
  faIdCard,
  faUser,
  faCamera,
  faSearch
} from '@fortawesome/free-solid-svg-icons'
import Modal from '@components/shared/Modal'
import TextField from '@components/shared/TextField'
// import Tab from '@components/shared/Tab';
import Button from '@components/shared/Button'
import DatePicker from '@components/shared/DatePicker'
import styles from '@styles/pages/doctor/AddDoctor.module.scss'
import { useState } from 'react'
import { validateDNI } from '../../helpers/patients'
import departments from '../../utils/departments.json'
import objProvinces from '../../utils/provinces.json'
import objDistricts from '../../utils/districts.json'
import InputTags from '@components/shared/InputTags'

const ModalPatient = (props) => {
  const {
    open,
    dataModal,
    patient,
    // Methods
    handleCloseModal,
    handleSearchPatient,
    handleSubmitModal,
    setPatient
  } = props

  const [provinces, setProvinces] = useState([])
  const [districts, setDistricts] = useState([])

  const onSelectedTags = (items) => {
    console.log(items)
    setPatient({ ...patient, phones: items })
  }

  return (
    <Modal open={open} onSubmit={handleSubmitModal} onClose={handleCloseModal} maxWidth="sm" title={dataModal.title}>
      {/* <Tab /> */}
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Box component="div" className={styles.Avatar}>
            <Box component="div">
              <Image
                src={patient.photo ? patient.photo : '/avatar.jpg'}
                alt={patient.names}
                layout="fill"
                objectFit="cover"
              />
              <Box component="div">
                <TextField type="file" />
                <IconButton>
                  <FontAwesomeIcon icon={faCamera} size="xs" />
                </IconButton>
              </Box>
            </Box>
          </Box>
        </Grid>
        <Grid item xs={12}>
          <TextField
            disabled={dataModal.edit}
            value={patient.dni}
            label="DNI"
            type="text"
            fullWidth
            error={validateDNI(patient.dni).error}
            InputAdornmentStart={<FontAwesomeIcon icon={faIdCard} />}
            helperText={validateDNI(patient.dni).msg}
            InputAdornmentEnd={(
              <Button
                disabled={dataModal.edit}
                onClick={handleSearchPatient}
                startIcon={<FontAwesomeIcon icon={faSearch} />}
              >
                Buscar
              </Button>
            )}
            onChange={(e) => setPatient({ ...patient, dni: e.target.value })}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            disabled={dataModal.inputDisabled}
            value={patient.names}
            label="Nombres"
            type="text"
            fullWidth
            InputAdornmentStart={<FontAwesomeIcon icon={faUser} />}
            onChange={(e) => setPatient({ ...patient, names: e.target.value })}
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            disabled={dataModal.inputDisabled}
            value={patient.firtsSurname}
            label="Apellido paterno"
            type="text"
            fullWidth
            InputAdornmentStart={<FontAwesomeIcon icon={faUser} />}
            onChange={(e) => setPatient({ ...patient, firtsSurname: e.target.value })}
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            disabled={dataModal.inputDisabled}
            value={patient.secondSurname}
            label="Apellido materno"
            type="text"
            fullWidth
            InputAdornmentStart={<FontAwesomeIcon icon={faUser} />}
            onChange={(e) => setPatient({ ...patient, secondSurname: e.target.value })}
          />
        </Grid>
        <Grid item xs={4}>
          <Autocomplete
            value={patient.department}
            options={departments.map((el) => ({ ...el, label: el.name }))}
            disabled={dataModal.inputDisabled}
            renderInput={(params) => (
              <MuiTextField
                {...params}
                label="Departamento"
                inputProps={{ ...params.inputProps }}
              />
            )}
            onChange={(e, newValue) => {
              if (!newValue) {
                // setPatient({ ...patient, department: { name: null } })
                setProvinces([])
                return
              }
              // setPatient({ ...patient, department: newValue })
              const currentProvinces = objProvinces[newValue.id]
              setProvinces(currentProvinces)
            }}
            onInputChange={(e, newValue) => {
              setPatient({ ...patient, department: newValue })
            }}
            isOptionEqualToValue={(option) => option.name}
          />
        </Grid>
        <Grid item xs={4}>
          <Autocomplete
            value={patient.province}
            options={provinces.map((el) => ({ ...el, label: el.name }))}
            disabled={provinces.length === 0}
            renderInput={(params) => (
              <MuiTextField
                {...params}
                label="Provincia"
                inputProps={{ ...params.inputProps }}
              />
            )}
            onChange={(e, newValue) => {
              if (!newValue) {
                // setPatient({ ...patient, province: { name: null } })
                setDistricts([])
                return
              }
              // setPatient({ ...patient, province: newValue })
              const currentDistricts = objDistricts[newValue.id]
              setDistricts(currentDistricts)
            }}
            onInputChange={(e, newValue) => {
              setPatient({ ...patient, province: newValue })
            }}
            isOptionEqualToValue={(option) => option.name}
          />
        </Grid>
        <Grid item xs={4}>
          <Autocomplete
            value={patient.district}
            options={districts.map((el) => ({ ...el, label: el.name }))}
            disabled={districts.length === 0}
            renderInput={(params) => (
              <MuiTextField
                {...params}
                label="Distrito"
                inputProps={{ ...params.inputProps }}
              />
            )}
            onChange={(e, newValue) => {
              /* if (!newValue) {
                setPatient({ ...patient, district: { name: null } })
                return
              }
              setPatient({ ...patient, district: newValue }) */
            }}
            onInputChange={(e, newValue) => {
              setPatient({ ...patient, district: newValue })
            }}
            isOptionEqualToValue={(option) => option.name}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            value={patient.address}
            label="Dirección"
            type="text"
            fullWidth
            disabled={dataModal.inputDisabled}
            onChange={(e) => setPatient({ ...patient, address: e.target.value })}
          />
        </Grid>
        <Grid item xs={6} lg={8}>
          <TextField
            disabled={dataModal.inputDisabled}
            value={patient.email}
            label="Correo"
            type="email"
            fullWidth
            className="input-search"
            InputAdornmentStart={<FontAwesomeIcon icon={faEnvelope} />}
          />
        </Grid>
        <Grid item xs={6} lg={4}>
          <InputTags
            onSelectedTags={onSelectedTags}
            label="Teléfonos"
            fullWidth
            tags={[]}
          />
        </Grid>
        <Grid item xs={4} lg={4}>
          <TextField
            value={patient.gender}
            select
            label="Género"
            fullWidth
            disabled={dataModal.inputDisabled}
            onChange={(e) => setPatient({ ...patient, gender: e.target.value })}
          >
            <MenuItem value="Masculino">Masculino</MenuItem>
            <MenuItem value="Femenino">Femenino</MenuItem>
          </TextField>
        </Grid>
        <Grid item xs={4} lg={4}>
          <TextField
            value={patient.civilStatus}
            select
            label="Estado civil"
            fullWidth
            disabled={dataModal.inputDisabled}
            onChange={(e) => setPatient({ ...patient, civilStatus: e.target.value })}
          >

            <MenuItem value="Soltero">Soltero</MenuItem>
            <MenuItem value="Casado">Casado</MenuItem>
            <MenuItem value="Divorciado">Divorciado</MenuItem>
            <MenuItem value="Viudo">Viudo</MenuItem>
            <MenuItem value="Soltera">Soltera</MenuItem>
            <MenuItem value="Casada">Casada</MenuItem>
            <MenuItem value="Divorciada">Divorciada</MenuItem>
            <MenuItem value="Viuda">Viuda</MenuItem>
          </TextField>
        </Grid>
        <Grid item xs={4} lg={4}>
          <DatePicker
            disabled={dataModal.inputDisabled}
            value={patient.dateOfBirth}
            onChange={(newValue) => setPatient({ ...patient, dateOfBirth: newValue })}
            label="Fecha de Nacimiento"
          />
        </Grid>
      </Grid>
    </Modal>
  )
}

ModalPatient.propTypes = {
  patient: PropTypes.shape({
    dni: PropTypes.string,
    email: PropTypes.string,
    photo: PropTypes.string,
    names: PropTypes.string,
    firtsSurname: PropTypes.string,
    secondSurname: PropTypes.string,
    phones: PropTypes.arrayOf(PropTypes.string),
    gender: PropTypes.string,
    address: PropTypes.string,
    civilStatus: PropTypes.string,
    dateOfBirth: PropTypes.date,
    department: PropTypes.string,
    province: PropTypes.string,
    district: PropTypes.string
  }).isRequired,
  open: PropTypes.bool.isRequired,
  dataModal: PropTypes.object.isRequired,
  handleCloseModal: PropTypes.func.isRequired,
  handleSearchPatient: PropTypes.func.isRequired,
  handleSubmitModal: PropTypes.func,
  setPatient: PropTypes.func.isRequired
}

ModalPatient.defaultProps = {
  handleSubmitModal: () => {}
}

export default ModalPatient
