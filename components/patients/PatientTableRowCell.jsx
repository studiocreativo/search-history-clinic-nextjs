import { useContext, useRef } from 'react'
import PropTypes from 'prop-types'
import {
  Grid,
  TableCell,
  Avatar,
  MenuItem,
  ListItemIcon,
  Typography,
  Tooltip,
  Chip
} from '@material-ui/core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faPencilAlt,
  faTrash,
  faFileAlt,
  faIdCard,
  faCalendar
} from '@fortawesome/free-solid-svg-icons'
import dayjs from 'dayjs'
import TableRow from '@components/shared/Table/TableRow'
import Ellipsis from '@components/shared/Ellipsis'
import { getColorAge } from '../../helpers/age'
import MeetingContext, { initState } from '../../context/meeting/meetingContext'

const PatientTableRowCell = (props) => {
  const childRef = useRef() // Child reference
  const {
    row,
    handleClick,
    isItemSelected,
    labelId,
    // functions
    handleEditPatient,
    handleDeletePatient
  } = props

  const { setMeeting } = useContext(MeetingContext)

  const scheduleAppointment = () => {
    setMeeting({ ...initState, open: true, patient: row })
    childRef.current.onHandleClose()
  }

  const editPatient = () => {
    handleEditPatient(row)
    childRef.current.onHandleClose()
  }

  const deletePatient = () => {
    handleDeletePatient(row.id)
    childRef.current.onHandleClose()
  }

  return (
    <TableRow
      row={row}
      handleClick={handleClick}
      labelId={labelId}
      isItemSelected={isItemSelected}
    >
      <TableCell
        component="th"
        id={labelId}
        scope="row"
        padding="none"
      >
        <Grid container spacing={1} alignItems="center">
          <Grid item lg={2}>
            <Tooltip title={row.fullname} placement="top" arrow>
              <Avatar src={row.photo} alt="asd" variant="rounded" sx={{ width: 30, height: 30 }} />
            </Tooltip>
          </Grid>
          <Grid item lg={10}>
            {row.names}
            <Typography variant="caption" display="block">
              {row.firtsSurname}
              {' '}
              {row.secondSurname}
            </Typography>
          </Grid>
        </Grid>
      </TableCell>
      <TableCell align="right">
        <FontAwesomeIcon icon={faFileAlt} />
        {' '}
        {row.numberHistory}
      </TableCell>
      <TableCell align="right">
        <FontAwesomeIcon icon={faIdCard} />
        {' '}
        {row.dni}
      </TableCell>
      <TableCell align="right">{row.gender === 'Masculino' ? 'M' : 'F' }</TableCell>
      <TableCell align="right">
        <Chip label={row.age} color={getColorAge(row.age)} size="small" />
      </TableCell>
      <TableCell align="right">{dayjs(row.dateOfBirth).add(1, 'day').format('DD/MM/YYYY')}</TableCell>
      <TableCell align="right">
        <Ellipsis ref={childRef}>
          <MenuItem onClick={scheduleAppointment}>
            <ListItemIcon>
              <FontAwesomeIcon icon={faCalendar} />
            </ListItemIcon>
            Agendar Cita
          </MenuItem>
          <MenuItem onClick={editPatient}>
            <ListItemIcon>
              <FontAwesomeIcon icon={faPencilAlt} />
            </ListItemIcon>
            Editar
          </MenuItem>
          <MenuItem onClick={deletePatient}>
            <ListItemIcon>
              <FontAwesomeIcon icon={faTrash} />
            </ListItemIcon>
            Eliminar
          </MenuItem>
        </Ellipsis>
      </TableCell>
    </TableRow>
  )
}

PatientTableRowCell.propTypes = {
  row: PropTypes.object.isRequired,
  handleClick: PropTypes.func.isRequired,
  isItemSelected: PropTypes.bool.isRequired,
  labelId: PropTypes.string.isRequired,
  // functions
  handleEditPatient: PropTypes.func.isRequired,
  handleDeletePatient: PropTypes.func.isRequired
}

export default PatientTableRowCell
