import { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import Ubigeo from '@utils/ubigeo'

import {
  Grid as MuiGrid,
  Box as MuiBox,
  Autocomplete as MuiAutocomplete,
  MenuItem as MuiMenuItem,
  IconButton as MuiIconButton,
  FormGroup as MuiFormGroup,
  FormControlLabel as MuiFormControlLabel,
  Checkbox as MuiCheckbox,
  TextField as MuiTextField
} from '@mui/material'
import Image from 'next/image'
import Modal from '@components/shared/Modal'
import Button from '@components/shared/Button'
import TextField from '@components/shared/TextField'
import InputTags from '@components/shared/InputTags'
import DatePicker from '@components/shared/DatePicker'

import {
  faIdCard,
  faCamera,
  faSearch
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

/**
* * CAMBIAR STYLES
*/
import styles from '@styles/pages/doctor/AddDoctor.module.scss'
import usePrevState from '@hooks/usePrevState'

const AutocompleteRenderInput = (title) => {
  const WrappedTextField = (props) => (
    <MuiTextField
      {...props}
      label={title}
      inputProps={{ ...props.inputProps }}
    />
  )

  WrappedTextField.displayName = 'WrappedTextField'
  WrappedTextField.propTypes = {
    inputProps: PropTypes.object
  }

  return WrappedTextField
}

AutocompleteRenderInput.displayName = 'AutocompleteRenderInput'
AutocompleteRenderInput.propTypes = {
  title: PropTypes.string.isRequired
}

const { departments, getProvinces, getDistricts } = Ubigeo
const initStateLocation = {
  department: null,
  province: null,
  district: null
}

const mapUbigeoToLocation = (ubigeo) => {
  const ubigeoArray = [
    ubigeo?.substring(0, 2),
    ubigeo?.substring(2, 4),
    ubigeo?.substring(4, 6)
  ]
  const department = departments.find(department => department.code === ubigeoArray[0])
  const province = getProvinces(department).find(province => province.code === ubigeoArray[1])
  const district = getDistricts(province).find(district => district.code === ubigeoArray[2])

  return {
    department: { ...department, label: department.name },
    province: { ...province, label: province.name },
    district: { ...district, label: district.name }
  }
}

const PatientModal = ({
  edit,
  open,
  onClose,
  patient: patientData
}) => {
  const [patient, setPatient] = useState(patientData)
  const [location, setLocation] = useState(initStateLocation)
  const prevLocation = usePrevState(location)
  const [manualEntry, setManualEntry] = useState(false)

  useEffect(() => {
    console.log('RENDER')
    setPatient(patientData)

    if (edit) {
      const mapLocation = mapUbigeoToLocation(patientData.ubigeo[2])
      setLocation(mapLocation)
    }
  }, [edit, patientData])

  useEffect(() => {
    const { department, province, district } = location

    if (!department && province) {
      setLocation({
        ...location,
        province: null,
        district: null
      })
    }

    if (department && !province && district) {
      setLocation({
        ...location,
        district: null
      })
    }
  }, [location])

  useEffect(() => {
    const { department, province } = location

    if (!prevLocation?.department && !department && department !== prevLocation?.department) {
      console.log('DEPARTMENT')
      setLocation({
        ...location,
        province: null,
        district: null
      })
    }

    if (!prevLocation?.province && !province && province !== prevLocation?.province) {
      console.log('PROVINCE')
      setLocation({
        ...location,
        district: null
      })
    }
  }, [location, prevLocation])

  const handleSubmit = () => {
    console.log('submit')
  }

  const handlePatientChangeData = ({ target: { value, name } }) => setPatient({ ...patient, [name]: value })
  const handlePatientChangePhones = (data) => setPatient({ ...patient, phones: data })
  const handlePatientChangeDateOfBirth = (date) => setPatient({ ...patient, dateOfBirth: date })

  const handleLocationChange = (property) => (_, value) => {
    setLocation({ ...location, [property]: value })
  }

  const handleSearchDni = () => {
    fetch(`http://localhost:3000/api/dni/${patient.dni}`)
      .then((res) => res.json())
      .then((data) => {
        if (data.success) {
          setPatient({
            ...patient,
            ...data.user,
            email: `${data.user.dni}@tudoctornet.com`
          })
          const mapLocation = mapUbigeoToLocation(data.user.ubigeo)
          setLocation(mapLocation)
        }
      })
  }

  const handleClose = () => {
    setLocation(initStateLocation)
    onClose()
  }

  const SearchDniAdornmentStart = (<FontAwesomeIcon icon={faIdCard} />)
  const SearchDniAdornmentEnd = (
    <Button
      disabled={edit || manualEntry}
      onClick={handleSearchDni}
      startIcon={<FontAwesomeIcon icon={faSearch} />}
    >
      Buscar
    </Button>
  )

  return (
    <Modal
      title={edit ? 'Editar Paciente' : 'Nuevo Paciente'}
      open={open}
      onClose={handleClose}
      onSubmit={handleSubmit}
      maxWidth="md"
    >
      <MuiGrid container spacing={2}>
        <MuiGrid item xs={12}>
          <MuiBox className={styles.Avatar}>
            <MuiBox>
              <Image
                src={patient.photo ? patient.photo : '/avatar.jpg'}
                alt={patient.shortname}
                layout="fill"
                objectFit="cover"
              />
              <MuiBox>
                <TextField type="file" />
                <MuiIconButton>
                  <FontAwesomeIcon icon={faCamera} size="xs" />
                </MuiIconButton>
              </MuiBox>
            </MuiBox>
          </MuiBox>
        </MuiGrid>
        <MuiGrid item xs={12}>
          <MuiFormGroup>
            <MuiFormControlLabel
              label="Ingreso Manual"
              labelPlacement='start'
              control={
                <MuiCheckbox
                  checked={manualEntry}
                  onChange={({ target: { checked } }) => setManualEntry(checked)}
                />
              }
            />
          </MuiFormGroup>
        </MuiGrid>
        <MuiGrid item xs={12}>
          <TextField
            disabled={edit}
            label="DNI"
            name="dni"
            maxLength={8}
            value={patient.dni}
            onChange={handlePatientChangeData}
            fullWidth
            InputAdornmentStart={SearchDniAdornmentStart}
            InputAdornmentEnd={SearchDniAdornmentEnd}
          />
        </MuiGrid>
        {
          !manualEntry
            ? (
                <MuiGrid item xs={12}>
                  <TextField
                    disabled={edit || !manualEntry}
                    label="Nombre Completo"
                    name="fullname"
                    value={patient.fullname}
                    onChange={handlePatientChangeData}
                    fullWidth
                  />
                </MuiGrid>
              )
            : (
              <>
                <MuiGrid item xs={12}>
                  <TextField
                    disabled={edit || !manualEntry}
                    label="Nombres"
                    name="names"
                    value={patient.names}
                    onChange={handlePatientChangeData}
                    fullWidth
                  />
                </MuiGrid>
                <MuiGrid item xs={6}>
                  <TextField
                    disabled={edit || !manualEntry}
                    label="Apellido Paterno"
                    name="firtsSurname"
                    value={patient.firtsSurname}
                    onChange={handlePatientChangeData}
                    fullWidth
                  />
                </MuiGrid><MuiGrid item xs={6}>
                  <TextField
                    disabled={edit || !manualEntry}
                    label="Apellido Materno"
                    name="secondSurname"
                    value={patient.secondSurname}
                    onChange={handlePatientChangeData}
                    fullWidth
                  />
                </MuiGrid>
              </>
              )
        }
        <MuiGrid item xs={4}>
          <MuiAutocomplete
            value={location.department}
            options={departments.map((el) => ({ ...el, label: el.name }))}
            renderInput={AutocompleteRenderInput('Departamento')}
            onChange={handleLocationChange('department')}
            isOptionEqualToValue={(option) => option.name}
          />
        </MuiGrid>
        <MuiGrid item xs={4}>
          <MuiAutocomplete
            disabled={location.department === null}
            value={location.province}
            options={getProvinces(location.department).map((el) => ({ ...el, label: el.name }))}
            renderInput={AutocompleteRenderInput('Provincia')}
            onChange={handleLocationChange('province')}
            isOptionEqualToValue={(option) => option.name}
          />
        </MuiGrid>
        <MuiGrid item xs={4}>
          <MuiAutocomplete
            disabled={location.province === null}
            value={location.district}
            options={getDistricts(location.province).map((el) => ({ ...el, label: el.name }))}
            renderInput={AutocompleteRenderInput('Distrito')}
            onChange={handleLocationChange('district')}
            isOptionEqualToValue={(option) => option.name}
          />
        </MuiGrid>
        <MuiGrid item xs={12}>
          <TextField
            label="Dirección"
            name="address"
            value={patient.address}
            onChange={handlePatientChangeData}
            fullWidth
          />
        </MuiGrid>
        <MuiGrid item xs={6} lg={8}>
          <TextField
            className="input-search"
            label="Correo Electrónico"
            type="email"
            name="email"
            value={patient.email}
            onChange={handlePatientChangeData}
            fullWidth
          />
        </MuiGrid>
        <MuiGrid item xs={6} lg={4}>
          <InputTags
            label="Teléfonos"
            name="phones"
            value={patient.phones}
            onChange={handlePatientChangePhones}
            fullWidth
          />
        </MuiGrid>
        <MuiGrid item xs={4} lg={4}>
          <TextField
            disabled={edit || !manualEntry}
            label="Género"
            select
            name="gender"
            value={patient.gender}
            onChange={handlePatientChangeData}
            fullWidth
          >
            <MuiMenuItem value="Masculino">Masculino</MuiMenuItem>
            <MuiMenuItem value="Femenino">Femenino</MuiMenuItem>
          </TextField>
        </MuiGrid>
        <MuiGrid item xs={4} lg={4}>
          <TextField
            label="Estado Civil"
            select
            name="civilStatus"
            value={patient.civilStatus}
            onChange={handlePatientChangeData}
            fullWidth
          >

            <MuiMenuItem value="Soltero">Soltero</MuiMenuItem>
            <MuiMenuItem value="Casado">Casado</MuiMenuItem>
            <MuiMenuItem value="Divorciado">Divorciado</MuiMenuItem>
            <MuiMenuItem value="Viudo">Viudo</MuiMenuItem>
          </TextField>
        </MuiGrid>
        <MuiGrid item xs={4} lg={4}>
          <DatePicker
            disabled={edit || !manualEntry}
            label="Fecha de Nacimiento"
            value={patient.dateOfBirth}
            onChange={handlePatientChangeDateOfBirth}
          />
        </MuiGrid>
        <MuiGrid item xs={12}>
          <TextField
            label="Ocupación"
            name="occupation"
            value={patient.occupation}
            onChange={handlePatientChangeData}
            fullWidth
          />
        </MuiGrid>
      </MuiGrid>
    </Modal>
  )
}

PatientModal.propTypes = {
  edit: PropTypes.bool,
  open: PropTypes.bool,
  onClose: PropTypes.func,
  patient: PropTypes.object.isRequired,
  onChangePatient: PropTypes.func.isRequired
}

PatientModal.defaultProps = {
  edit: false,
  open: false,
  onClose: () => { }
}

export default PatientModal
