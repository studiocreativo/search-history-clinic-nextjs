import { createTheme } from '@material-ui/core/styles';
import { esES } from '@material-ui/core/locale';

const theme = createTheme({
  palette: {
    primary: {
      main: '#02A7E1',
      contrastText: '#fff',
    },
    secondary: {
      main: '#084B83',
      contrastText: '#fff',
    },
    success: {
      main: '#5DFDCB',
    },
    warning: {
      main: '#FFC857',
    },
    black: {
      main: '#08090A',
    },
    white: {
      main: '#F4FAFF',
    },
    danger: {
      main: '#FF5E5B',
      contrastText: '#fff',
    },
    error: {
      main: '#FF5E5B',
      contrastText: '#fff',
    },
    background: {
      default: '#fff',
    },
    text: {
      success: '#5DFDCB',
      warning: '#FFC857',
    },
  },
  components: {
    MuiAppBar: {
      defaultProps: {
        color: 'inherit',
      },
    },
  },
}, esES);

export default theme;
