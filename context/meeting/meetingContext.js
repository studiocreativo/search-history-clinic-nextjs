import { createContext } from 'react'

export const initState = {
  open: false,
  edit: false,
  title: 'Agendar Cita',
  patient: {
    fullname: null
  },
  doctor: null,
  attentionDate: null,
  ailments: undefined,
  status: 1
}

const MeetingContext = createContext(initState)

export default MeetingContext
