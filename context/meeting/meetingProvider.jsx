import { useState } from 'react'
import PropTypes from 'prop-types'
import MeetingContext, { initState } from './meetingContext'

const MeetingProvider = ({ children }) => {
  const [meeting, setMeeting] = useState(initState)

  return (
    <MeetingContext.Provider value={{ ...meeting, setMeeting }}>
      {children}
    </MeetingContext.Provider>
  )
}

MeetingProvider.propTypes = {
  children: PropTypes.element
}

export default MeetingProvider
