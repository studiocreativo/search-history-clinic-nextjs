import { createContext } from 'react'

export const initState = {
  user: {
    firebase: null,
    local: null
  },
  doctors: [],
  patients: [],
  meetings: []
}

const GlobalContext = createContext(initState)

export default GlobalContext
