import { useState, useEffect } from 'react'
import { onIdTokenChanged } from 'firebase/auth'
import { FirebaseAuth } from '@services/firebase'
import nookies from 'nookies'
import FeaturesPatients from '@features/patients'
import FeaturesMeetings from '@features/meetings'
import GlobalContext, { initState } from './GlobalContext'
import PropTypes from 'prop-types'

const GlobalProvider = ({ children }) => {
  const [globalState, setGlobalState] = useState(initState)
  const [currentPatients, setCurrentPatients] = useState([])
  const [currentMeetings, setCurrentMeetings] = useState([])

  useEffect(() => {
    const unsub = onIdTokenChanged(FirebaseAuth, async (user) => {
      if (!user) {
        setGlobalState({
          ...globalState,
          user: {
            ...globalState.user,
            firebase: null
          }
        })
        nookies.destroy(null, 'auth')
        nookies.set(null, 'token', '', { path: '/' })
        return
      }

      const token = await user.getIdToken()
      setGlobalState({
        ...globalState,
        user: {
          ...globalState.user,
          firebase: user
        }
      })
      nookies.destroy(null, 'auth')
      nookies.set(null, 'token', token, { path: '/' })
    })
    return () => {
      unsub()
    }
  }, [])

  useEffect(() => {
    const handle = setInterval(async () => {
      const user = FirebaseAuth.currentUser
      if (user) {
        await user.getIdToken()
      }
    }, 10 * 60 * 1000)
    return () => {
      clearInterval(handle)
    }
  }, [])

  useEffect(() => {
    const patientsUnsuscribe = FeaturesPatients.getPatients((patients) => {
      setCurrentPatients(patients)
    })

    const meetingsUnsuscribe = FeaturesMeetings.getMeetings((meetings) => {
      setCurrentMeetings(meetings)
    })

    return () => {
      meetingsUnsuscribe()
      patientsUnsuscribe()
    }
  }, [])

  return (
    <GlobalContext.Provider value={{
      ...globalState,
      patients: currentPatients,
      meetings: currentMeetings
    }}>
      {children}
    </GlobalContext.Provider>
  )
}

GlobalProvider.propTypes = {
  children: PropTypes.node.isRequired
}

export default GlobalProvider
