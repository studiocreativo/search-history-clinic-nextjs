import { useContext } from 'react'
import GlobalContext from './GlobalContext'
import GlobalProvider from './GlobalProvider'

export const useAuth = () => {
  const { user } = useContext(GlobalContext)
  return user
}

export const useMeetings = () => {
  const { meetings } = useContext(GlobalContext)
  return meetings
}

export const usePatients = () => {
  const { patients } = useContext(GlobalContext)
  return patients
}

export default GlobalProvider
